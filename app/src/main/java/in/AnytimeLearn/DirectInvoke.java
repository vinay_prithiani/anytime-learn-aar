package in.AnytimeLearn;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.KeyGenerator;

public class DirectInvoke extends Activity {
	private TextView messageToUser;
	private WebView appGuidelines;
	protected static final String PREFS_NAME = "AnytimeLearnPref";
	protected static final String DEVICE_KEY = "DeviceKey";
	// Constants for Account
	// The authority for the sync adapter's content provider
	protected String AUTHORITY = null;
	// An account type, in the form of a domain name
	protected String ACCOUNT_TYPE = null;
	// The account name
	protected String ACCOUNT = null;
	// Instance fields
	Account mAccount;
	String testId, registeredEmail, deviceId;
	String courseName = null, usageGuidelines = null;
	Button testStartButton;
	protected ProgressDialog enablingChecker;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_direct_invoke);

		//assuming always a test for now
		testId = getIntent().getStringExtra("COURSE_ID");
		courseName = getIntent().getStringExtra("COURSE_NAME");
		//usageGuidelines = getIntent().getStringExtra("APP_GUIDELINES");
		registeredEmail = getIntent().getStringExtra("REGISTERED_EMAIL");

		deviceId = getDeviceId();

		testStartButton = (Button)findViewById(R.id.commenceCourseButton);
		appGuidelines = (WebView) findViewById(R.id.appUsageGuidelines);
		messageToUser = (TextView)findViewById(R.id.MessageFromApp);

		createRegistrationSettingIfNotDone();
		initiateTestIfEnabled();

		//startButton.setEnabled(true);
	}

	private void createRegistrationSettingIfNotDone()
	{
		//put settings below which are required for upgrades.
		//Copy the same settings to registration code so that new installations have the settings.
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		boolean dirtyFlag = false;

        //create storage pref
        if (!settings.contains(getString(R.string.STORAGE_LOCATION))) //create storage location if it does not exist
        {
            System.out.println("Storage location does not exist");
            dirtyFlag = true;
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()))
                editor.putString(getString(R.string.STORAGE_LOCATION), getExternalFilesDir(null).toString());
            else
                editor.putString(getString(R.string.STORAGE_LOCATION), getFilesDir().toString());
        }

		if (!settings.contains(getString(R.string.DEVICE_ID))) //add device id if it does not exist
		{
			System.out.println("Device id pref does not exist");
			dirtyFlag = true;
			editor.putString(getString(R.string.DEVICE_ID), deviceId);
			try {
				//create the device key for encryption/decryption
				SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
				sr.setSeed(deviceId.getBytes());
				KeyGenerator kg = KeyGenerator.getInstance("AES");
				kg.init(128, sr);

				byte[] deviceKey = kg.generateKey().getEncoded();
				//convert to base64 for storage
				String deviceKeyString = Base64.encodeToString(deviceKey, Base64.DEFAULT);

				editor.putString(getString(R.string.DEVICE_KEY), deviceKeyString);
			}
			catch (NoSuchAlgorithmException ae)
			{
				settings.edit().remove(getString(R.string.DEVICE_KEY)); //removing so that application does not proceed
				settings.edit().remove(getString(R.string.DEVICE_ID)); //removing so that application does not proceed
			}

			// Create the account for adapter. Initialize all sync constants
			AUTHORITY = getString(R.string.SYNC_AUTHORITY);
			ACCOUNT_TYPE = getString(R.string.SYNC_ACCOUNT_TYPE);
			ACCOUNT = deviceId;
			mAccount = CreateSyncAccount(this);

		}

		if (dirtyFlag)
		{
			// Commit the edits!
			editor.commit();
		}

	} //createRegistrationSettingIfNotDone()

	/**     * Create a new account for the sync adapter
	 * *
	 * * @param context The application context     */
	private Account CreateSyncAccount(Context context)
	{
		// Create the account type and default account
		Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
		// Get an instance of the Android account manager
		AccountManager accountManager = (AccountManager) context.getSystemService(ACCOUNT_SERVICE);

		/** Add the account and account type, no password or user data
		 * * If successful, return the Account object, otherwise report an error.
		 * */
		if (accountManager.addAccountExplicitly(newAccount, null, null))
		{
			/** If you don't set android:syncable="true" in
			 * * in your <provider> element in the manifest,
			 * * then call context.setIsSyncable(account, AUTHORITY, 1)
			 * * here.             */
		}
		else
		{
			/** The account exists or some other error occurred. Log this, report it,
			 * * or handle it internally.
			 * */
		}

		return newAccount;
	} //createSyncAccount()

	private String getDeviceId()
	{
		String subscriberId = null;

		//Code below to get sim id
		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		if (tm != null)
		{
			//check if the device has a SIM
			subscriberId = tm.getSubscriberId();

			if (subscriberId != null) //not null means the device has SIM
				//truncating subscriber id (IMSI) to remove the operator portion as that seems to vary during roaming. We are using just the MSIN portion of the no.
				subscriberId = subscriberId.substring(tm.getSimOperator().length());
			else //means the phone does not have a SIM
				subscriberId = tm.getDeviceId();
		}
		if (subscriberId == null) //non-phone device probably
			if (android.os.Build.SERIAL != null)
				subscriberId = android.os.Build.SERIAL;
			else
			{
				//add code here to show error message to user when registration fails
				System.out.println("Registration failed");
			}

		return subscriberId;
	}

	private void initiateTestIfEnabled()
	{
		enablingChecker = new ProgressDialog(this);
		enablingChecker.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		enablingChecker.setMessage("Checking if enabled for Learning Content..");
		enablingChecker.show();

		new checkRegistrationAndGetWelcomeTextAsyncTask().execute();
	}

	public void commenceCourse(View startButton)
	{
		SharedPreferences settings = getSharedPreferences(getString(R.string.PREFS_NAME), 0);
		//	key already created during registration

		String deviceId = settings.getString(getString(R.string.DEVICE_ID), null);
		if (deviceId== null)
		{
			System.out.println("Device key not found");
			messageToUser.setText("Error initializing. Please re-register");
			return;
		}

		//check course state. Right now assuming that if a row exists for course, it is current. We need to find a way to check this with server later
		boolean courseCacheState = checkIfCourseRowFresh();
		Intent examViewIntent = new Intent(this, in.AnytimeLearn.ContinuousExamTaker.class);
		examViewIntent.putExtra("SIM_ID", deviceId);
		examViewIntent.putExtra("COURSE_ID", testId);
		examViewIntent.putExtra("COURSE_NAME", courseName);
		examViewIntent.putExtra("COURSE_MODIFY_DATE", "");
		examViewIntent.putExtra(getString(R.string.CACHE_STATE), courseCacheState);

		startActivity(examViewIntent);
		finish(); //finish current activity

	}

	private boolean checkIfCourseRowFresh() //function checks for the current course id if the modify date is latter than the cache date
	{
		boolean courseCacheState = false;

		//get the cache date for this course
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		//read from the db here
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = db.query(AnytimeLearnDb.COURSE_TABLE_NAME, new String[]{AnytimeLearnDb.DOWNLOAD_DATE_COL_NAME},
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[]{testId}, null, null, null, null);
		if (cursor != null) {
			if (cursor.moveToFirst())            //means course row exists.
				courseCacheState = true; //currently returning true as we don't have date from server yet.
		}
		cursor.close();
		db.close();

		return courseCacheState;
	} //checkIfCourseRowFresh()


	private class checkRegistrationAndGetWelcomeTextAsyncTask extends AsyncTask<Object, Void, Object[]>
	{
		private String responseText = "";

		@Override
		protected Object[] doInBackground(Object... urls) {
			String response = "";

			// url where the data will be posted
			String checkRegistrationUrl = "http://www.anytimelearn.in/maPages/checkRegistrationForDirectInvoke.php";

			//set the name value parameters for call
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
			nameValuePairs.add(new BasicNameValuePair("EmailId",registeredEmail));
			nameValuePairs.add(new BasicNameValuePair("SimId", deviceId));
			nameValuePairs.add(new BasicNameValuePair("CourseId", testId));
System.out.println("Test id = " + testId);
			PostRequestPerform testAttributeRequest = new PostRequestPerform(checkRegistrationUrl, nameValuePairs);
			InputStream content = testAttributeRequest.getResponse();

			if (content != null)
			{
				//reply expected in form lastPage&totalPage
				BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
				try
				{
					String s = "";
					while ((s = buffer.readLine()) != null)
						responseText += s;
					//System.out.println("book mark response " + response);
				}
				catch (IOException ioe)
				{
					ioe.printStackTrace();
					//lastPage = totalPages = -1; //commenting to avoid overwriting of value from db. We ignore exception and let value remain -1
					return null;
				}

			}
			else
				return null; //when there was an error in retrieving data

			Integer returnValue = new Integer(1);

			return new Integer[]{returnValue};
		}

		@Override
		protected void onPostExecute(Object[] pageData) {
			String usageGuidelines = null;

		//	System.out.println("Response from check enrollment = " + responseText);

			enablingChecker.dismiss();

			if (pageData == null) //shows problems with internet connectivity
			{
				usageGuidelines = "Please verify your Internet connectivity and retry";
				messageToUser.setText(usageGuidelines);
				messageToUser.setVisibility(View.VISIBLE);
				appGuidelines.setVisibility(View.INVISIBLE);
				testStartButton.setVisibility(View.INVISIBLE);
			}
 			else if (responseText.startsWith("0")) //means not registered
			{
				usageGuidelines = "You do not seem to be registered for this learning content. Please check with the Provider";
				messageToUser.setText(usageGuidelines);
				messageToUser.setVisibility(View.VISIBLE);
				appGuidelines.setVisibility(View.INVISIBLE);
				testStartButton.setVisibility(View.INVISIBLE);
			}
			else {
				usageGuidelines = responseText;
				testStartButton.setEnabled(true);
				testStartButton.setVisibility(View.VISIBLE);
				appGuidelines.loadData(usageGuidelines,"text/html", null);
			}


		} //end of handler function

	} //checkRegistrationAndGetWelcomeTextAsyncTask
}