package in.AnytimeLearn;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.File;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

//import com.customapp.android.AnytimeLearn.R;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.os.Messenger;
import android.os.Message;
import android.os.Bundle;
import android.util.Base64;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
 

public class CourseRetrieverDownloadOptimized extends IntentService {
	private int isPreviousEnabled;
	private int isNextEnabled;
	private boolean courseResetStatus;
//	private final String PREFS_NAME = "AnytimeLearnPref";
//	private final String DEVICE_KEY = "DeviceKey";
	private byte[] deviceKey;
	private int lastPage = -1, totalPages;
	private boolean cacheStatus = false;
	private String storageDirectory = null;
	
	public CourseRetrieverDownloadOptimized(String name) {
		super(name);
		// TODO Auto-generated constructor stub 
	}

	public CourseRetrieverDownloadOptimized() {
		super("CourseRetriever");
		// TODO Auto-generated constructor stub
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override 
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		String trigger = intent.getStringExtra("Trigger");
//		System.out.println("Intent trigger = " + trigger);
		if ((trigger != null) && ((trigger.equals("CourseReset")) || (trigger.equals("CourseListing"))))
		{
		//	boolean stopStatus = stopSelfResult(startId);
		//	System.out.println("Stop status "+ stopStatus);
			courseResetStatus = true;
		}
		return super.onStartCommand(intent, flags, startId);     
	} 	
/*	@Override
	public void onDestroy()
	{
		System.out.println("On destroy called");
		super.onDestroy();
	}
*/	
	@Override
	public void onCreate()
	{
		super.onCreate();
		isPreviousEnabled = isNextEnabled = 0;
		courseResetStatus = false;
	//	try
	//	{
	//      FileInputStream fIn = openFileInput("samplefile.txt");
	//        InputStreamReader isr = new InputStreamReader(fIn);          
	        /* Prepare a char-Array that will          
	         * * hold the chars we read back in. */         
	//       char[] inputBuffer = new char[1];          
	        // Fill the Buffer with data from the file         
	//       isr.read(inputBuffer);
	        // Transform the chars to a no.         
	//       currentCallNo = new Integer(new String(inputBuffer)).intValue();          
	//	}
	//    catch (IOException ioe)        
	//    {
	//    	currentCallNo = 0;
	//    	ioe.printStackTrace();
	//    } 

//		currentCallNo = 0;
	}
	
	private void readDataFromFile(ArrayBlockingQueue<String> fileData)
	{
			try
			{
		      FileInputStream fIn = openFileInput("nextPageBuffer.txt");
		      InputStreamReader isr = new InputStreamReader(fIn);
		      BufferedReader br = new BufferedReader(isr);
		      String fileLine = "";
		      while ((fileLine = br.readLine()) != null)
		      {
		    	  fileData.add(fileLine);
		      }
		      
		      br.close();
		      isr.close();
		      fIn.close();
		      
		      if (fileData.size() > 1) //there are atleast 2 pages
		    	  isNextEnabled = 1;
			}
		    catch (IOException ioe)        
		    {
		    	ioe.printStackTrace();
		    } 
	}

	private String[] parseResponseAndGetUrls(String response, String[] courseParams) {
	    String[] contentValues = response.split("&");
	    
	    //format is contentValue[0] the number value indicates the current page no. of text
	    //contentValue[1] the number 1 indicates sound page corresponding to the text page no
	    //contentValue[2] number 0 indicates no previous, 1 indicates previous
	    //contentValue[3] number 0 indicates no next, 1 indicates next
	    //contentValue[4] number 0 indicates no next, 1 indicates next
	    int pageNo = Integer.valueOf(contentValues[0]);
	    int soundExists = Integer.valueOf(contentValues[1]);
	    isPreviousEnabled = Integer.valueOf(contentValues[2]);
	    isNextEnabled = Integer.valueOf(contentValues[3]);
	    int pageType = Integer.valueOf(contentValues[4]);
System.out.println("Page no = " + pageNo + " soundExists = "+soundExists+" isPreviousEnabled = "+isPreviousEnabled+" isNextEnabled = "+isNextEnabled + " pageType = "+ pageType);	
	    
	    String[] returnValues = new String[4];
	    
	    returnValues[0] = null;
	    if (pageNo != 0) //not the last page
	    {
	    	 returnValues[0] = getString(R.string.baseURL) + courseParams[0] + "/" + pageNo;
	    	//returnValues[0] = ""+ pageNo; //postChange 
			
		    switch (pageType)
		    {
		    	case 2 : returnValues[0] = returnValues[0] + ".swf"; break;
		    	case 3 :  returnValues[0] = returnValues[0] + ".mp4"; break;
		    	case 4 :  returnValues[0] = returnValues[0] + ".ogv"; break;
		    	case 5 :  returnValues[0] = returnValues[0] + ".avi"; break;
		    	default : returnValues[0] = returnValues[0] + ".html"; break;
		    }

/*
		    if (pageType == 2) //swf page
	    		returnValues[0] = returnValues[0] + ".swf";
	    		//returnValues[0] = returnValues[0] + ".mp4";  //changing to mp4 temporarily
	    	else //html page (content or question)
	    		returnValues[0] = returnValues[0] + ".html"; */
	    }
	    	
	    returnValues[1] = null;
	    if (soundExists > 0) //>0 is the page no. of the sound page
    	    returnValues[1] = getString(R.string.baseURL) + courseParams[0] + "/" + pageNo + "s.mp3"; //postChange
	    	//  returnValues[1] = pageNo + "s.mp3"; //postChange
	    
	    returnValues[2] = "" + pageNo;
	    returnValues[3] = "" + pageType;
	    
	    return returnValues;
	}
	
	//this downloads a page of the course as per parameters given from server
	private String[] downloadNumberedPage(String courseId, String simId, String pageNo, boolean cachedStatus)
	{
		return downloadOnePageAndStoreLocally(courseId, simId, pageNo, cachedStatus);
		//return new String[3];
	}
	
	//calls the handler to display page
	private void sendPageForDisplay(String displayUrls[], Intent intentToService)
	{
		Bundle extras = intentToService.getExtras();

		if (extras != null) {
			Messenger messenger = (Messenger) extras.get("MESSENGER");
			Message msg = Message.obtain();
//			msg.arg1 = result;
			msg.obj = displayUrls;
			try {
				messenger.send(msg);
			} catch (android.os.RemoteException e1) {
				//	Log.w(getClass().getName(), "Exception sending message", e1);
				e1.printStackTrace();
			}
		}
		
		return;
	}

	//sends the reply to the activity so that it can display list of courses
	private void courseListingReply(String courseId, String simId, String lastDisplayedPage, Intent intent)
	{
		//reset the server to the page last displayed
		HttpClient client = new DefaultHttpClient();
		String pageResetUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "updatePageNo.php?simId="+simId+"&courseId="+courseId+"&pageNo="+lastDisplayedPage;
		
		HttpGet httpSetPageNo = new HttpGet(pageResetUrl);
		try {
				//get the text part
				client.execute(httpSetPageNo);
			} 
		catch (Exception e) {
				e.printStackTrace();
		}

		//send the response to handler
		Bundle extras = intent.getExtras();
	
		if (extras != null) {
			Messenger messenger = (Messenger) extras.get("MESSENGER");
			Message msg = Message.obtain();
//			msg.arg1 = result;
			msg.obj = null; //no response from server in this case
			try {
				messenger.send(msg);
			} catch (android.os.RemoteException e1) {
				//	Log.w(getClass().getName(), "Exception sending message", e1);
				e1.printStackTrace();
			}
		}
		
		//delete and reset any downloaded contents
		deleteFile("nextPageBuffer.txt"); 
		
		return;
	} //courseListingReply()

	//sends the reply to the activity so that it can display list of courses
	private void testPageGetReply(String courseId, String simId, String lastDisplayedPage, String totalPagesInTest, Intent intent)
	{
		//see if a specific page is asked for
		if (lastDisplayedPage != null)
			lastPage = Integer.parseInt(lastDisplayedPage);

		//see if total pages is already known
		if (totalPagesInTest != null)
			totalPages = Integer.parseInt(totalPagesInTest);
		
		System.out.println("Current test page = " + lastPage);

		boolean internetOn;

		//first check if Internet is on
	    ConnectivityManager connectivityManager 
		          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		internetOn =  (activeNetworkInfo != null) && (activeNetworkInfo.isConnected());

System.out.println("Internet state = " + internetOn + " Network = " + activeNetworkInfo.getTypeName() + " subtype name = " + activeNetworkInfo.getSubtypeName());

		boolean serverRetrievalSuccess = false;

		if (lastPage == -1)
		{
			if (internetOn)
				serverRetrievalSuccess = getLastPageAndTotalPage(courseId, simId);
		

			//if status is successful, then last page and total page are available, else read from local content status
			if ((!serverRetrievalSuccess) && cacheStatus) //course must have been locally cached
				getLastAndTotalPageFromCache(courseId);
			else if (!serverRetrievalSuccess)
				//need to return error at this point
				return;
		}
		
		System.out.println("Last page = " + lastPage + " Total page = " + totalPages);
		
		if (lastPage == totalPages)
			return; //should decide what to do here
		
		String testContentFileName = readTestPage(courseId, lastPage + 1); //method should check if page is in local cache.Else read from server and return the page String
		
		/*
		String testPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestQuestion.php"; //+ "?simId="+simId + "&courseId="+courseId;
		
		//save the test page and send a reply
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGetText = new HttpGet(testPageUrl);

		//create the file
		String testContentFileName = courseId+"_"+ "1"; //hardcoding the file name for now
		System.out.println("Test file name = " + testContentFileName);
		
		File questionTextFile = new File(getExternalFilesDir(null), testContentFileName);
		
		try {
				FileOutputStream questionFileOutputStream= new FileOutputStream(questionTextFile);
				HttpResponse execute = client.execute(httpGetText);
				InputStream content = execute.getEntity().getContent();

				BufferedInputStream buffer = new BufferedInputStream(content);
				byte buf[]=new byte[10240];
				int len;
				while((len=buffer.read(buf)) >0)
					questionFileOutputStream.write(buf,0,len);
			
				questionFileOutputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		*/
		Bundle extras = intent.getExtras();
		String[] urls = new String[5]; //2 strings are url, third is page no and fourth is pagetype and fifth is total pages
		urls[0] = testContentFileName; //text url
		urls[1] = ""; //sound url
		urls[2] = Integer.toString(lastPage + 1); //this is the new page no.
		urls[3] = ""; //this is page type
		urls[4] = Integer.toString(totalPages);

	
		if (extras != null) {
System.out.println("Calling test activity");			
			Messenger messenger = (Messenger) extras.get("MESSENGER");
			Message msg = Message.obtain();
//			msg.arg1 = result;
			msg.obj = urls;
			try {
				messenger.send(msg);
			} catch (android.os.RemoteException e1) {
				//	Log.w(getClass().getName(), "Exception sending message", e1);
				e1.printStackTrace();
			}
		}

	} //testPageGetReply()
	
	private String readTestPage(String courseId, int pageNo)
	{
	//	boolean localCacheFound = false;
		String testPageName = courseId+"_"+ pageNo;
		
		/* commenting this as we will never call for cached page
		//read the cache from db
		//check if page exists locally only if cached status is true
		if (cacheStatus)
		{	
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
			
			//read the last page stored
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME, AnytimeLearnDb.HAS_SOUND_COL_NAME, AnytimeLearnDb.HAS_NEXT_COL_NAME, AnytimeLearnDb.HAS_PREV_COL_NAME, AnytimeLearnDb.PAGE_TYPE_COL_NAME}, 
					AnytimeLearnDb.COURSE_ID_COL_NAME + "=? AND " + AnytimeLearnDb.PAGE_NO_COL_NAME + "=?",
					new String[] {courseId, Integer.toString(pageNo)}, null, null, null, null);
			if (cursor != null)
			{
				if (cursor.moveToFirst()) 	//means page row exists.
				{
					localCacheFound = true;
					testPageName = courseId + "_" + cursor.getInt(0);
				}
			}
			cursor.close();
			db.close();
		}
		
	//	localCacheFound = false;
		System.out.println("Local cached found status = " + localCacheFound);

		if (!localCacheFound)
		{ */
		//get the text content to a local file of the same name
		String testPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestQuestion.php";
		//File questionTextFile = new File(getExternalFilesDir(null), testPageName);
		File questionTextFile = new File(storageDirectory, testPageName);

		/* session key to be added
		//get the session cookie here to send to server for genuine call check
		String sessionKeyValue = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(ProfileSelectionActivity.SESSION_KEY, null);
		httpGetContentPost.setHeader("Cookie", sessionKeyValue);
		*/
			
		//set the course id and page no.
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
		nameValuePairs.add(new BasicNameValuePair("CourseId",courseId));            
		nameValuePairs.add(new BasicNameValuePair("PageNo", Integer.toString(pageNo)));            
			

		PostRequestPerform testPageRequest = new PostRequestPerform(testPageUrl, nameValuePairs);
		InputStream content = testPageRequest.getResponse();

		try
		{
			FileOutputStream questionFileOutputStream= new FileOutputStream(questionTextFile);

			SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");
			// Create the cipher for encryption
			Cipher cEncrypt = Cipher.getInstance("AES");
			cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
			CipherOutputStream encryptedQuestionFile = new CipherOutputStream(questionFileOutputStream, cEncrypt); 

			if (content != null)
			{
				BufferedInputStream buffer = new BufferedInputStream(content);
				byte buf[]=new byte[10240];
				int len;
				while((len=buffer.read(buf)) >0)
					encryptedQuestionFile.write(buf,0,len);
			
				encryptedQuestionFile.close();

				AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

				SQLiteDatabase db = dbHelper.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, courseId);
				values.put(AnytimeLearnDb.PAGE_NO_COL_NAME, pageNo);
				values.put(AnytimeLearnDb.HAS_SOUND_COL_NAME, 0);
				values.put(AnytimeLearnDb.HAS_NEXT_COL_NAME, 0);
				values.put(AnytimeLearnDb.HAS_PREV_COL_NAME, 0);
				values.put(AnytimeLearnDb.PAGE_TYPE_COL_NAME, 0);
			
				long insertValue = db.insert(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, null, values);
				System.out.println("Insert value for test page= " + insertValue);
				db.close(); // Closing database connection			
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			testPageName = null;
		}
			
		return testPageName;
	} //readTestPage()
	
	//get course bookmarks from local cache
	private void getLastAndTotalPageFromCache(String courseId)
	{
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
		
		//read the last page stored
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = db.query(AnytimeLearnDb.COURSE_TABLE_NAME, new String[] { AnytimeLearnDb.NUM_PAGES, AnytimeLearnDb.LAST_DISPLAY_PAGE}, 
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {courseId}, null, null, null, null);
		if (cursor != null)
		{
			if (cursor.moveToFirst()) 	//means page row exists.
			{
				totalPages =  cursor.getInt(0);
				lastPage = cursor.getInt(1);
			}
			cursor.close();
		}
		
		db.close();
		return;
	}
	
	
	private void downloadFullCourse(String courseId, String simId, int totalPages)
	{
	     Map<Integer, String> availablePageMap = new HashMap<Integer, String>();   
	     
		//start from page 1 and check for missing page
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
		
		//read the last page stored
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME}, 
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {courseId}, null, null, null, null);
		if (cursor != null)
		{

   	    while (cursor.moveToNext())
   	    	availablePageMap.put(cursor.getInt(0), "");
   	    cursor.close();
		}
		
		db.close();
		
		for (int count = 1; count <= totalPages; count++)
			if (!availablePageMap.containsKey(new Integer(count)))
				//key not present, page should be downloaded
				downloadOnePageAndStoreLocally(courseId, simId, Integer.toString(count), false);
	} //downloadFullCourse()

	private void downloadFullTest(String courseId, String simId, int totalPages)
	{
	     Map<Integer, String> availablePageMap = new HashMap<Integer, String>();   
	     
		//start from page 1 and check for missing page
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
		
		//read the last page stored
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME}, 
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {courseId}, null, null, null, null);
		if (cursor != null)
		{

    	    while (cursor.moveToNext())
    	    	availablePageMap.put(cursor.getInt(0), "");
    	    cursor.close();
		}
		
		db.close();
		
		for (int count = 1; count <= totalPages; count++)
			if (!availablePageMap.containsKey(new Integer(count)))
				//key not present, page should be downloaded
				readTestPage(courseId, count);

	} //downloadFullTest()
	
	//gets course status for this sim from server. If not successful returns false
	private boolean getLastPageAndTotalPage(String courseId, String simId)
	{
		boolean returnStatus = false;
		
		HttpClient client = new DefaultHttpClient();
		
		String getBookmarksUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getBookmark.php";
		HttpPost httpGetBookmarkPost = new HttpPost(getBookmarksUrl);
				
		/*
		//get the session cookie here to send to server for genuine call check
		String sessionKeyValue = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(ProfileSelectionActivity.SESSION_KEY, null);
		httpGetContentPost.setHeader("Cookie", sessionKeyValue);
		*/		
		//set the course id and sim id.
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
		nameValuePairs.add(new BasicNameValuePair("CourseId",courseId));            
		nameValuePairs.add(new BasicNameValuePair("SimId",simId));
		String response = "";		
				
		try {
					httpGetBookmarkPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					System.out.println("Calling url for bookmark = " + getBookmarksUrl);
					
					HttpResponse execute = client.execute(httpGetBookmarkPost); 
					InputStream content = execute.getEntity().getContent();

					//reply expected in form lastPage&totalPage
					BufferedReader buffer = new BufferedReader(
							new InputStreamReader(content));
					String s = "";
					while ((s = buffer.readLine()) != null) {
						response += s;
					}
					returnStatus = true;
					
				} catch (Exception e) {
					e.printStackTrace();
					returnStatus = false;
				}
			
			if (returnStatus)
			{
				System.out.println("book mark response " + response);
				
				String[] courseBookmarks = response.split("&");

				lastPage = Integer.valueOf(courseBookmarks[0]);
				totalPages = Integer.valueOf(courseBookmarks[1]);
			}
			
			return returnStatus;
	} //getLastPageAndTotalPage
	
/*
	//resets the course and sends the reply to the activity
	private void resetCourseAndSendReply(String courseId, String simId, Intent intent)
	{
		String resetUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "coursereset.php" + "?simId="+simId + "&courseId="+courseId;
//		String resetUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "courseresettemp.php" + "?simId="+simId + "&courseId="+courseId;
		System.out.println("URL called = " + resetUrl);
		
		
		//reset url and get response
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGetText = new HttpGet(resetUrl);

		String response = "";
		try {
				HttpResponse execute = client.execute(httpGetText);
				InputStream content = execute.getEntity().getContent();

				BufferedReader buffer = new BufferedReader(
						new InputStreamReader(content));
				String s = "";
				while ((s = buffer.readLine()) != null) {
					response += s;
				}	
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		//send the response to handler
		Bundle extras = intent.getExtras();
	
		if (extras != null) {
System.out.println("Calling activity to reset");			
			Messenger messenger = (Messenger) extras.get("MESSENGER");
			Message msg = Message.obtain();
//			msg.arg1 = result;
			msg.obj = response;
			try {
				messenger.send(msg);
			} catch (android.os.RemoteException e1) {
				//	Log.w(getClass().getName(), "Exception sending message", e1);
				e1.printStackTrace();
			}
		}
		
		//delete and reset any downloaded contents
		
		return;
	}
*/	
	//returns in the first element of the array the text file name locally stored and sound file in the second element. 
	//This internally calls another function of similiar name with no value for page no
	private String[] downloadOnePageAndStoreLocally(String courseId, String simId, boolean cachedStatus)
	{
		return downloadOnePageAndStoreLocally(courseId, simId, null, cachedStatus);
	}
	
	//returns in the first element of the array the text file name locally stored and sound file in the second element.
	private String[] downloadOnePageAndStoreLocally(String courseId, String simId, String pageNo, boolean cachedStatus)
	{
		int hasSound = 0;
		String courseContentStatusUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getContentStatus.php?courseId=" + courseId + "&simId=" + simId;

		//add page No if available
		if (pageNo != null)
		{
			courseContentStatusUrl = courseContentStatusUrl + "&pageNo="+pageNo;

		}

		System.out.println("Content url " + courseContentStatusUrl);
		
		//get the no. of urls and if there is a next page
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGetText = new HttpGet(courseContentStatusUrl);
		String response = "";
		try {
				HttpResponse execute = client.execute(httpGetText);
				InputStream content = execute.getEntity().getContent();

				BufferedReader buffer = new BufferedReader(
						new InputStreamReader(content));
				String s = "";
				while ((s = buffer.readLine()) != null) {
					response += s;
				}	
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		String[] courseParams = new String[]{courseId, simId};
		
		String[] contentUrls = parseResponseAndGetUrls(response, courseParams);
		String[] savedFiles = new String[4]; //2 to store the urls, third element to give page no and fourth gives the page type. 0 indicates normal page, 1 question page and 2 is flash/video page
		int pageType = Integer.parseInt(contentUrls[3]); //use object array in parseResponseAndGetUrls to avoid needless conversion from string to int
		pageNo = contentUrls[2];
		
		if (contentUrls[0] != null) //not the last page
		{
			//get the text content to a local file of the same name
			httpGetText = new HttpGet(contentUrls[0]);
			//postChange
			//String readUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getPage.php";
			//HttpPost httpGetContentPost = new HttpPost(readUrl);
			
			//get the session cookie here to send to server for genuine call check
			//String sessionKeyValue = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(ProfileSelectionActivity.SESSION_KEY, null);
			//httpGetContentPost.setHeader("Cookie", sessionKeyValue);
			
			//set the course id and page no.
			//List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
			//nameValuePairs.add(new BasicNameValuePair("CourseId",courseParams[0]));            
			//nameValuePairs.add(new BasicNameValuePair("PageNo",contentUrls[0]));            
			//nameValuePairs.add(new BasicNameValuePair("Type","Content"));
			//nameValuePairs.add(new BasicNameValuePair("SimId",courseParams[1]));
			
			
			try {
				//httpGetContentPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				//System.out.println("Calling url = " + readUrl);
				
				HttpResponse execute = client.execute(httpGetText);
				//HttpResponse execute = client.execute(httpGetContentPost); //postChange
				InputStream content = execute.getEntity().getContent();
				BufferedInputStream buffer = new BufferedInputStream(content);
					//open the local text file for storage
				String mainContentFileName = courseId+"_"+contentUrls[2];
					/* Commented below to avoid adding an extension to saved file
			    switch (pageType)
			    {
			    	case 2: mainContentFileName = mainContentFileName +".swf";  break; //format is courseid_pageNo.swf
			    	case 3: mainContentFileName = mainContentFileName +".mp4"; break;
			    	case 4: mainContentFileName = mainContentFileName + ".ogv"; break;
			    	case 5: mainContentFileName = mainContentFileName + ".avi"; break;
			    	default: mainContentFileName = mainContentFileName +".html"; //format is courseid_pageNo.html
			    } */
				
/*					
				if (pageType == 2) //check if swf page
					mainContentFileName = mainContentFileName +".swf"; //format is courseid_pageNo.swf
				//	mainContentFileName = mainContentFileName +".mp4"; //temporary change to mp4. format is courseid_pageNo.swf
				else //html page (course or question)
					mainContentFileName = mainContentFileName +".html"; //format is courseid_pageNo.html
*/				
				System.out.println("main file name = " + mainContentFileName);
				//					FileOutputStream fText = openFileOutput(textFileName, MODE_WORLD_READABLE); 
				
				//open world readable for video files as video player needs so. Others open as private
			//	FileOutputStream fText;
				OutputStream fText;
				if ((pageType == 3) || (pageType == 4) || (pageType == 5))
				{
				//	fText = openFileOutput(mainContentFileName, MODE_WORLD_READABLE);
					//File textFile = new File(getExternalFilesDir(null), mainContentFileName);
					File textFile = new File(storageDirectory, mainContentFileName);
					
					SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");
						// Create the cipher
		            Cipher cEncrypt = Cipher.getInstance("AES");
		            cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
		            fText = new CipherOutputStream(new FileOutputStream(textFile), cEncrypt); 						
					// fText = new FileOutputStream(textFile);
				}
				else
				{
					//create encrypted content for text content
					
				    //fText = openFileOutput(mainContentFileName, MODE_PRIVATE);
					//File textFile = new File(getExternalFilesDir(null), mainContentFileName);
					File textFile = new File(storageDirectory, mainContentFileName);
					//fText = new FileOutputStream(textFile);
					/*
					//create the key for encryption
					SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		            sr.setSeed(simId.getBytes());
		            KeyGenerator kg = KeyGenerator.getInstance("AES");
		            kg.init(128, sr);
					
					SecretKeySpec encryptKey = new SecretKeySpec(kg.generateKey().getEncoded(), "AES");
					*/
					SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");
						// Create the cipher
		            Cipher cEncrypt = Cipher.getInstance("AES");
		            cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
		            fText = new CipherOutputStream(new FileOutputStream(textFile), cEncrypt); 
					
				}
				byte buf[]=new byte[10240];
				int len;
				while((len=buffer.read(buf)) >0)
					fText.write(buf,0,len);
			
				fText.close();
				savedFiles[0] = mainContentFileName;
			
			} 
			catch (Exception e) {
				e.printStackTrace();
				savedFiles[0] = null;
			}
		} //downloading content
		else
		{
			savedFiles[0] = null;
		}
		
		//get the sound content to a local file of the same name
		if (contentUrls[1] != null)
		{
			hasSound = 1;
			//postChange
			//combine code to create post request to a single location
			httpGetText = new HttpGet(contentUrls[1]);
			//String readUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getPage.php";
			//HttpPost httpGetSoundPost = new HttpPost(readUrl);
			
			//get the session cookie here to send to server for genuine call check
			//String sessionKeyValue = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(ProfileSelectionActivity.SESSION_KEY, null);
			//httpGetSoundPost.setHeader("Cookie", sessionKeyValue);
			
			//set the course id and page no.
			//List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
			//nameValuePairs.add(new BasicNameValuePair("CourseId",courseParams[0]));            
			//nameValuePairs.add(new BasicNameValuePair("PageNo",contentUrls[1]));            
			//nameValuePairs.add(new BasicNameValuePair("Type","Sound"));
			//nameValuePairs.add(new BasicNameValuePair("SimId",courseParams[1]));
			
			try {
				//postChange
				//httpGetSoundPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				//System.out.println("Calling url for sound = " + readUrl);
				//HttpResponse execute = client.execute(httpGetSoundPost);
				HttpResponse execute = client.execute(httpGetText);
				InputStream content = execute.getEntity().getContent();
				BufferedInputStream buffer = new BufferedInputStream(content);
				//open the local sound file for storage
				/*
				//getting date to randomize the file name
				DateFormat dateFormat = new SimpleDateFormat("dd_MM_HH_mm_ss");
				//get current date time with Date()
				Date date = new Date();
					String soundFileName = courseId+"_"+contentUrls[2]+"_"+dateFormat.format(date)+"_s.mp3"; //format is courseid_pageNo_s.mp3
				*/
				//below line commented to avoid putting an extention to the file
				//String soundFileName = courseId+"_"+contentUrls[2]+"_s.mp3"; //format is courseid_pageNo_s.mp3
				String soundFileName = courseId+"_"+contentUrls[2]+"_s"; //format is courseid_pageNo_s
				//FileOutputStream fSound = openFileOutput(soundFileName, MODE_WORLD_READABLE);   
				//File soundFile = new File(getExternalFilesDir(null), soundFileName);
				File soundFile = new File(storageDirectory, soundFileName);
				
				//create the encrypted sound file
				/*
				//create the key for encryption
				SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
	            sr.setSeed(simId.getBytes());
	            KeyGenerator kg = KeyGenerator.getInstance("AES");
	            kg.init(128, sr);
				
				SecretKeySpec encryptKey = new SecretKeySpec(kg.generateKey().getEncoded(), "AES");
				*/
				SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");

				// Create the cipher
	            Cipher cEncrypt = Cipher.getInstance("AES");
	            cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
	            OutputStream fSound = new CipherOutputStream(new FileOutputStream(soundFile), cEncrypt); 
					//FileOutputStream fSound = new FileOutputStream(soundFile);
				byte buf[]=new byte[1024];
				int len;
				int lengthRead = 0;
				while((len=buffer.read(buf)) >0)
				{
					fSound.write(buf,0,len);
					lengthRead++;
				}
				
				fSound .close();
				savedFiles[1] = soundFileName;
				
				System.out.println("Name = "+ soundFileName + " size  = " + lengthRead);
				
				} 
			catch (Exception e) 
			{
				e.printStackTrace();
				savedFiles[1] = null;
				savedFiles[0] = null; //making text file also 0 in case sound file download faced any exception
			}
		} //if (contentUrls[1] != null)
		else
			savedFiles[1] = null;
		
		//save the page details locally if required
		if (savedFiles[0] != null)
		{
				AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

				SQLiteDatabase db = dbHelper.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, courseId);
				values.put(AnytimeLearnDb.PAGE_NO_COL_NAME, Integer.parseInt(pageNo));
				values.put(AnytimeLearnDb.HAS_SOUND_COL_NAME, hasSound);
				values.put(AnytimeLearnDb.HAS_NEXT_COL_NAME, 0);
				values.put(AnytimeLearnDb.HAS_PREV_COL_NAME, 0);
				values.put(AnytimeLearnDb.PAGE_TYPE_COL_NAME, Integer.parseInt(contentUrls[3]));
				
				long insertValue = db.insert(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, null, values);
				System.out.println("Insert value = " + insertValue);
				db.close(); // Closing database connection			
		}
		
		savedFiles[2] = contentUrls[2]; //this is the page no.
		savedFiles[3] = contentUrls[3]; //this is the page type
		
		return savedFiles;
	} //downloadOnePageAndStoreLocally()
	
	@Override
	protected void onHandleIntent(Intent intent) {

		
		String trigger = intent.getStringExtra("Trigger");
		System.out.println("Intent trigger in onHandleIntent= " + trigger);
		
		String courseId = intent.getStringExtra("courseId");
		String simId = intent.getStringExtra("simId");
		
		String pageDirection = intent.getStringExtra("pageDirection");
		boolean localCache = intent.getBooleanExtra("cacheStatus", false);
		
		cacheStatus  = intent.getBooleanExtra(getString(R.string.CACHE_STATE), false);
		
		System.out.println("Page Direction = " + pageDirection);
		
		//load the key for encryption
		//deviceKey = getSharedPreferences(PREFS_NAME, 0).getString(DEVICE_KEY, null).getBytes();
		//String deviceKeyString = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(RegistrationActivity.DEVICE_KEY, null);
		String deviceKeyString = getSharedPreferences(getString(R.string.PREFS_NAME), 0).getString(getString(R.string.DEVICE_KEY), null);
		deviceKey = Base64.decode(deviceKeyString, Base64.DEFAULT);
		//storageDirectory = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(getString(R.string.STORAGE_LOCATION), null);
		storageDirectory = getSharedPreferences(getString(R.string.PREFS_NAME), 0).getString(getString(R.string.STORAGE_LOCATION), null);
		//System.out.println("Key retrieved in service = " + deviceKey);

/*
		if ((trigger != null) && (trigger.equals("CourseReset")))
		{
			//in future do not send whole intent but just the messenger object
			resetCourseAndSendReply(courseId, simId, intent);
			return;
		}
*/
		if ((trigger != null) && (trigger.equals("CourseListing")))
		{
			//in future do not send whole intent but just the messenger object
			courseListingReply(courseId, simId, intent.getStringExtra("pageNo"), intent);
			return;
		}
		
		if ((trigger != null) && (trigger.equals("TestPage")))
		{
			//in future do not send whole intent but just the messenger object
			testPageGetReply(courseId, simId, intent.getStringExtra(getString(R.string.CURRENT_PAGE_NO)), intent.getStringExtra(getString(R.string.TOTAL_PAGES)), intent);
			return;
		}
		
		if ((trigger != null) && (trigger.equals("AllTestPages")))
		{
			//in future do not send whole intent but just the messenger object
			downloadFullTest(courseId, simId, intent.getIntExtra("TotalPages", -1));
			return;
		}

		if ((trigger != null) && (trigger.equals("AllCoursePages")))
		{
			//in future do not send whole intent but just the messenger object
			downloadFullCourse(courseId, simId, intent.getIntExtra("TotalPages", -1));
			return;
		}


		//create ArrayBlockingQueue that will hold 5 elements, each containing 4 string
		ArrayBlockingQueue<String> fileContents = new ArrayBlockingQueue<String>(20);
		
		if ((pageDirection != null) && (pageDirection.equals("Previous")))
		{
			String pageNo = intent.getStringExtra("pageNo");
			String[] localPage = downloadNumberedPage(courseId, simId, pageNo, localCache);
			sendPageForDisplay(localPage, intent);
		}
		else
		{	//downloading next page
			//check the no. of lines already present in file denoting pages already downloaded
			readDataFromFile(fileContents);
			if (fileContents.size() == 0)
			{
System.out.println("Empty buffer need to download");				
				//read one pair of urls first
				String[] localPage = downloadOnePageAndStoreLocally(courseId, simId, localCache);
System.out.println("Return from pagedownload with text = " + localPage[0]);
				if (localPage[0] == null) //change null to indicate empty as queue does not accept null
					localPage[0] = "-1";
				fileContents.add(localPage[0]);
				if (localPage[1] == null) //change null to indicate empty as queue does not accept null
					localPage[1] = "-1";
				fileContents.add(localPage[1]);
			
				//add the page no.
				fileContents.add(localPage[2]);
				
				//add the page type
				fileContents.add(localPage[3]);
			}


System.out.println("Download done");			
			//send the top two strings from the internal buffer for text and sound urls
			Bundle extras = intent.getExtras();
			String[] urls = new String[4]; //2 strings are url, third is page no and fourth is pagetype.
			urls[0] = fileContents.poll(); //text url
			urls[1] = fileContents.poll(); //sound url
			urls[2] = fileContents.poll(); //this is page no.
			urls[3] = fileContents.poll(); //this is page type

		
			if (extras != null) {
System.out.println("Calling activity");			
				Messenger messenger = (Messenger) extras.get("MESSENGER");
				Message msg = Message.obtain();
//				msg.arg1 = result;
				msg.obj = urls;
				try {
					messenger.send(msg);
				} catch (android.os.RemoteException e1) {
					//	Log.w(getClass().getName(), "Exception sending message", e1);
					e1.printStackTrace();
				}
			}
		
			System.out.println("Before start of loop " + fileContents.size());
			//add as many urls as needed to make up 5 pages. 
			//trying with just 2 urls downloaded to see if removes voice breaking. that will make file contents size as 8
			while ((fileContents.size() != 8) && (!courseResetStatus)) //do not enter loop if course reset 
			{
				//read as many pair of urls to make buffer of 5
				String[] localPage = downloadOnePageAndStoreLocally(courseId, simId, localCache);
				System.out.println("Downloading in service at size " + fileContents.size());
				
				//if course reset during download then abandon page
				if (courseResetStatus)
					break;
			
				if (localPage[0] == null) //change null to indicate empty as queue does not accept null
					localPage[0] = "-1";
				fileContents.add(localPage[0]);
				if (localPage[1] == null) //change null to indicate empty as queue does not accept null
					localPage[1] = "-1";
				fileContents.add(localPage[1]);
			
				//add the page no.
				fileContents.add(localPage[2]);

				//add the page type
				fileContents.add(localPage[3]);
				
				if (isNextEnabled == 0)
					break;
			}
		
			//write data to file for next call to the service
			deleteFile("nextPageBuffer.txt"); //delete the file so that fresh load can start
			if (!courseResetStatus) //write only if courseReset is not called so that retrieved page not returned
			{
				try
				{
					FileOutputStream fOut = openFileOutput("nextPageBuffer.txt", MODE_WORLD_READABLE);        
					OutputStreamWriter osw = new OutputStreamWriter(fOut);          
					// 	Write the local file names to file
					int totalLines = fileContents.size();
		
					for (int i = 0; i < totalLines; i++)
						osw.write(fileContents.poll() + "\n");         

					osw.flush();        
					osw.close(); 		
				}
				catch (IOException ioe)        
				{
					ioe.printStackTrace();
				}
			} //if courseResetStatus is false
		} //else if it is a next page
		
		/*
		//get the data of Intent
//		Uri intentUri = intent.getData();
		String triggerFeature = intent.getStringExtra("Trigger");
		int numPagesToDownload = 0;		
				
		if (triggerFeature.equals("CourseStart"))
		{
			numPagesToDownload = 5;
			
		}
		else if (triggerFeature.equals("NextPage"))
		{
			numPagesToDownload = 1;
		}
		else
			numPagesToDownload = 0;
		
		
		
		
		//start download
		for (int i = 0; i < numPagesToDownload; i++)
		{
//			String courseContentStatusUrl = getString(R.string.baseURL) + "getContentStatus.php?courseId=" + courseId + "&simId=" + simId;
			String courseContentStatusUrl = getString(R.string.baseURL) + "1/1s.mp3"; 
			
			//get the no. of urls and if there is a next page
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGetText = new HttpGet(courseContentStatusUrl);
			try {
					HttpResponse execute = client.execute(httpGetText);
					InputStream content = execute.getEntity().getContent();

					
					BufferedInputStream buffer = new BufferedInputStream(content);
					
					FileOutputStream fSound = openFileOutput("1s.mp3", MODE_WORLD_READABLE);   
					byte buf[]=new byte[1024];
					int len;
					while((len=buffer.read(buf)) >0)
						fSound.write(buf,0,len);
					fSound.close();
					buffer.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			writeToMessageFile("1s.mp3");
		}
		*/
		/*
		Intent responseMessage = new Intent(this, CourseDisplay.class);
		responseMessage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
		startActivity(responseMessage);
		*/

//		writeToMessageFile(new Integer(currentCallNo).toString())
	}
	
	private void writeToMessageFile(String message)
	{
		
		try {         
			// catches IOException below        
			/* We have to use the openFileOutput()-method        
			 * * the ActivityContext provides, to        
			 * * protect your file from others and        
			 * * This is done for security-reasons.        
			 * * We chose MODE_WORLD_READABLE, because        
			 * *  we have nothing to hide in our file */                     
			FileOutputStream fOut = openFileOutput("samplefile.txt", MODE_WORLD_READABLE);        
			OutputStreamWriter osw = new OutputStreamWriter(fOut);          
			// Write the string to the file        
	//		osw.write(new Integer(currentCallNo).toString());         
			osw.write(message);         
			/* ensure that everything is         
			 * * really written out and close */        
			osw.flush();        
			osw.close(); 		
		}
		 catch (IOException ioe)        
		 {
			 ioe.printStackTrace();
		}
	}

}
