package in.AnytimeLearn;



import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.Window;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

public class TestViewer extends Activity {
	protected TextView testQuestion, answerAndMessages;
	protected RadioGroup optionsRadioGroup;
	protected Button nextButton, evaluateButton, previousButton;
	Intent downloadService;
	protected String currentTextUrl, currentSoundUrl, currentPageNo, currentPageType;
	boolean testLocallyCached = false;
	protected String subscriberId, testName;
	protected int testId, lastPage = -1, totalPages = -1, maxPageSeen = 0;
	protected int[] rightOption;
	protected String testType, answerExplanation;
	protected boolean multipleRight = false;
	protected RelativeLayout currentLayout;
	protected LinearLayout multipleRightChoice;
	protected byte[] deviceKey;
	ScrollView testViewerScrollLayout;
//	private ScaleGestureDetector mScaleDetector;
	private View titleView, pageNumberView;
	protected final int DEFAULT_TEXT_SIZE = 20;
	protected ProgressDialog loadProgress, initializeProgress;
	protected String storageDirectory = null;
	// Constants for Account   
	// The authority for the sync adapter's content provider    
	protected String AUTHORITY = null;    
	// An account type, in the form of a domain name    
	protected String ACCOUNT_TYPE = null;    
	// The account name    
	//public static final String ACCOUNT = "dummyaccount";    
	// Instance fields    
	Account mAccount;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LayoutInflater inflater = getLayoutInflater();
		testViewerScrollLayout = (ScrollView) inflater.inflate(R.layout.activity_test_viewer,null);

	//	setContentView(R.layout.activity_test_viewer);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(testViewerScrollLayout);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);
		
		testQuestion = (TextView)findViewById(R.id.questionText);
		optionsRadioGroup = (RadioGroup)findViewById(R.id.optionsGroup);
		answerAndMessages = (TextView)findViewById(R.id.answerAndNotifications);
		nextButton = (Button)findViewById(R.id.nextQuestion);
		evaluateButton = (Button)findViewById(R.id.evaluate);
		previousButton = (Button)findViewById(R.id.previous);
		titleView = findViewById(R.id.myTitle); 
		pageNumberView = findViewById(R.id.pageNumber);
	
		currentLayout = (RelativeLayout)findViewById(R.id.testLayout);
		
		//create a new linear layout to show multiple right choice options
		multipleRightChoice = new LinearLayout(this);
		multipleRightChoice.setOrientation(LinearLayout.VERTICAL);
		multipleRightChoice.setId(R.id.testViewerOptions); //temporary id as the code below works only when an id is set

		/*
		testQuestion.setText("Sample question");
		for (int count = 0; count < 4; count ++)
		{
			RadioButton optionButton = new RadioButton(this);
			optionButton.setText("Option " + count);
			options.addView(optionButton);
		} */
		
		//setting text size
		 testQuestion.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
		 answerAndMessages.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
	
		//setup account constants and get the existing account
	    AUTHORITY = getString(R.string.SYNC_AUTHORITY);
	    ACCOUNT_TYPE = getString(R.string.SYNC_ACCOUNT_TYPE);
		// Get an instance of the Android account manager        
		AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
		Account[] allAnytimeLearnAccounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
		if (allAnytimeLearnAccounts != null)
			mAccount = allAnytimeLearnAccounts[0]; //there is only one account that we need
		else
			System.out.println("No account exists");

		/* Below code did not work. Replaced by a larger text size above.
		//create gesture detector
		mScaleDetector = new ScaleGestureDetector(testViewerScrollLayout.getContext(), new OnScaleGestureListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			public void onScaleEnd(ScaleGestureDetector detector) {
		    	testViewerScrollLayout.setScaleX((float) 2.0);
		    	testViewerScrollLayout.setScaleY((float) 2.0);
		    	System.out.println("On scale end called");
		    }
		    public boolean onScaleBegin(ScaleGestureDetector detector) {
		    	System.out.println("On scale begin called");
		        return true;
		    }
		    public boolean onScale(ScaleGestureDetector detector) {
		    	System.out.println("On scale called");
		        return false;
		    }
		});
		
		testViewerScrollLayout.setOnTouchListener(new OnTouchListener()
		{
			public boolean onTouchEvent(MotionEvent event) {
			    mScaleDetector.onTouchEvent(event);
			    return true;
			}

			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		*/
		//check if the application's key exists. If not disallow load of information
		if (!LoadEncryptionKeyAndStorageLoc())
			displayMessage(getString(R.string.START_HTML)+ "Device key not found" + getString(R.string.END_HTML));
			
		//disable buttons till first page download is done
		evaluateButton.setEnabled(false);
		previousButton.setEnabled(false);
			
		initializeTest();
		
	} //onCreate()
	
    /** Called when the activity is destroyed. */
    @Override
    public void onDestroy() {
        super.onDestroy();
        
        //store current page no. and total pages in db
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		//Update the row corresponding to this course id only. The row would be created in initializeTest
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
		Date currentDate = new Date();
		String formattedCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDate);

		ContentValues values = new ContentValues();
		values.put(AnytimeLearnDb.LAST_DISPLAY_PAGE, lastPage); // put current page
		values.put(AnytimeLearnDb.NUM_PAGES, totalPages); // put totalPage
		values.put(AnytimeLearnDb.EVALUATION_TYPE_COL_NAME, testType); // put evaluation Type
		values.put(AnytimeLearnDb.MAX_PAGE_SEEN, maxPageSeen); //setting max page seen
		values.put(AnytimeLearnDb.LAST_ACCESS_DATETIME, formattedCurrentDate); // put current time for last access
		long updateValue = db.update(AnytimeLearnDb.COURSE_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});
    
System.out.println("Update value in destroy = " + updateValue);
		db.close();

		//keep a call to sync
	     // Pass the settings flags by inserting them in a bundle        
		Bundle settingsBundle = new Bundle();        
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);        
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);        
		/** Request the sync for the default account, authority, and         
		 * * manual sync settings         */        
		ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);			

		//also update page no. on server
		//updating the last page on the server if course id is not null
		UpdatePageNoAsyncTask pageUpdateTask = new UpdatePageNoAsyncTask();
		pageUpdateTask.execute();
    }

    private void checkDownloadStatusAndDownloadAll()
    {
    	int numRecords = -1;
    	
    	//no need to check cache status
    //	if (testLocallyCached)
    	{
    		//check no. of page records against total pages
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
			
			//read the last page stored
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME }, 
					AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
					new String[] {Integer.toString(testId)}, null, null, null, null);
			if (cursor != null)
			{
				numRecords = cursor.getCount();
				
				if (numRecords != totalPages) //total pages is already filled up before this
					commenceTestDownload();

				cursor.close();
			}
			db.close();
    	}
    } //checkDownloadStatusAndDownloadAll()
    
    private void commenceTestDownload()
    {
	//	Handler handler = new TestDisplayHandler();

System.out.println("communicating with service for all downloads");		

	//	Messenger messenger = new Messenger(handler);
		downloadService = new Intent(this, CourseRetrieverDownloadOptimized.class);
	//	downloadService.putExtra("MESSENGER", messenger);
		downloadService.putExtra("courseId", Integer.toString(testId)); 
		downloadService.putExtra("simId", subscriberId);
		downloadService.putExtra("Trigger", "AllTestPages");
		downloadService.putExtra("TotalPages", totalPages);
		//downloadService.putExtra("cacheStatus", courseLocallyCached); 
		startService(downloadService);		
    } //commenceTestDownload()

    //returns false if encryptionKey cannot be loaded. True otherwise
	private boolean LoadEncryptionKeyAndStorageLoc()
	{
		SharedPreferences settings = getSharedPreferences(getString(R.string.PREFS_NAME), 0);
		//	key already created during registration
		   
		String deviceKeyString = settings.getString(getString(R.string.DEVICE_KEY), null);
		if (deviceKeyString == null)
		{
		  System.out.println("Device key not found");
		  return false;
		}
		  
		deviceKey = Base64.decode(deviceKeyString, Base64.DEFAULT);
		
		storageDirectory = settings.getString(getString(R.string.STORAGE_LOCATION), null);
		if (storageDirectory == null)
		{
			System.out.println("Storage directory not found");
			return false;
		}
		
		return true;
		//  System.out.println("Key retrieved in activity = " + deviceKeyString.getBytes());

	} //LoadEncryptionKeyAndStorageLoc()

	private void initializeTest()
	{
		//comment below statement if caching is to be tested
		testLocallyCached = getIntent().getBooleanExtra(getString(R.string.CACHE_STATE), false);
		testId = Integer.parseInt(getIntent().getStringExtra("COURSE_ID"));
		testName = getIntent().getStringExtra("COURSE_NAME");
		
		subscriberId = getIntent().getStringExtra("SIM_ID");
		
		//get the test attributes such as last page, total page
		getTestAttributes();
		
		if (!testLocallyCached) //put a row to indicate that course first downloaded on today's date/time
		{
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			//delete all rows of the page content that exist
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			String deleteWhereClause = AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?";
			db.delete(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, deleteWhereClause, new String[]{Integer.toString(testId)});
			
			//get the current date
			Date currentDate = new Date();
			String formattedCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDate);
			ContentValues values = new ContentValues();
			values.put(AnytimeLearnDb.DOWNLOAD_DATE_COL_NAME, formattedCurrentDate); // put current time
			values.put(AnytimeLearnDb.COURSE_NAME_COL_NAME, testName); // put course name
			values.put(AnytimeLearnDb.COURSE_TYPE_COL_NAME, "T"); // put type as test
			// Update Row
			long updateValue = db.update(AnytimeLearnDb.COURSE_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});
			System.out.println("Update value = " + updateValue);
			
			//if update did not work, insert a new row
			if (updateValue != 1) //only 1 row should have been updated
			{
				values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, testId);
				long insertValue = db.insert(AnytimeLearnDb.COURSE_TABLE_NAME, null, values);
				System.out.println("Insert value = " + insertValue);
			}
			db.close(); // Closing database connection
			
		} //end of row insertion in case course is first time downloaded


		//disable the answer and next button
		answerAndMessages.setVisibility(View.INVISIBLE);
		nextButton.setVisibility(View.INVISIBLE);

    	//set the title to parameter course name
    	if (testName != null)
    		((TextView)titleView).setText("Anytime Learn - " + testName);

	} //initializeTest()
	
	private void getTestAttributes()
	{
		//first fill from db if the course is locally cached
		if (testLocallyCached)
		{
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
		
			//read the last page stored
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			Cursor cursor = db.query(AnytimeLearnDb.COURSE_TABLE_NAME, new String[] { AnytimeLearnDb.NUM_PAGES, AnytimeLearnDb.LAST_DISPLAY_PAGE, AnytimeLearnDb.EVALUATION_TYPE_COL_NAME}, 
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {Integer.toString(testId)}, null, null, null, null);
			if (cursor != null)
			{
				if (cursor.moveToFirst()) 	//means page row exists.
				{
					totalPages =  cursor.getInt(0);
					lastPage = cursor.getInt(1);
					testType = cursor.getString(2);
				}
				cursor.close();
			}
			
			db.close();
		}

		//show progress bar for initializing test
		initializeProgress = new ProgressDialog(this);
		initializeProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		initializeProgress.setMessage("Initializing test..");
		initializeProgress.show();

		TestAttributeRetrieveAsyncTask testAttributeRetrieve = new TestAttributeRetrieveAsyncTask();

		testAttributeRetrieve.execute();
		
	} //getTestAttributes()
	
    
    /** Called when evaluate button is clicked  **/
	public void evaluateAnswer(View view)
	{
		String answerResponseText = "";
		
		//check if atleast one is answered
		boolean answerGiven = false;
		if (multipleRight)
		{
			 for (int count = 0; count < multipleRightChoice.getChildCount(); count++)
				 if (((CheckBox)multipleRightChoice.getChildAt(count)).isChecked())
				 {
					 answerGiven = true;
					 break;
				 }
		}
		else if (optionsRadioGroup.getCheckedRadioButtonId() != -1)
			answerGiven = true;
		
		boolean answerCorrectness = rightAnswerEvaluation(); 
		SpannableString evaluationText = null; //initialized to response to be given;
		
		//decide what to display
		if (!answerGiven)
			answerResponseText = "Please select one of the options to answer";
		else if (testType.equals("R")) //only right answer allowed
		{
			if (answerCorrectness)
			{
				evaluationText = new SpannableString("Correct Answer. Press next to continue");
				evaluationText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.deepSkyBlue)), 0, evaluationText.length(), 0);
				//answerResponseText = "Correct Answer. Press next to continue";
				answerResponseText = "\n" + answerExplanation;
				nextButton.setVisibility(View.VISIBLE);
				evaluateButton.setEnabled(false);
				//	increment last page only on correct answer
				lastPage++;
				
				//set max page to the last page answered
				if (lastPage > maxPageSeen)
					maxPageSeen = lastPage;

			}
			else
			{
				evaluationText = new SpannableString("Wrong answer. Please re-attempt the question");
				evaluationText.setSpan(new ForegroundColorSpan(Color.RED), 0, evaluationText.length(), 0);

			//	answerResponseText = "Wrong answer. Please re-attempt the question";
			}
		}
		else //assume this is test type "E" which means evaluate and provide the right answer plus explanation
		{
			if (answerCorrectness)
			{
				evaluationText = new SpannableString("Correct Answer. Press next to continue");
				evaluationText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.deepSkyBlue)), 0, evaluationText.length(), 0);

				//				answerResponseText = "Correct Answer. Press next to continue";
			}
			else
			{
				evaluationText = new SpannableString("Wrong answer");
				evaluationText.setSpan(new ForegroundColorSpan(Color.RED), 0, evaluationText.length(), 0);
			//	answerResponseText = "Wrong answer.";
			}
			answerResponseText = "\n" + answerExplanation;
			nextButton.setVisibility(View.VISIBLE);
			evaluateButton.setEnabled(false);
			//	increment page no. for next question
			lastPage++;
			
			//set max page to the last page answered
			if (lastPage > maxPageSeen)
				maxPageSeen = lastPage;

		} //test type = "E"
			
		answerAndMessages.setVisibility(View.VISIBLE);
		if (evaluationText != null)
		{
			answerAndMessages.setText(evaluationText);
			if (answerResponseText != null)
				answerAndMessages.append(answerResponseText);
		}
		else
			answerAndMessages.setText(answerResponseText);
		
//		System.out.println("Checked button id = " + optionsRadioGroup.getCheckedRadioButtonId());
	} //evaluateAnswer()
	
	private boolean rightAnswerEvaluation()
	{
		if (multipleRight)
		{
			int rightOptionCount = 0;
			int numCorrectAnswers = 0, numWrongAnswers = 0;
			for (int count = 0; count < multipleRightChoice.getChildCount(); count++)
			{
				CheckBox optionCheckBox = (CheckBox)multipleRightChoice.getChildAt(count);
				int boxCount = count+1; //child at 0 is option #1
				if (boxCount > rightOption[rightOptionCount]) //see if we have reached the next right option
					if ((rightOptionCount+1) < rightOption.length)
						rightOptionCount++;
				if (optionCheckBox.isChecked())
					if (rightOption[rightOptionCount] == boxCount)
						numCorrectAnswers++;
					else
						numWrongAnswers++;
			}
			if ((numCorrectAnswers == rightOption.length) && (numWrongAnswers == 0))
				return true; //indicates only correct answers and all correct given
			else
				return false;
		} //if multiple Right
		else //only 1 correct answer
		{	
			int answeredResponse = optionsRadioGroup.getCheckedRadioButtonId();

			if (answeredResponse == rightOption[0])
				return true;
		}
		
		return false; //any other condition return false
	} //rightAnswerEvaluation
	
	public void loadNextQuestion(View view)
	{
		nextButton.setVisibility(View.INVISIBLE);
		loadNextQuestion();
	}

	public void loadPreviousQuestion(View view)
	{
		if (lastPage >= 1)
		{	
			lastPage--; //reduce page no. and call the loadNextQuestion
			loadNextQuestion();
		}
		else
		{
			displayMessage("No previous question. Press next to continue");
			lastPage = 0;
			nextButton.setVisibility(View.VISIBLE);
		}
	}

	protected void loadNextQuestion()
	{
		/*
		//call course retriever with a specific page no. to retrieve
		downloadService.putExtra(getString(R.string.CURRENT_PAGE_NO), currentPageNo);
		downloadService.putExtra(getString(R.string.TOTAL_PAGES), totalPages);
		startService(downloadService);		
		*/
System.out.println("load Next question called");
		if (lastPage < totalPages)
		{
			boolean localCacheFound = false;
			
			//read the cache from db
			//check if page exists locally irrespective of cache status because it might be that pages are downloaded in background after first page
			
			//if (testLocallyCached)
			{	
				AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
				
				//read the last page stored
				SQLiteDatabase db = dbHelper.getReadableDatabase();

				Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME, AnytimeLearnDb.HAS_SOUND_COL_NAME, AnytimeLearnDb.HAS_NEXT_COL_NAME, AnytimeLearnDb.HAS_PREV_COL_NAME, AnytimeLearnDb.PAGE_TYPE_COL_NAME}, 
						AnytimeLearnDb.COURSE_ID_COL_NAME + "=? AND " + AnytimeLearnDb.PAGE_NO_COL_NAME + "=?",
						new String[] {Integer.toString(testId), Integer.toString(lastPage+1)}, null, null, null, null);
				if (cursor != null)
				{
					if (cursor.moveToFirst()) 	//means page row exists.
					{
						localCacheFound = true;
						currentTextUrl = testId + "_" + cursor.getInt(0);
					}
				}
				cursor.close();
				db.close();
			}
			
		//	localCacheFound = false;
			System.out.println("Local cached found status = " + localCacheFound);
			
			if (!localCacheFound)
			{
				//retrieve specifc page
				TestPageRetrieveAsyncTask pageRetrieveTask = new TestPageRetrieveAsyncTask();
				pageRetrieveTask.execute();
				//show progress bar here
				//show progress bar here
		    	loadProgress = new ProgressDialog(this);
		    	loadProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		    	loadProgress.setMessage("Loading test questions..");
		    	loadProgress.show();

			}
			else
				displayCurrentUrls();
		} //if (lastPage < totalPage)
		else
		{
			displayMessage("Last question reached. Use back to see previous question");
			//enable previous button for situations where last page is the page loaded
			previousButton.setEnabled(true);
		}
	} //loadNextQuestion()
	
	//this method is called to display a message and hide evaluate and other buttons
	private void displayMessage(String messageToDisplay)
	{
		//	display the data
		testQuestion.setText("".toCharArray(), 0, 0);
		answerAndMessages.setVisibility(View.VISIBLE);
		answerAndMessages.setText(messageToDisplay.toCharArray(), 0, messageToDisplay.length());
		//delete existing options
		optionsRadioGroup.removeAllViews();
		multipleRightChoice.removeAllViews();
		evaluateButton.setEnabled(false);
		nextButton.setVisibility(View.INVISIBLE);
	}
	
		
	private void displayCurrentUrls()
	{
		//disable the answer 
		answerAndMessages.setVisibility(View.INVISIBLE);
		
		//enable evaluate button for next question
		evaluateButton.setEnabled(true);
		
		//enable previous button
		previousButton.setEnabled(true);

		//uncheck all options
		optionsRadioGroup.clearCheck();
		
		if (multipleRight) //remove the checkbox group
		{
			if (multipleRightChoice != null)
			{
				multipleRightChoice.removeAllViews();
				currentLayout.removeView(multipleRightChoice);
			}
		}
		else //remove all radio boxes
			optionsRadioGroup.removeAllViews();
		
		//make all option views invisible
		multipleRightChoice.setVisibility(View.INVISIBLE);
		optionsRadioGroup.setVisibility(View.INVISIBLE);

		multipleRight = false;
		
		try
		{
			//read the first two lines of the file
			//File questionFile = new File(getExternalFilesDir(null), currentTextUrl);
			File questionFile = new File(storageDirectory, currentTextUrl);
			//FileReader questionFileReader = new FileReader(questionFile);
			
			//setup decrytion for data
			SecretKeySpec decryptKey = new SecretKeySpec(deviceKey, "AES");
			// 	Create the cipher
			Cipher cDecrypt = Cipher.getInstance("AES");
			cDecrypt.init(Cipher.DECRYPT_MODE, decryptKey);
/*
	InputStream testStream = new CipherInputStream(new FileInputStream(questionFile), cDecrypt);
	int content;
	while ((content = testStream.read()) != -1) {
		// convert to char and display it
		System.out.print((char) content);
	}
	testStream.close();
*/
			InputStream fIn = new CipherInputStream(new FileInputStream(questionFile), cDecrypt);

			
//			BufferedReader bufferedQuestionReader = new BufferedReader(questionFileReader);
			BufferedReader bufferedQuestionReader = new BufferedReader(new InputStreamReader(fIn));
		
			String questionLengthStr = bufferedQuestionReader.readLine(); //first line is length of question
			String numberOfOptionsStr = bufferedQuestionReader.readLine(); //next line is no. of options
		
			//	convert the two to ints
			int questionLength = Integer.parseInt(questionLengthStr);
			int numberOfOptions = Integer.parseInt(numberOfOptionsStr);
		
			//	read the question as no. of characters
			char[] questionBuf = new char[questionLength];
			//bufferedQuestionReader.read(questionBuf, 0, questionLength);
			// for some reason lolipop is not reading a buffer cleanly with length specified. Therefore using single character read
			for (int questionBufLength = 0; questionBufLength < questionLength; questionBufLength++)
				questionBuf[questionBufLength] = (char)bufferedQuestionReader.read();
		
			//read the no. of new lines as options
			String[] options = new String[numberOfOptions];
//System.out.println("Number of options = " + numberOfOptions);
			for (int count = 0; count < numberOfOptions; count++)
			{
				//read the length of option given till end of line
				String optionLengthStr = bufferedQuestionReader.readLine(); 
				int optionLength = Integer.parseInt(optionLengthStr);
			
				//read characters as per length and assign to option string array
				char[] thisOption = new char[optionLength];
				//bufferedQuestionReader.read(thisOption, 0, optionLength);
				// for some reason lolipop is not reading a buffer cleanly with length specified. Therefore using single character read
				for (int optionLengthCount = 0; optionLengthCount < optionLength; optionLengthCount++)
					thisOption[optionLengthCount] = (char)bufferedQuestionReader.read();
				
				options[count] = String.valueOf(thisOption);
				//System.out.println("Option string = " + options[count]);
			}
		
			String rightOptionStr = bufferedQuestionReader.readLine();
			//System.out.println("right option = " + rightOptionStr);
			
			String[] rightOptionBreakup = rightOptionStr.split(",");
			rightOption = new int[rightOptionBreakup.length];
			
			for (int count = 0; count < rightOptionBreakup.length; count++)
				rightOption[count] = Integer.parseInt(rightOptionBreakup[count]);
		
			if (rightOption.length > 1)
				multipleRight = true;
			
			String explanationLengthStr = bufferedQuestionReader.readLine();
			int explanationLength = Integer.parseInt(explanationLengthStr);
			if (explanationLength != 0)
			{
				//	read the question as no. of characters
				char[] explanationBuf = new char[explanationLength];
			//	bufferedQuestionReader.read(explanationBuf, 0, explanationLength);
			// for some reason lolipop is not reading a buffer cleanly with length specified. Therefore using single character read
				for (int explanationBufCount = 0; explanationBufCount < explanationLength; explanationBufCount++)
					explanationBuf[explanationBufCount] = (char)bufferedQuestionReader.read();
				answerExplanation = String.valueOf(explanationBuf);
			// System.out.println("Explanation = " + answerExplanation);
			}
			else
				answerExplanation = "";
		
			//	display the data
			testQuestion.setText(questionBuf, 0, questionLength);
		
			//delete existing options
			//optionsRadioGroup.removeAllViews();
			
			LayoutParams optionLayoutParam = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
			
			//display the options either as a radio group or check box
			if (multipleRight)
			{
				//optionsRadioGroup.setVisibility(View.INVISIBLE); //disable radio group
				
				multipleRightChoice.setVisibility(View.VISIBLE);

				//add checkboxes
				for (int count = 0; count < numberOfOptions; count++)
				{
					CheckBox optionCheckBox = new CheckBox(currentLayout.getContext());
					optionCheckBox.setText(options[count]);
					optionCheckBox.setTextColor(getResources().getColor(R.color.blackText));
					optionCheckBox.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
					if ((count % 2) == 0) //odd row as count starts from 0 for row #1
						optionCheckBox.setBackgroundColor(getResources().getColor(R.color.lightGrey));
					optionCheckBox.setLayoutParams(optionLayoutParam);
					optionCheckBox.setId(count+1);
					multipleRightChoice.addView(optionCheckBox);
				}

				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.BELOW, R.id.questionText);
				currentLayout.addView(multipleRightChoice, params);

				//show buttons relative to last option
				params = (RelativeLayout.LayoutParams)previousButton.getLayoutParams();
				params.addRule(RelativeLayout.BELOW, 1234);
			}
			else
			{
				optionsRadioGroup.setVisibility(View.VISIBLE);
				
				for (int count = 0; count < numberOfOptions; count++)
				{
					RadioButton optionButton = new RadioButton(this);
					optionButton.setText(options[count]);
					optionButton.setId(count+1);
					optionButton.setTextColor(getResources().getColor(R.color.blackText));
					optionButton.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
					if ((count % 2) == 0) //odd row as count starts from 0 for row #1
						optionButton.setBackgroundColor(getResources().getColor(R.color.lightGrey));
					optionButton.setLayoutParams(optionLayoutParam);
					optionsRadioGroup.addView(optionButton);
				}
				
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)previousButton.getLayoutParams();
				params.addRule(RelativeLayout.BELOW, R.id.optionsGroup);
			}
			
			bufferedQuestionReader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		//set the page number values
    	((TextView)pageNumberView).setText((lastPage+1) + "/" + totalPages);

		testViewerScrollLayout.scrollTo(0, 0); //show the top of the screen always
		
	} //displayCurrentUrls()

	private class TestAttributeRetrieveAsyncTask extends AsyncTask<Object, Void, Object[]> 
	{   
			
			@Override
			protected Object[] doInBackground(Object... urls) {
				String response = "";
				
				//get the last page and total page and any other test attributes
				String testAttributeUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestAttributes.php";
				//set the name value parameters for call
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
				nameValuePairs.add(new BasicNameValuePair("CourseId",Integer.toString(testId)));            
				nameValuePairs.add(new BasicNameValuePair("SimId",subscriberId));

				PostRequestPerform testAttributeRequest = new PostRequestPerform(testAttributeUrl, nameValuePairs);
				InputStream content = testAttributeRequest.getResponse();

				if (content != null)
				{
					//reply expected in form lastPage&totalPage
					BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
					try
					{
						String s = "";
						while ((s = buffer.readLine()) != null)
							response += s;
		//System.out.println("book mark response " + response);
					}
					catch (IOException ioe)
					{
						ioe.printStackTrace();
						//lastPage = totalPages = -1; //commenting to avoid overwriting of value from db. We ignore exception and let value remain -1
						return null;
					}
			
					String[] testAttributes = response.split("&");

					lastPage = Integer.parseInt(testAttributes[0]);
					totalPages = Integer.parseInt(testAttributes[1]);
					testType = testAttributes[2];
				}
						
				return null;	
			} //doInBackground()

			@Override
			protected void onPostExecute(Object[] pageData) 
			{
				initializeProgress.dismiss();
				
				//System.out.println("last page = " + lastPage + " total page = " + totalPages);
				if (lastPage == -1) //means details could not be retrieved and are not locally available also
				{
					displayMessage("Problems in connecting to server. Please verify your Internet connection");
					return;
				} //if (lastPage == -1)

				loadNextQuestion();
				
				//check if all pages exist and if not start download
				checkDownloadStatusAndDownloadAll();

				//show message here if both could not be retrieved
			} //onPostExecute
	} //TestAttributeRetrieveAsyncTask


	private class TestPageRetrieveAsyncTask extends AsyncTask<Object, Void, Object[]> 
	{   
			String testPageName = null;
			
			@Override
			protected Object[] doInBackground(Object... urls) {
		
				//get the required page
				String testQuestionPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestQuestion.php";
				//temporary comment to try out on a different page.
				//String testQuestionPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestQuestion_Trial.php";
				//set the name value parameters for call
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
				nameValuePairs.add(new BasicNameValuePair("CourseId",Integer.toString(testId)));            
				nameValuePairs.add(new BasicNameValuePair("PageNo", Integer.toString(lastPage + 1)));            

				PostRequestPerform testQuestionPageRequest = new PostRequestPerform(testQuestionPageUrl, nameValuePairs);
				InputStream content = testQuestionPageRequest.getResponse();

				if (content != null)
				{
					//create the file to store
					testPageName = testId+"_"+ (lastPage+1); 
					System.out.println("Test file name = " + testPageName);
				
					try
					{
						//File questionTextFile = new File(getExternalFilesDir(null), testPageName);
						File questionTextFile = new File(storageDirectory, testPageName);
						FileOutputStream questionFileOutputStream= new FileOutputStream(questionTextFile);

						SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");
						// Create the cipher
						Cipher cEncrypt = Cipher.getInstance("AES");
						cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
						CipherOutputStream encrypytedQuestionFile = new CipherOutputStream(questionFileOutputStream, cEncrypt); 


						BufferedInputStream buffer = new BufferedInputStream(content);
						byte buf[]=new byte[10240];
						int len;
						while((len=buffer.read(buf)) >0)
							encrypytedQuestionFile.write(buf,0,len);
				
						encrypytedQuestionFile.close();
						questionFileOutputStream.close();
					}
					catch (Exception e)
					{
						testPageName = null;
						e.printStackTrace();
					}
				
					//	make the entry for the page in the local db
					//save the page details locally if required
					AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
						
					SQLiteDatabase db = dbHelper.getWritableDatabase();
					ContentValues values = new ContentValues();
					values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, testId);
					values.put(AnytimeLearnDb.PAGE_NO_COL_NAME, lastPage + 1);
					values.put(AnytimeLearnDb.HAS_SOUND_COL_NAME, 0);
					values.put(AnytimeLearnDb.HAS_NEXT_COL_NAME, 0);
					values.put(AnytimeLearnDb.HAS_PREV_COL_NAME, 0);
					values.put(AnytimeLearnDb.PAGE_TYPE_COL_NAME, 0);
						
					long insertValue = db.insert(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, null, values);
					System.out.println("Insert value for test page= " + insertValue);
					db.close(); // Closing database connection
				}
				
				return null;
					
		} //doInBackground()

		@Override
		protected void onPostExecute(Object[] pageData) 
		{
			//show progress bar here
	    	loadProgress.dismiss();

			if (testPageName != null)
			{
				currentTextUrl = testPageName;  
				displayCurrentUrls();
			}
		} //onPostExecute
	} //TestAttributeRetrieveAsyncTask	
	
	private class UpdatePageNoAsyncTask extends AsyncTask<Object, Void, Object[]> 
	{   
			
			@Override
			protected Object[] doInBackground(Object... urls) {
				String response = "";

				//update the last page no. on server
				String pageResetUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "updatePageNoPost.php";
				//set the name value parameters for call
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);            
				nameValuePairs.add(new BasicNameValuePair("CourseId",Integer.toString(testId)));            
				nameValuePairs.add(new BasicNameValuePair("SimId",subscriberId));
				nameValuePairs.add(new BasicNameValuePair("PageNo", Integer.toString(lastPage)));

				PostRequestPerform pageNoUpdateRequest = new PostRequestPerform(pageResetUrl, nameValuePairs);
				InputStream responseFromPageUpdate = pageNoUpdateRequest.getResponse();

				//discard response
					
				return null;	
			} //doInBackground()

			@Override
			protected void onPostExecute(Object[] pageData) 
			{
		System.out.println("Page no. updated");
				//do nothing here
			} //onPostExecute
	} //TestAttributeRetrieveAsyncTask
	
} //TestViewer
