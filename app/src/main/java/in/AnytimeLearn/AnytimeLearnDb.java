package in.AnytimeLearn;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AnytimeLearnDb extends SQLiteOpenHelper{
	 private static final int DATABASE_VERSION = 4;
	 public static final String COURSE_TABLE_NAME = "localCourses";    
	 public static final String COURSE_ID_COL_NAME = "courseId";
	 public static final String COURSE_NAME_COL_NAME = "courseName";
	 public static final String COURSE_TYPE_COL_NAME = "courseType";
	 public static final String EVALUATION_TYPE_COL_NAME = "evaluationType";
	 public static final String DOWNLOAD_DATE_COL_NAME = "downloadDate";
	 public static final String NUM_PAGES = "numberPages";
	 public static final String LAST_DISPLAY_PAGE = "lastDisplayPage";
	 public static final String MAX_PAGE_SEEN = "maxPage";
	 public static final String LAST_ACCESS_DATETIME = "lastAccessDateTime";
	 public static final String MINUTES_SPENT_ON_COURSE = "minutesSpentOnCourse";
	 public static final String DATABASE_NAME = "AnytimeLearn Db";

	 public static final String PAGE_INFO_TABLE_NAME = "localPageInfo";    
	 public static final String PAGE_NO_COL_NAME = "pageNo";
	 public static final String HAS_SOUND_COL_NAME = "hasSound";
	 public static final String HAS_PREV_COL_NAME = "hasPrevious";
	 public static final String HAS_NEXT_COL_NAME = "hasNext";
	 public static final String PAGE_TYPE_COL_NAME = "pageType";

	 public static final String EXAM_TABLE_NAME = "localExams";    
	 public static final String EXAM_DURATION_MINUTES = "examDurationMinutes";    
	 public static final String USED_MINUTES = "elapsedMinutes";    
	 public static final String LAST_DISPLAY_QUESTION_NO = "lastDisplayQuestionNumber";    
	 public static final String NUM_QUESTIONS = "numQuestions";    
	 public static final String EXAM_STATUS = "examStatus";
	 public static final String EXAM_START_TIME = "examStartTime";


	 public static final String EXAM_QUESTION_TABLE_NAME = "examQuestions";    
	 public static final String QUESTION_SEQUENCE_NO = "questionSequenceNo";    
	 public static final String QUESTION_PAGE_NO = "questionPageNumber";    
	 public static final String GIVEN_ANSWER_OPTION = "givenAnswerOption";    

	 public static final char EXAM_STATUS_ACTIVE = 1;
	 public static final char EXAM_STATUS_COMPLETE = 2;
	 
	 private static final String COURSE_TABLE_CREATE =  "CREATE TABLE " + COURSE_TABLE_NAME 
			 + " (" +                COURSE_ID_COL_NAME + " varchar(5), " 
			 +                COURSE_NAME_COL_NAME + " varchar(50)," 
			 +                COURSE_TYPE_COL_NAME + " char(1)," 
			 +                EVALUATION_TYPE_COL_NAME + " char(1)," 
			 +                DOWNLOAD_DATE_COL_NAME + " datetime," 
			 +				  LAST_DISPLAY_PAGE + " int(4),"
			 +				  NUM_PAGES + " int(4),"
			 +				  MAX_PAGE_SEEN + " int(4),"
			 +				  LAST_ACCESS_DATETIME + " datetime,"
			 +				  MINUTES_SPENT_ON_COURSE + " int(2)"
			 +		");";
	 
	 private static final String PAGE_INFO_TABLE_CREATE =  "CREATE TABLE " + PAGE_INFO_TABLE_NAME 
			 + " (" +               COURSE_ID_COL_NAME + " varchar(20), " 
			 +                		PAGE_NO_COL_NAME + " int, " 
			 +                		HAS_SOUND_COL_NAME + " int, " 
			 +                		HAS_PREV_COL_NAME + " int, " 
			 +                		HAS_NEXT_COL_NAME + " int, " 
			 +                		PAGE_TYPE_COL_NAME + " int);";

	 
	 private static final String EXAM_TABLE_CREATE =  "CREATE TABLE " + EXAM_TABLE_NAME 
			 + " (" +                COURSE_ID_COL_NAME + " varchar(5), " 
			 +                COURSE_NAME_COL_NAME + " varchar(50)," 
			 +				  LAST_DISPLAY_QUESTION_NO + " int(4),"
			 +				  NUM_QUESTIONS + " int(4),"
			 +				  EXAM_DURATION_MINUTES + " int(4),"
			 +				  USED_MINUTES + " int(4),"
			 +				  EXAM_STATUS + " char(1)," //1 = active, 2 = complete but not uploaded
			 +				  EXAM_START_TIME + " varchar(25)" //field used when exam done in continuous mode. Saving in varchar mode to avoid formatting issues on devices
			 +		");";

	 private static final String EXAM_QUESTIONS_CREATE =  "CREATE TABLE " + EXAM_QUESTION_TABLE_NAME 
			 + " (" +                COURSE_ID_COL_NAME + " varchar(5), " 
			 +                QUESTION_SEQUENCE_NO + " int(2)," 
			 +                QUESTION_PAGE_NO + " int(2)," 
			 +				  GIVEN_ANSWER_OPTION + " varchar(15)"
			 +		");";

	 private static final String ADDITIONAL_COLUMN_MAX_PAGE_SEEN_COURSE_CREATE_v2 = 
			 "ALTER TABLE " + COURSE_TABLE_NAME + " ADD " + 	MAX_PAGE_SEEN + " int(4);";
	 private static final String ADDITIONAL_COLUMN_LAST_ACCESS_DATETIME_COURSE_CREATE_v2 = 
			 "ALTER TABLE " + COURSE_TABLE_NAME + " ADD " + 	LAST_ACCESS_DATETIME + " datetime;";
	 private static final String ADDITIONAL_COLUMN_MINUTES_SPENT_ON_COURSE_COURSE_CREATE_v2 = 
			 "ALTER TABLE " + COURSE_TABLE_NAME + " ADD " + 	MINUTES_SPENT_ON_COURSE + " int(2);";
	 private static final String ADDITIONAL_COLUMN_EXAM_STATUS_EXAM_TABLE_CREATE_v3 = 
			 "ALTER TABLE " + EXAM_TABLE_NAME + " ADD " + 	EXAM_STATUS + " char(1);";
	private static final String ADDITIONAL_COLUMN_EXAM_START_TIME_EXAM_TABLE_CREATE_v4 =
			"ALTER TABLE " + EXAM_TABLE_NAME + " ADD " + 	EXAM_START_TIME + " varchar(25);";

	 AnytimeLearnDb(Context context) 
	 {        
		 super(context, DATABASE_NAME, null, DATABASE_VERSION);    
	 }  
	 
	 @Override    
	 public void onCreate(SQLiteDatabase db) {        
		 db.execSQL(COURSE_TABLE_CREATE);    
		 db.execSQL(PAGE_INFO_TABLE_CREATE);
		 db.execSQL(EXAM_TABLE_CREATE);
		 db.execSQL(EXAM_QUESTIONS_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		//do nothing for now.
System.out.println("DB upgrade called" );
		switch (oldVersion)
		{
			case 1: 
//System.out.println("Old version = " + oldVersion);
				db.execSQL(EXAM_TABLE_CREATE);
				db.execSQL(EXAM_QUESTIONS_CREATE);
				db.execSQL(ADDITIONAL_COLUMN_MAX_PAGE_SEEN_COURSE_CREATE_v2);
				db.execSQL(ADDITIONAL_COLUMN_LAST_ACCESS_DATETIME_COURSE_CREATE_v2);
				db.execSQL(ADDITIONAL_COLUMN_MINUTES_SPENT_ON_COURSE_COURSE_CREATE_v2);
				break;
			case 2:
	System.out.println("Old version 2");
				db.execSQL(ADDITIONAL_COLUMN_EXAM_STATUS_EXAM_TABLE_CREATE_v3);
				db.execSQL(ADDITIONAL_COLUMN_EXAM_START_TIME_EXAM_TABLE_CREATE_v4);
				break;
			case 3:
				System.out.println("Old version 3");
				db.execSQL(ADDITIONAL_COLUMN_EXAM_START_TIME_EXAM_TABLE_CREATE_v4);
				break;
		}
		
	}

}
