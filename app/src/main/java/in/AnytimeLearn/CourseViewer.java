package in.AnytimeLearn;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

public class CourseViewer extends Activity {
	protected PopupWindow mPopupWindow;
	private WebView contentView;
	private WebViewClient courseViewClient; 
	protected Button nextButtonView, previousButtonView, indexButtonView;
	protected String subscriberId = null, courseId = null, courseName = null, courseModifyDate = null;
	protected boolean courseReset, lastPageReached, courseLocallyCached = false;
	private Intent downloadService;
//	protected Stack<String> previousFileStack, nextFileStack;
	MediaPlayer mMediaPlayer;
	protected Map<String, String> glossary, courseAttribMap;
	private Vector<String> courseIndex;
	protected byte[] deviceKey;
	protected String currentTextUrl, currentSoundUrl, currentPageNo, currentPageType;
	protected final String MAP_STORE= "storeMap";
	protected final String VECTOR_STORE= "storeVector";
	protected final String DO_NOTHING= "doNothing";
	private View titleView, pageNumberView;
	protected ProgressDialog initializeProgress, loadProgress;
	protected int lastPage = -1, totalPages = -1, maxPageSeen = 0;
	protected String storageDirectory = null;
	// Constants for Account   
	// The authority for the sync adapter's content provider    
	protected String AUTHORITY = null;    
	// An account type, in the form of a domain name    
	protected String ACCOUNT_TYPE = null;    
	// The account name    
	//public static final String ACCOUNT = "dummyaccount";    
	// Instance fields    
	Account mAccount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_course_viewer);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);

		//video commented
		/*
		contentView = (WebView) mainLayout.findViewById(R.id.contentView);
		nextButtonView = (Button) mainLayout.findViewById(R.id.nextButton);
		previousButtonView = (Button) mainLayout.findViewById(R.id.previousButton);
		titleView = findViewById(R.id.myTitle); 
		*/
		contentView = (WebView)findViewById(R.id.htmlCourseContentView);
		nextButtonView = (Button) findViewById(R.id.nextButton);
		previousButtonView = (Button) findViewById(R.id.previousButton);
		indexButtonView = (Button) findViewById(R.id.courseIndex);
		titleView = findViewById(R.id.myTitle); 
		pageNumberView = findViewById(R.id.pageNumber);

		mPopupWindow = new PopupWindow(contentView.getContext());
        mPopupWindow.setHeight(100);
        //mPopupWindow.setWidth(70);
        mPopupWindow.setWindowLayoutMode (ViewGroup.LayoutParams.FILL_PARENT, 0);
        
		Intent startingIntent = getIntent();
		subscriberId = startingIntent.getStringExtra("SIM_ID");
		courseId = startingIntent.getStringExtra("COURSE_ID");
		courseName = startingIntent.getStringExtra("COURSE_NAME");
		courseModifyDate = startingIntent.getStringExtra("COURSE_MODIFY_DATE");
		//comment below statement if caching is to be tested
		courseLocallyCached = getIntent().getBooleanExtra(getString(R.string.CACHE_STATE), false);
		
		
		//setup account constants and get the existing account
        AUTHORITY = getString(R.string.SYNC_AUTHORITY);
        ACCOUNT_TYPE = getString(R.string.SYNC_ACCOUNT_TYPE);
		// Get an instance of the Android account manager        
		AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
		Account[] allAnytimeLearnAccounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
		if (allAnytimeLearnAccounts != null)
			mAccount = allAnytimeLearnAccounts[0]; //there is only one account that we need
		else
			System.out.println("No account exists");
		
		System.out.println("Retrieving course with course Id = " + courseId);

	    courseViewClient = new WebViewClient()        
        {            
        	// Override page so it's load on my view only            
	    	//@Override           
        	public boolean shouldOverrideUrlLoading(WebView  view, String  url)            
        	{
        		String singleCaseUrl = url.toLowerCase();

        		if (singleCaseUrl.indexOf("http://correct") != -1) //used for answer to a question
        		{
//                	view.loadData("URL intercepted next button to be called", "text/html", null);
        			nextButtonView.performClick();
        			return true;
        		}

        		if (singleCaseUrl.indexOf("http://incorrect") != -1) //used for incorrect answer to a question
        		{
  //              	view.loadData("URL intercepted previous button to be called", "text/html", null);
        		//	previousButtonView.performClick();
                	displayCurrentUrls(); //Incorrect should just display the question page again
        			return true;
    			}

        		//to show glossary
        		if (singleCaseUrl.indexOf("http://glossary") != -1) //used to check for glossary
        		{
        			//remove earlier contents
        			if (mPopupWindow.isShowing())
        				mPopupWindow.dismiss();
        			
        	        TextView popUpText = new TextView(view.getContext());
        	       // view.loadData("Glossary clicked", "text/html", null);

            		URL urlClass;
            		try
            		{
            			urlClass = new URL(url);
            		}
            		catch (MalformedURLException mue)
            		{
//            			contentView.setInitialScale(STANDARD_ZOOM);
            			view.loadData(getString(R.string.START_HTML)+"Bad glossary indicator"+getString(R.string.END_HTML), "text/html", null);
            		
            			return false;
            		}
            		
            		String query = urlClass.getQuery();   
                //	view.loadData("Query string = " + query, "text/html", null);
            		Map<String, String> map = getQueryMap(query);
            		String term = map.get("GlossaryTerm");
            		
        	        //get the value for this content from glossary
        	        String elaboration = glossary.get(term);
        	        if (elaboration == null)
        	        	elaboration = term + " is not elaborated";
        	        
        	        popUpText.setText(elaboration);
        	        popUpText.append("\nClick outside this box to close");
        	        mPopupWindow.setContentView(popUpText);
        	        mPopupWindow.setFocusable(true);
        	        mPopupWindow.showAsDropDown(view, 0, 0);
        			return true;
    			}
        /* 		
        		//check if the url points to anytime or elsewhere. for other urls return false. special urls already handled above
        		//only course urls handled beyond this point
        		if (singleCaseUrl.indexOf(getString(R.string.baseURL).toLowerCase()) == -1)
        		{
        			//let other urls be handled by browser
        			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        			startActivity(browserIntent);        			
        			return true;
        		}
        	*/
        		/*
        		initializeCourse();

        		communicateWithServiceToGetPage();
        		*/
        		// Return true to override url loading (In this case do nothing).             
        		return true;            
        	}
        	
        	private Map<String, String> getQueryMap(String query)   
        	{   
        	    String[] params = query.split("&");   
        	    Map<String, String> map = new HashMap<String, String>();   
        	    for (String param : params)   
        	    {   
        	        String name = param.split("=")[0];   
        	        String value = param.split("=")[1];   
        	        map.put(name, value);   
        	    }   
        	    return map;   
        	} 
        };         
        
		contentView.getSettings().setJavaScriptEnabled(true);
		contentView.getSettings().setPluginsEnabled(true);
		contentView.getSettings().setLoadsImagesAutomatically(true);
		contentView.getSettings().setBuiltInZoomControls(true);
		contentView.getSettings().setLoadWithOverviewMode(true);
		contentView.getSettings().setUseWideViewPort(true);
		contentView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//		contentView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//		contentView.getSettings().setPluginState(PluginState.ON_DEMAND);
//		contentView.getSettings().setAllowFileAccess(true);
		contentView.setWebViewClient(courseViewClient);
		
		deleteFile("nextPageBuffer.txt"); //delete the file so that fresh load can start
		lastPageReached = false;
		courseReset = false;
		
		
		//read the previousPageBuffer into a stack
		//previousFileStack = new Stack<String>();
		//nextFileStack = new Stack<String>();
		
		//initialize variable for glossary
		glossary = new HashMap<String, String>();

		//initialize variable for course attributes
		courseAttribMap = new HashMap<String, String>();
		
		//intitialze variable for course index
		courseIndex = new Vector<String>();
				
		//readPreviousContentsIntoStack();
		
		//Media player instance used in playback
		mMediaPlayer = new MediaPlayer();
		mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() 
		{      
			public void onPrepared(MediaPlayer mp) 
			{         
				mp.start(); 
			}
		}); 
		
		mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() 
		{      
			public void onCompletion(MediaPlayer mp) 
			{  
				//enable next only in case of non-question page
				if (!(currentPageType.equals("1")))
					nextButtonView.setEnabled(true); 
				
			}
		}); 	 					

		//check if the application's key exists. If not disallow load of information
		if (!LoadEncryptionKeyAndStorageLoc())
			contentView.loadData(getString(R.string.START_HTML)+ "Device key not found" + getString(R.string.END_HTML), "text/html", null);

		initializeCourse();

		} //onCreate()

	  /*
	  protected void commenceCoursePageLoad() 
	  {
		  //initializeProgress.dismiss();
		  
  //System.out.println("Course initialized, total page = " + courseAttribMap.get("TotalPage") + "start Page = " + courseAttribMap.get("LastPage"));        		
		  communicateWithServiceToGetPage();

	  } //commenceCoursePageLoad()
	  */
	  //returns false if encryptionKey cannot be loaded. True otherwise
	   private boolean LoadEncryptionKeyAndStorageLoc()
	   {
		   SharedPreferences settings = getSharedPreferences(getString(R.string.PREFS_NAME), 0);
		   //key already created during registration
		   
		   String deviceKeyString = settings.getString(getString(R.string.DEVICE_KEY), null);
		   if (deviceKeyString == null)
		   {
			   System.out.println("Device key not found");
			   return false;
		   }
		   deviceKey = Base64.decode(deviceKeyString, Base64.DEFAULT);

		   storageDirectory = settings.getString(getString(R.string.STORAGE_LOCATION), null);
		   if (storageDirectory == null)
		   {
			   System.out.println("Storage directory not found");
			   return false;
		   }

		   return true;
		 //  System.out.println("Key retrieved in activity = " + deviceKeyString.getBytes());

	    } //LoadEncryptionKey()

		@Override
		//called when child activity finishes and this leads to reload of course list
		protected void onActivityResult(int requestCode, int resultCode, Intent dataFromSubActivity) {
			//to be filled
		} //onActivityResult()
	   
	    public void onConfigurationChanged(Configuration newConfig) 
	    {     
	    	super.onConfigurationChanged(newConfig);     
			setContentView(R.layout.activity_course_viewer); 
			//video commented
			/*
			contentView = (WebView) mainLayout.findViewById(R.id.contentView);
			nextButtonView = (Button) mainLayout.findViewById(R.id.nextButton);
			previousButtonView = (Button) mainLayout.findViewById(R.id.previousButton); */
			contentView = (WebView)findViewById(R.id.htmlCourseContentView);
			nextButtonView = (Button) findViewById(R.id.nextButton);
			previousButtonView = (Button) findViewById(R.id.previousButton);

	    	
			//code to re-initialize contentview. Can be later moved to a common function
			contentView.getSettings().setJavaScriptEnabled(true);
			contentView.getSettings().setPluginsEnabled(true);
			contentView.getSettings().setLoadsImagesAutomatically(true);
			contentView.getSettings().setBuiltInZoomControls(true);
			contentView.getSettings().setLoadWithOverviewMode(true);
			contentView.getSettings().setUseWideViewPort(true);
			contentView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			
			contentView.setWebViewClient(courseViewClient);
		//video part commented
			/*
			if (videoDisplay != null)
			{
				videoDisplay.stopPlayback();
				rootLayout.removeView(videoDisplayLayout);
				videoDisplay = null;
			} */
	    	mMediaPlayer.reset();
	    	
	    	//do not call display current urls if no course being displayed
	    	if (courseId != null)
	    	     displayCurrentUrls();
	    } //onConfigurationChanged()


	    protected File decryptBinaryFile(String fileName)
		{
			File outputFile = null;
			
			try
			{
				SecretKeySpec decryptKey = new SecretKeySpec(deviceKey, "AES");
				// 	Create the cipher
				Cipher cDecrypt = Cipher.getInstance("AES");
				cDecrypt.init(Cipher.DECRYPT_MODE, decryptKey);
			
				//get the location for the file
				//File binaryFile = new File(getExternalFilesDir(null), fileName);
				File binaryFile = new File(storageDirectory, fileName);

				InputStream fIn = new CipherInputStream(new FileInputStream(binaryFile), cDecrypt);
			
				//save all run files in run directory
				//outputFile = new File(getExternalFilesDir(null)+"/run", fileName + ".run");
				outputFile = new File(storageDirectory+"/run", fileName + ".run");
				FileOutputStream fOut = new FileOutputStream (outputFile);
			
				byte[] buffer = new byte[10240];
				int len;
				while ((len = fIn.read(buffer)) != -1) {
					fOut.write(buffer, 0, len);
				}
				fOut.close();
				fIn.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return outputFile;

		} //decryptBinaryFile()

	    protected void displayCurrentUrls()
	    {
	    	if (currentPageNo != null)
	    	{
	    		//	set the correct page no.
	    		lastPage = Integer.parseInt(currentPageNo);
	    		
	    		//set max page
	    		if (lastPage > maxPageSeen)
	    			maxPageSeen = lastPage;
	    	}
	    		
			//set the page number values
	    	((TextView)pageNumberView).setText((lastPage) + "/" + totalPages);


	    	//disable buttons till content is played if this is the first time that it is being seen
			if (maxPageSeen == lastPage)
				nextButtonView.setEnabled(false); //in case last Page is less than max page button will be enabled. It cannot be greater as it is made equal above if >
			else
				nextButtonView.setEnabled(true);

	    	if ((currentTextUrl == null) || (currentTextUrl.equals("-1"))) //last page reached
			{
				contentView.loadData(getString(R.string.START_HTML)+" Last page reached. Reset course to start again "+getString(R.string.END_HTML), "text/html", null);
				lastPageReached = true;
			}
	    	else if (currentPageType.equals("2")) //this is a swf video page
	    	{
	    		
	    		String flashUrl = "file:///data/data/com.customapp.android.AnytimeLearn/files/" + currentTextUrl;

	    		System.out.println("Flash url is " + flashUrl);
	    		
	    		//check if flash player exists
	    		boolean flashInstalled = false;
	    		try {
	    		  PackageManager pm = getPackageManager();
	    		  ApplicationInfo ai = pm.getApplicationInfo("com.adobe.flashplayer", 0);
	    		  if (ai != null)
	    		    flashInstalled = true;
	    		} catch (NameNotFoundException e) {
	    		  flashInstalled = false;
	    		}
	    		
	    		if (flashInstalled)
	    		{
	    			
	    			String flashLoader = "<html><body><object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0\" width=\"250\" height=\"350\" id=\"player1\" align=\"middle\"><param name=\"movie\" value=\""
	    						+ flashUrl + "\" /><param name=\"menu\" value=\"false\"/><param name=\"quality\" value=\"high\"/><param name=\"loop\" value=\"false\" /><param name=\"bgcolor\" value=\"#FFFFFF\"/><param name=\"salign\" value=\"tl\" /><noscript><a href=\"http://www.dvdvideosoft.com/products/dvd/Free-YouTube-Download.htm\">youtube download</a></noscript><embed src=\""
	    						+ flashUrl	+	"\" menu=\"false\" quality=\"high\" bgcolor=\"#FFFFFF\" width=\"300\" height=\"350\" name=\"player\" salign=\"tl\" loop=\"false\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\"/></object></body></html>";
	    				
	    			contentView.loadDataWithBaseURL(null, flashLoader, "text/html", "utf-8", "");
	    			
	    		}
	    		else
	    		{
	    			contentView.loadData(getString(R.string.START_HTML)+"This page requires flash player which is not installed on this device. You can download flash player for your device from <a href=\"http://helpx.adobe.com/flash-player/kb/archived-flash-player-versions.html\">here</a> and then revisit this course page. You can continue meantime with the rest of the course"+ getString(R.string.END_HTML), "text/html", null);
	    		}
	    		
	    	}
	    	else if ((currentPageType.equals("3")) || (currentPageType.equals("4")) || (currentPageType.equals("5"))) //any of the video pages mp4, ogv
	    	{
	    		int pageTypeInt = Integer.parseInt(currentPageType);
	    		String mimeType = "video/";
			    switch (pageTypeInt)
			    {
			    	case 3 :  mimeType = mimeType + "mp4"; break;
			    	case 4 :  mimeType = mimeType + "ogg"; break;
			    	case 5 :  mimeType = mimeType + "avi"; break;
			    }

	    		//trying a different approach to show video instead of locking into media frame
				File internalFile = decryptBinaryFile(currentTextUrl);

				
		        Uri internalFileUrl = Uri.fromFile(internalFile);
//		        System.out.println("URL for video = " + internalFileUrl);
		        Intent videoIntent = new Intent(Intent.ACTION_VIEW);  
		        videoIntent.setDataAndType(internalFileUrl, mimeType);  
		        startActivity(videoIntent);
		        
		        contentView.loadData(getString(R.string.START_HTML) + "Press next to continue" + getString(R.string.END_HTML), "text/html", null);
		        
	    		//video part commented
	    		/*
				//create the video view layout
		        videoDisplayLayout = (RelativeLayout) LayoutInflater.from(rootLayout.getContext()).inflate(R.layout.video_display, rootLayout, false);
		        videoDisplayLayout.setLayoutParams(new LayoutParams(contentView.getWidth(), contentView.getHeight())); //try to give it the same height as content view in case full screen is shown
		        videoDisplay = (VideoView) videoDisplayLayout.findViewById(R.id.viewVideo);    		
	       
				//decrypt the file
				File internalFile = decryptBinaryFile(currentTextUrl);

				
		        Uri internalFileUrl = Uri.fromFile(internalFile);
		        System.out.println("URL = " + internalFileUrl);

		        
		         //Use a media controller so that you can scroll the video contents
		        //and also to pause, start the video.
		        MediaController mediaController = new MediaController(contentView.getContext()); 
		        mediaController.setAnchorView(videoDisplay);
		        videoDisplay.setMediaController(mediaController);
		        videoDisplay.setVideoPath(internalFileUrl.toString());

		        if (videoDisplayLayout == null)
		        	System.out.println("Video display lyout is null");
		        rootLayout.addView(videoDisplayLayout);
		        contentView.loadData("", "text/html", null); //clean up background content
		        videoDisplay.start(); */	        
	    	}
			/* Below code to read from file  */
			else //this is a standard text page
			{
//		System.out.println("Current text url = " + currentTextUrl);

				String textData = readTextContentFromFile(currentTextUrl);
				
		        Uri textFileUrl = Uri.fromFile(new File(storageDirectory));
		        //System.out.println("URL for textFile = " + textFileUrl);
				//contentView.loadData(textData, "text/html", null);
				contentView.loadDataWithBaseURL(textFileUrl.toString(), textData, "text/html", "UTF-8", null);
			}
			/* The only order that seems to work is to load text first and use async mode in sound loading
			 * also removed the analytics code on website
			 * */

			if ((currentSoundUrl != null) && (!(currentSoundUrl.equals("-1")))) //there is no sound if value is -1
			{
				String mp3FileName = currentSoundUrl;
				try
				{
					//decrypt the file
					File soundFile = decryptBinaryFile(mp3FileName);

					//File soundFile = new File(getExternalFilesDir(null), mp3FileName);
					
					FileInputStream fSound = new FileInputStream(soundFile);

					FileDescriptor fSoundFD = fSound.getFD();
					mMediaPlayer.setDataSource(fSoundFD);                    
					mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
	/*				mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() 
					{      
						public void onPrepared(MediaPlayer mp) 
						{         
							mp.start(); 
						}
					}); */ 	 					
					mMediaPlayer.prepareAsync();
					fSound.close();

				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			} //if there is sound
			else if ((currentPageType != null) && (!(currentPageType.equals("1")))) //enable next button immediately if there is no sound and if it is not a question page
				nextButtonView.setEnabled(true);
	    } //displayCurrentUrls()

	    //method called to enable or disable button depending on when they are required for course navigation
	    private void initializeNavigationButtons()
	    {
			  previousButtonView.setEnabled(true);
			  nextButtonView.setEnabled(true);
			  ((Button) findViewById(R.id.reset)).setEnabled(true);
			  // ((Button) findViewById(R.id.courseIndex)).setEnabled(false); //this is enabled after index download
			  indexButtonView.setEnabled(true); //this is disabled if index download fails
			  
	    } //changeNaviationButtonState
	    

	    private void initializeCourse()
	    {
	    	/*
	    	initializeProgress = new ProgressDialog(this);
	    	initializeProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	    	initializeProgress.setMessage("Initializing course..");
	    	initializeProgress.show(); */

	    	//uses the parameter 1Title to indicate course title
	    	try {
	    		  initializeNavigationButtons();

	    		  //create the glossary from received text
		    		/*
	          	  //temporary code to simulate received file
	    		  FileOutputStream fOut = openFileOutput("courseGlossary.txt", MODE_WORLD_READABLE);        
				  OutputStreamWriter osw = new OutputStreamWriter(fOut);          
	              
				  osw.append("Knowledge=Power\n");
				  osw.append("Platform=Anytime Learn which gives flexibility to learn while travelling\n");
				  osw.append("Powerpoint=Microsoft tool to create presentations\n");
				  
				  osw.flush();        
				  osw.close();
				  fOut.close();
				 */
	    		  
	    		  getCourseAttributes();

System.out.println("Getting glossary"); 
      			 DownloadSinglePageURLTask glossaryDownload = new DownloadSinglePageURLTask();
    			 //String glossaryUrl = getString(R.string.baseURL) + courseId + "/glossary.html";
    			 String glossaryUrl = getString(R.string.baseURL) + courseId + "/glossary.txt";
    			 String whatToDoWithGlossaryUrl = MAP_STORE;
    			 glossaryDownload.execute(new Object[] { glossaryUrl, whatToDoWithGlossaryUrl, glossary, "GET" });
    			 
    			 
    			 //create the run directory
 			//	File runDir = new File(getExternalFilesDir(null)+"/run");
    	 		File runDir = new File(storageDirectory+"/run");
 				if (!runDir.exists())
 					runDir.mkdir();
 				else
 				{
 					deleteRuntimeFiles();
 					runDir.mkdir();
 				}
 					
/*
    			 InitializeCourseAsyncTask courseAttributeDownload = new InitializeCourseAsyncTask();
    			 //String glossaryUrl = getString(R.string.baseURL) + courseId + "/glossary.html";
    			 String courseAttribUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) +  "/getCourseAttributes.php";
    			 String whatToDoWithCourseAttribUrl = MAP_STORE;
    			 //put the post parameters for this call
    			 List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
    			 nameValuePairs.add(new BasicNameValuePair("CourseId",courseId));  
    			 nameValuePairs.add(new BasicNameValuePair("simId",subscriberId));  
     			 courseAttributeDownload.execute(new Object[] { courseAttribUrl, whatToDoWithCourseAttribUrl, courseAttribMap, "POST", nameValuePairs});
*/
	    			/* Commented courseIndex and called through async task for same reason
	 			   //get course Index
				   String courseIndexUrl = getString(R.string.baseURL) + "getCourseIndex.php" +"?courseId=" + courseId + "&simId=" + subscriberId;
			       System.out.println("course index URL = " + courseIndexUrl);
			          
					httpGetText = new HttpGet(courseIndexUrl);
		            //get the text part
					execute = client.execute(httpGetText);
					content = execute.getEntity().getContent();

					buffer = new BufferedReader(
							new InputStreamReader(content));
					fileLine = "";
					
					while ((fileLine = buffer.readLine()) != null) {
					  System.out.println("Line = " + fileLine);
					  //using a vector to store all index lines
		        	     courseIndex.add(fileLine);   
					 }
				   */

	    			DownloadIndexURLTask courseIndexDownload = new DownloadIndexURLTask();
	    			String courseIndexUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getCourseIndex.php" +"?courseId=" + courseId + "&simId=" + subscriberId;
	    			courseIndexDownload.execute(new Object[] { courseIndexUrl, courseIndex });
	    			
	    			
				   /*
				  //reading code starts here
	        	  FileInputStream fIn = openFileInput("courseGlossary.txt");
			      InputStreamReader isr = new InputStreamReader(fIn);
			      BufferedReader br = new BufferedReader(isr);
			      String fileLine = ""; 
			      while ((fileLine = br.readLine()) != null)
			      {
			    	  //separate the tokens
		        	   String name = fileLine.split("=")[0];   
		        	   String value = fileLine.split("=")[1];   
	        	       glossary.put(name, value);   
			      }
			      
			      br.close();
			      isr.close();
			      fIn.close(); */
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!courseLocallyCached) //put a row to indicate that course first downloaded on today's date/time
			{
				AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

				//delete all rows of the page content that exist
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				String deleteWhereClause = AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?";
				db.delete(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, deleteWhereClause, new String[]{courseId});
				
				//get the current date
				Date currentDate = new Date();
				String formattedCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDate);
				ContentValues values = new ContentValues();
				values.put(AnytimeLearnDb.DOWNLOAD_DATE_COL_NAME, formattedCurrentDate); // put current time
				values.put(AnytimeLearnDb.COURSE_NAME_COL_NAME, courseName); // put course name
				values.put(AnytimeLearnDb.COURSE_TYPE_COL_NAME, "C"); // put type as test
				// Update Row
				long updateValue = db.update(AnytimeLearnDb.COURSE_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{courseId});
				System.out.println("Update value = " + updateValue);
				
				//if update did not work, insert a new row
				if (updateValue != 1) //only 1 row should have been updated
				{
					values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, courseId);
					long insertValue = db.insert(AnytimeLearnDb.COURSE_TABLE_NAME, null, values);
					System.out.println("Insert value = " + insertValue);
				}
				db.close(); // Closing database connection
				
			} //end of row insertion in case course is first time downloaded

	    	//set the title to parameter course name
	    	if (courseName != null)
	    		((TextView)titleView).setText("Anytime Learn - " + courseName);
	    } //initializeCourse()
	    
	    private void getCourseAttributes()
	    {
			//first fill from db if the course is locally cached
			if (courseLocallyCached)
			{
				AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
			
				//read the last page stored
				SQLiteDatabase db = dbHelper.getReadableDatabase();

				Cursor cursor = db.query(AnytimeLearnDb.COURSE_TABLE_NAME, new String[] { AnytimeLearnDb.NUM_PAGES, AnytimeLearnDb.LAST_DISPLAY_PAGE, AnytimeLearnDb.MAX_PAGE_SEEN },
					AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
					new String[] {courseId}, null, null, null, null);
				if (cursor != null)
				{
					if (cursor.moveToFirst()) 	//means page row exists.
					{
						totalPages =  cursor.getInt(0);
						lastPage = cursor.getInt(1);
						maxPageSeen = cursor.getInt(2);
					}
					cursor.close();
				}
				
				db.close();
			}
			
			CourseAttributeRetrieveAsyncTask courseAttributeRetrieve = new CourseAttributeRetrieveAsyncTask();

			courseAttributeRetrieve.execute();
	    	
	    } //getCourseAttributes()
	    
	    /** Called when next button is clicked  **/
		public void loadNextPage(View view)
		{
		   //for some reason contentView.loadData only shows the first text given in this method and does not replace it later with any other values
		   //therefore checking initially the text that needs to be shown and showing that only.
		   //however this does not work as well. therefore currently the reset text is shown in displayCurrentUrls
			if (lastPageReached)
			{
				contentView.loadData(getString(R.string.START_HTML)+"Last page reached. Reset course to start again"+getString(R.string.END_HTML), "test/html", null);
				displayCurrentUrls(); //currentTextUrl must be null due to the previous call to next button
			}
			else
			{	
				contentView.loadData(getString(R.string.START_HTML)+"Loading next page"+getString(R.string.END_HTML), "text/html", null);
				loadNumberedPage(lastPage+1);
			}
		} //loadNextPage()

		protected void loadNumberedPage(int pageNumber)
		{
			//used to load next page by either retrieving one or taking from local storage
			/*
			//call course retriever with a specific page no. to retrieve
			downloadService.putExtra(getString(R.string.CURRENT_PAGE_NO), currentPageNo);
			downloadService.putExtra(getString(R.string.TOTAL_PAGES), totalPages);
			startService(downloadService);		
			*/
	System.out.println("load Next page  called with values pageNumber = " + pageNumber + " total page = " + totalPages);
	
			mMediaPlayer.reset();
			if (pageNumber <= totalPages)
			{
				boolean localCacheFound = false;
				
				//read the cache from db
				//check if page exists locally irrespective of cache status because it might be that pages are downloaded in background after first page
				
				//if (testLocallyCached)
				{	
					AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
					
					//read the last page stored
					SQLiteDatabase db = dbHelper.getReadableDatabase();

					Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME, AnytimeLearnDb.HAS_SOUND_COL_NAME, AnytimeLearnDb.HAS_NEXT_COL_NAME, AnytimeLearnDb.HAS_PREV_COL_NAME, AnytimeLearnDb.PAGE_TYPE_COL_NAME}, 
							AnytimeLearnDb.COURSE_ID_COL_NAME + "=? AND " + AnytimeLearnDb.PAGE_NO_COL_NAME + "=?",
							new String[] {courseId, Integer.toString(pageNumber)}, null, null, null, null);
					if (cursor != null)
					{
						if (cursor.moveToFirst()) 	//means page row exists.
						{
							localCacheFound = true;
							currentTextUrl = courseId + "_" + cursor.getInt(0);
							if (cursor.getInt(1) == 1)
							{
								currentSoundUrl = courseId + "_" + cursor.getInt(0)+"_s";
							}
							else
								currentSoundUrl = null;
							currentPageType = cursor.getString(4);
							currentPageNo = Integer.toString(pageNumber);
						}
					}
					cursor.close();
					db.close();
				}
				
			//	localCacheFound = false;
				System.out.println("Local cached found status = " + localCacheFound);
				
				if (!localCacheFound)
				{
					//retrieve specifc page
					CoursePageRetrieveAsyncTask pageRetrieveTask = new CoursePageRetrieveAsyncTask();
		        	Integer pageNoToRetrieve = new Integer(pageNumber);
					pageRetrieveTask.execute(new Object[] {pageNoToRetrieve});
					
					//show progress bar here
			    	loadProgress = new ProgressDialog(this);
			    	loadProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			    	loadProgress.setMessage("Loading course page..");
			    	loadProgress.show();

				}
				else
					displayCurrentUrls();
			} //if (lastPage < totalPage)
			else
			{
				
				lastPageReached = true;
				currentTextUrl = null; //setting up content view message in this method does not work. So doing that through displayCurrentUrls
				currentSoundUrl = null; //sound page is null so that sound does not play
				displayCurrentUrls();
			}

		} //loadNumberedPage
		
	    /** Called when previous button is clicked  **/
		public void loadPreviousPage(View view) 
		{
			contentView.loadData(getString(R.string.START_HTML)+ "Loading previous page"+ getString(R.string.END_HTML), "text/html", null);
			mMediaPlayer.reset();
			
			//video comment
			/*
			if (videoDisplay != null)
			{
				videoDisplay.stopPlayback();
				rootLayout.removeView(videoDisplayLayout);
				videoDisplay = null;
			}
			*/
			/*
			mMediaPlayer.reset();
			lastPageReached = false;
			
			//put current in next buffer
			nextFileStack.push(currentTextUrl);
			nextFileStack.push(currentSoundUrl);
			nextFileStack.push(currentPageNo);
			nextFileStack.push(currentPageType);
			
			
			if (previousFileStack.empty()) 
			{
				int integerPageNo = Integer.parseInt(currentPageNo);
				
				if (integerPageNo != 1) // get previous page from server if not first page
				{
					getPreviousPageFromServer();
				//	contentView.loadData("Getting previous page no "+currentPageNo, "text/null", null);
				}
				else
				{
					contentView.loadData(getString(R.string.START_HTML)+"No previous page "+getString(R.string.END_HTML), "text/html", null);
					nextButtonView.setEnabled(true); //enable next for moving to next pages
				}
			}
			else
			{	
				currentPageType = previousFileStack.pop();
				currentPageNo = previousFileStack.pop();
				currentSoundUrl = previousFileStack.pop();
				currentTextUrl = previousFileStack.pop();
				displayCurrentUrls();
			} */
			if (lastPage > 1)
			{
				if (lastPageReached) //if previous called after next was pressed after last page, then load last page
				{
					lastPageReached = false;
					loadNumberedPage(lastPage);
				}
				else
					loadNumberedPage(lastPage-1);
			}
			else
			{
				if (lastPage == 1)
					lastPage--;
				contentView.loadData(getString(R.string.START_HTML)+"No previous page "+getString(R.string.END_HTML), "text/html", null);
				nextButtonView.setEnabled(true); //enable next for moving to next pages
			}
		} //loadPreviousPage()
	    
		protected void checkDownloadStatusAndDownloadAll()
		{
	    	int numRecords = -1;
	    	
	    	//no need to check cache status
	    //	if (testLocallyCached)
	    	{
	    		//check no. of page records against total pages
				AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
				
				//read the last page stored
				SQLiteDatabase db = dbHelper.getReadableDatabase();

				Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME }, 
						AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
						new String[] {courseId}, null, null, null, null);
				if (cursor != null)
				{
					numRecords = cursor.getCount();
					
					if (numRecords != totalPages) //total pages is already filled up before this
						commenceCourseDownload();

					cursor.close();
				}
				db.close();
	    	}
			
		} //checkDownloadStatusAndDownloadAll()
		
		private void commenceCourseDownload()
		{
			//	Handler handler = new TestDisplayHandler();

			System.out.println("communicating with service for all downloads - course");		

			//	Messenger messenger = new Messenger(handler);
				downloadService = new Intent(this, CourseRetrieverDownloadOptimized.class);
			//	downloadService.putExtra("MESSENGER", messenger);
				downloadService.putExtra("courseId", courseId); 
				downloadService.putExtra("simId", subscriberId);
				downloadService.putExtra("Trigger", "AllCoursePages");
				downloadService.putExtra("TotalPages", totalPages);
				//downloadService.putExtra("cacheStatus", courseLocallyCached); 
				startService(downloadService);		
			
		} //commenceCourseDownload()
		
		public void getCourseIndex(View view)
		{
			//System.out.println("showing popup");
			View pView = ((LayoutInflater)(this.getSystemService(Context.LAYOUT_INFLATER_SERVICE))).inflate(R.layout.index,null);
			
			//final PopupWindow indexPopupWindow = new PopupWindow(pView, 100, 100);
			final PopupWindow indexPopupWindow = new PopupWindow(contentView.getContext());
			indexPopupWindow.setHeight(500);
			//indexPopupWindow.setWidth(100);
			indexPopupWindow.setWindowLayoutMode (ViewGroup.LayoutParams.FILL_PARENT, 0);

	        TextView popUpText = (TextView)pView.findViewById(R.id.indexContent);

	        popUpText.append("Index for course\n\n");
	        
	        for (int i = 0; i < courseIndex.size(); i++)
	        {
	        	if ((i+1) == Integer.parseInt(currentPageNo)) //current page no. starts from 1
	        	{
	        		popUpText.append("-> ");
	        	}
	        	popUpText.append(courseIndex.get(i)+"\n");
	        }
	        
	        popUpText.append("Click outside this box to close\n");
	        //popUpText.setVerticalScrollBarEnabled(true);
	        //indexPopupWindow.setOutsideTouchable(true);

	        indexPopupWindow.setContentView(pView);
	        
	        /*
			indexPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
					public boolean onTouch(View popUpView, MotionEvent event) {
						System.out.println("Touch interceptor called");
						indexPopupWindow.dismiss();
		        		return true;
						}
		    });		
	        */
			
//			indexPopupWindow.showAtLocation(contentView, Gravity.LEFT | Gravity.TOP, 40, 40);
//			indexPopupWindow.showAsDropDown(contentView, 0, 0);
			indexPopupWindow.setFocusable(true);
		//	indexPopupWindow.showAsDropDown(getWindow().findViewById(android.R.id.title),0,0);
			indexPopupWindow.showAsDropDown(titleView, 0, 0);
			return;
		} //getCourseIndex()

		//called when reset to start button is clicked
		public void ResetToStart(View view)
		{
			/*
			//stop service to avoid any downloading in progress
			Handler handler = new DirectMessageDisplayHandler();

	//System.out.println("communicating with service for reset");		
			contentView.loadData(getString(R.string.START_HTML)+"Resetting course.."+getString(R.string.END_HTML), "text/html", null);

			Messenger messenger = new Messenger(handler);
			//only put two fields that change, rest are assumed to be in intent anyway
			downloadService.putExtra("MESSENGER", messenger);
			downloadService.putExtra("Trigger", "CourseReset");
			*/
			/*
			boolean stopReturn = stopService(downloadService);
			System.out.println("Service stop value = " + stopReturn);
			*/
			//startService(downloadService);

			//reset the course information
			ResetCourseSetup();
			
			courseReset = true;
			loadNumberedPage(1);
		} //ResetToStart()

		//this method is called from reset course or view my course buttons to reset current course buffers
		//note that this method does not set the value of courseReset flag which is dependent on the method calling this one
		private void ResetCourseSetup()
		{
			//delete file
			deleteFile("nextPageBuffer.txt");
			
			//clear internal buffers
			//nextFileStack.clear();
			//previousFileStack.clear();
			//video comment
			/*
			if (videoDisplay != null)
			{
				videoDisplay.stopPlayback();
				rootLayout.removeView(videoDisplayLayout);
				videoDisplay = null;
			}
			*/
			mMediaPlayer.reset();
			//glossary.clear(); //commenting this and next line as we are now resetting course locally
			//courseIndex.clear();
			lastPageReached = false;
			//make current page as 0
			currentPageNo = "0";
		} //ResetCourseSetup()
	/*	
		//method to get the previous page when previous button is clicked. called by previous button handleer
		private void getPreviousPageFromServer()
		{
			Handler handler = new CourseDisplayHandler();

			Intent downloadService = new Intent(this, CourseRetriever.class);
			Messenger messenger = new Messenger(handler);
			downloadService.putExtra("MESSENGER", messenger);
			downloadService.putExtra("courseId", courseId);
			downloadService.putExtra("simId", subscriberId);
			downloadService.putExtra("pageDirection", "Previous");
			int newPageNo = Integer.parseInt(currentPageNo)-1;
			downloadService.putExtra("pageNo", new Integer(newPageNo).toString());
			downloadService.putExtra("Trigger", "CourseStart");
			downloadService.putExtra("cacheStatus", courseLocallyCached); 
			startService(downloadService);			
		} //getPreviousPageFromServer()
*/
		//method to read text file that is created with content. Returns String containing data
		private String readTextContentFromFile(String fileName)
		{
				String fileData = "";
				try
				{
			//     FileInputStream fIn = openFileInput(fileName);
	 		//	 File textFile = new File(getExternalFilesDir(null), fileName);
			 	 File textFile = new File(storageDirectory, fileName);
	 			 // FileInputStream fIn = new FileInputStream(textFile);
	 			 //create a decrypt reader
	 			 SecretKeySpec decryptKey = new SecretKeySpec(deviceKey, "AES");

				 // Create the cipher
		         Cipher cDecrypt = Cipher.getInstance("AES");
		         cDecrypt.init(Cipher.DECRYPT_MODE, decryptKey);
		         InputStream fIn = new CipherInputStream(new FileInputStream(textFile), cDecrypt); 
			     
		         InputStreamReader isr = new InputStreamReader(fIn);
	     	     BufferedReader br = new BufferedReader(isr);
			     String fileLine = "";
			     while ((fileLine = br.readLine()) != null)
			     {
			    	 fileData+=fileLine;
			     }
			      
			     br.close();
			     isr.close();
			     fIn.close();
				}
			    catch (Exception ioe)        
			    {
			    	ioe.printStackTrace();
			    }
				
				return fileData;
		} //readTextContentFromFile()
		
	    /** Called when the activity is destroyed. */
	    @Override
	    public void onDestroy() {
	        super.onDestroy();

	        if (courseId != null) //not destroying from course page
	        {
	        	//updating the last page on the server if course id is not null
	        	DownloadSinglePageURLTask pageResetTask = new DownloadSinglePageURLTask();
	        	String pageResetUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "updatePageNo.php?simId="+subscriberId+"&courseId="+courseId+"&pageNo="+currentPageNo;
	        	//System.out.println("Page reset called with " + pageResetUrl);
	        	String whatToDoWitResetUrl = DO_NOTHING;
	        	pageResetTask.execute(new Object[] { pageResetUrl, whatToDoWitResetUrl, null, "GET" });
	        }
	        
			//reset to stop the voice
	        mMediaPlayer.reset();
	        
	        //store current page no. and total pages in db
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			//Update the row corresponding to this course id only. The row would be created in initializeTest
			SQLiteDatabase db = dbHelper.getWritableDatabase();

			Date currentDate = new Date();
			String formattedCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDate);

			ContentValues values = new ContentValues();
			values.put(AnytimeLearnDb.LAST_DISPLAY_PAGE, lastPage); // put current time
			values.put(AnytimeLearnDb.NUM_PAGES, totalPages); // put totalPage
			values.put(AnytimeLearnDb.MAX_PAGE_SEEN, maxPageSeen); //setting max page seen
			values.put(AnytimeLearnDb.LAST_ACCESS_DATETIME, formattedCurrentDate); // put current time for last access
			long updateValue = db.update(AnytimeLearnDb.COURSE_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{courseId});
	    
	System.out.println("Update value in destroy = " + updateValue);
			db.close();

	        
	        //video commented
	        /*
	        //stop the video if playing
			if (videoDisplay != null)
			{
				videoDisplay.stopPlayback();
				rootLayout.removeView(videoDisplayLayout);
				videoDisplay = null;
			}
	        */
			//disabling file deletion to make use of downloaded pages
			/*
	        //delete the files associated with this application to cleanup storage
	        String filesStored[] = fileList();
//	        System.out.println("No. of pages stored = " + filesStored.length);
	        for (int i=0; i < filesStored.length; i++)
	        {
	 //       	System.out.println(filesStored[i]);
	        	deleteFile(filesStored[i]);
	        }
	        */
			
	        //delete the application cache
	        //trimCache();
	        
			//delete runtime files
			deleteRuntimeFiles();
			
			//keep a call to sync
		     // Pass the settings flags by inserting them in a bundle        
			Bundle settingsBundle = new Bundle();        
			settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);        
			settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);        
			/** Request the sync for the default account, authority, and         
			 * * manual sync settings         */        
			ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);			
			
	    } //onDestroy()

	     private void trimCache() 
	     {
	          File dir = getCacheDir();
	          if (dir != null && dir.isDirectory()) {
	               deleteDir(dir);
	          }
	               
	     } //trimCache() 
	           
	     private void deleteRuntimeFiles() 
	     {
	    	 //deleting entire run directory
 	    	 // deleteDir(new File(getExternalFilesDir(null) + "/run"));
 	    	 deleteDir(new File(storageDirectory + "/run"));
	     } //deleteRuntimeFiles() 
	           
	  
	     private boolean deleteDir(File dir) {
	         if (dir!=null && dir.isDirectory()) {
	              String[] children = dir.list();

	              for (int i = 0; i < children.length; i++) {
	                   boolean success = deleteDir(new File(dir, children[i]));
	                   if (!success) {
	                       return false;
	                   }
	               }
	         }
	         // The directory is now empty so delete it
//	         System.out.println("Deleting cache dir = "+ dir);
	         return dir.delete();
	     } //deleteDir(File dir)

	 	private class DownloadSinglePageURLTask extends AsyncTask<Object, Void, Object[]> 
		{   
			private String whatToDoWithUrl; //defines actions such as display or storeMap or storeVector to define action with url data. Copy is meant to have a filename as third parameter and store is meant to have a Hashmap
			private Object storageObject;
			
			@Override
			protected Object[] doInBackground(Object... urls) {
				String response = "";
				String url = (String)urls[0];
				whatToDoWithUrl = (String)urls[1];
				boolean postRequest = false;
				
				//check if this is a post request
				if (((String)urls[3]).equals("POST"))
					postRequest = true;
					
				
				System.out.println("Calling with URL = " + url);
				
				HttpClient client = new DefaultHttpClient();
				try {
						HttpResponse execute;
						if (postRequest)
						{
							//get the text part
							HttpPost httpPostRequest = new HttpPost(url); 
							httpPostRequest.setEntity(new UrlEncodedFormEntity((List<NameValuePair>)urls[4]));
							execute = client.execute(httpPostRequest);
						}
						else
						{
							//get the text part
							HttpGet httpGetText = new HttpGet(url);
							execute = client.execute(httpGetText);
						}
						InputStream content = execute.getEntity().getContent();

						BufferedReader buffer = new BufferedReader(
								new InputStreamReader(content));
						String s = "";
						while ((s = buffer.readLine()) != null) {
							response += s;
							response += "\n";
						}	
					} catch (Exception e) {
						response = getString(R.string.START_HTML) + "Cannot connect to server. Please check your Internet connectivity" + getString(R.string.END_HTML);
						e.printStackTrace();
					}
				
//					System.out.println("Response = "+ response);
					Object responseData[] = new Object[]{ response };	
					//pass parameter in result that helps to store information in onPostexecute
					if (!(whatToDoWithUrl.equals("display")))
						storageObject = urls[2]; 
					
					return responseData;
				} //doInBackground()

				@Override
				protected void onPostExecute(Object[] pageData) 
				{
					final String result = (String)pageData[0];
					
					try
					{
						if (whatToDoWithUrl.equals("display"))
							contentView.loadData(result, "text/html", null);
						else if (whatToDoWithUrl.equals(MAP_STORE)) //expect a hashmap as a storage object
						{
							//	StringBufferInputStream buffer = new StringBufferInputStream(result);
							BufferedReader buffer = new BufferedReader(new StringReader(result));
										
						   String fileLine = "";
						   while ((fileLine = buffer.readLine()) != null) {
							   //System.out.println("Line = " + fileLine);
			             	   if (fileLine.lastIndexOf('=') != -1)
			             	   {
							      //separate the tokens
				        	      String name = fileLine.split("=")[0];   
				        	      String value = fileLine.split("=")[1];   
			        	          ((Map<String, String>)storageObject).put(name, value); 
							   }
						   }
						}
						else if (whatToDoWithUrl.equals(VECTOR_STORE)) //expect a vector as a storage object
						{
							Vector<String> vectorContentStorage = (Vector<String>)storageObject;
							
						   //get course Index or course page attributes
//							StringBufferInputStream buffer = new StringBufferInputStream(result);
							BufferedReader buffer = new BufferedReader(new StringReader(result));
							String fileLine = "";
							
							while ((fileLine = buffer.readLine()) != null) {
							  //using a vector to store all index lines
				        	    // ((Vector<String>)courseIndex).add(fileLine);
								vectorContentStorage.add(fileLine);
							 }
						}
						
						//other value expected for whatToDoWithUrl is DO_NOTHING which means response is discarded
					}
					catch (Exception e)
					{
						System.out.println("Unhandled exception at async task");
					}
						
				} //end of handler function

				
				private String[] parseCachedCoursePageInfo(String response)
				{
				    String[] contentValues = response.split("&");
				    
				    //format is contentValue[0] the number value indicates the current page no. of text
				    //contentValue[1] the number 1 indicates sound page corresponding to the text page no
				    //contentValue[2] number 0 indicates no previous, 1 indicates previous
				    //contentValue[3] number 0 indicates no next, 1 indicates next
				    //contentValue[4] number 0 indicates no next, 1 indicates next
				    int pageNo = Integer.valueOf(contentValues[0]);
				    int soundExists = Integer.valueOf(contentValues[1]);
				    int isPreviousEnabled = Integer.valueOf(contentValues[2]);
				    int isNextEnabled = Integer.valueOf(contentValues[3]);
				    int pageType = Integer.valueOf(contentValues[4]);
//			System.out.println("Page no = " + pageNo + " soundExists = "+soundExists+" isPreviousEnabled = "+isPreviousEnabled+" isNextEnabled = "+isNextEnabled + " pageType = "+ pageType);	
				    
				    String[] returnValues = new String[4];
				    
				    returnValues[0] = null;
				    if (pageNo != 0) //not the last page
				    {
				    	returnValues[0] = courseId + "_" + pageNo;
						
					    switch (pageType)
					    {
					    	case 2 : returnValues[0] = returnValues[0] + ".swf"; break;
					    	case 3 :  returnValues[0] = returnValues[0] + ".mp4"; break;
					    	case 4 :  returnValues[0] = returnValues[0] + ".ogv"; break;
					    	case 5 :  returnValues[0] = returnValues[0] + ".avi"; break;
					    	default : returnValues[0] = returnValues[0] + ".html"; break;
					    }

				    }
				    	
				    returnValues[1] = null;
				    if (soundExists > 0) //>0 is the page no. of the sound page
			    	    returnValues[1] = courseId + "_" + pageNo + "s.mp3";
				    
				    returnValues[2] = "" + pageNo;
				    returnValues[3] = "" + pageType;
				    
				    return returnValues;
				} //parseCachedCoursePageInfo
		} //DownloadSinglePageURLTask()

	 	private class DownloadIndexURLTask extends AsyncTask<Object, Void, Object[]> 
		{   
			private Object storageObject;
			
			@Override
			protected Object[] doInBackground(Object... urls) {
				String response = "";
				String url = (String)urls[0];
				boolean postRequest = false;
				
				System.out.println("Calling with URL = " + url);
				
				HttpClient client = new DefaultHttpClient();
				try {
						HttpResponse execute;
						//get the text part
						HttpGet httpGetText = new HttpGet(url);
						execute = client.execute(httpGetText);
						InputStream content = execute.getEntity().getContent();

						BufferedReader buffer = new BufferedReader(
								new InputStreamReader(content));
						String s = "";
						while ((s = buffer.readLine()) != null) {
							response += s;
							response += "\n";
						}	
					} catch (Exception e) {
						response = getString(R.string.START_HTML) + "Cannot connect to server. Please check your Internet connectivity" + getString(R.string.END_HTML);
						e.printStackTrace();
					}
				
//					System.out.println("Response = "+ response);
					Object responseData[] = new Object[]{ response };	
					//pass parameter in result that helps to store information in onPostexecute
					storageObject = urls[1]; 
					
					return responseData;
				} //doInBackground()

				@Override
				protected void onPostExecute(Object[] pageData) 
				{
					final String result = (String)pageData[0];
					
					try
					{
						Vector<String> vectorContentStorage = (Vector<String>)storageObject;
							
					   //get course Index or course page attributes
//							StringBufferInputStream buffer = new StringBufferInputStream(result);
						BufferedReader buffer = new BufferedReader(new StringReader(result));
						String fileLine = "";
							
						while ((fileLine = buffer.readLine()) != null) {
						  //using a vector to store all index lines
			        	    // ((Vector<String>)courseIndex).add(fileLine);
							vectorContentStorage.add(fileLine);
						}
					}
					catch (Exception e)
					{
						System.out.println("Unhandled exception at async task");
					}
						
				} //end of handler function
		} //DownloadIndexURLTask()
		
		
		/*
		//below class used to display content when it comes through a text file such as presentation/sound
		private class CourseDisplayHandler extends Handler
		{
		
			@Override
			public void handleMessage(Message message) {
					//do not display if course is being reset to avoid displaying  downloads in progress
					if (courseReset)
						return;
					
					final String[] urlsFromService = (String[])message.obj;
					
					currentTextUrl = urlsFromService[0];
					currentSoundUrl = urlsFromService[1];
					currentPageNo = urlsFromService[2];
					currentPageType = urlsFromService[3];
//					System.out.println("To display page no " + currentPageNo);
					displayCurrentUrls();
			} //end of handler
		} //end of private class coursedisplay

		//Below class used to display messages from server directly.
		private class DirectMessageDisplayHandler extends Handler
		{
		
			@Override
			public void handleMessage(Message message) {
					final String msgFromServer = (String)message.obj;

//					contentView.setInitialScale(STANDARD_ZOOM);
					contentView.loadData(msgFromServer, "text/html", null);
	 				courseReset = false;
					
			} //end of handler
		} //end of private class DirectMessageDisplayHandler
*/
/*
		private class InitializeCourseAsyncTask extends AsyncTask<Object, Void, Object[]> 
		{   
				private String whatToDoWithUrl; //defines actions such as display or storeMap or storeVector to define action with url data. Copy is meant to have a filename as third parameter and store is meant to have a Hashmap
				private Object storageObject;
				
				@Override
				protected Object[] doInBackground(Object... urls) {
					String response = "";
					String url = (String)urls[0];
					whatToDoWithUrl = (String)urls[1];
					boolean postRequest = false;
					
					//check if this is a post request
					if (((String)urls[3]).equals("POST"))
						postRequest = true;
						
					
					System.out.println("Calling with URL = " + url);
					
					HttpClient client = new DefaultHttpClient();
					try {
							HttpResponse execute;
							if (postRequest)
							{
								//get the text part
								HttpPost httpPostRequest = new HttpPost(url); 
								httpPostRequest.setEntity(new UrlEncodedFormEntity((List<NameValuePair>)urls[4]));
								execute = client.execute(httpPostRequest);
							}
							else
							{
								//get the text part
								HttpGet httpGetText = new HttpGet(url);
								execute = client.execute(httpGetText);
							}
							InputStream content = execute.getEntity().getContent();

							BufferedReader buffer = new BufferedReader(
									new InputStreamReader(content));
							String s = "";
							while ((s = buffer.readLine()) != null) {
								response += s;
								response += "\n";
							}	
						} catch (Exception e) {
							response = getString(R.string.START_HTML) + "Cannot connect to server. Please check your Internet connectivity" + getString(R.string.END_HTML);
							e.printStackTrace();
						}
					
//						System.out.println("Response = "+ response);
						Object responseData[] = new Object[]{ response };	
						//pass parameter in result that helps to store information in onPostexecute
						if (!(whatToDoWithUrl.equals("display")))
							storageObject = urls[2]; 
						
						return responseData;
					} //doInBackground()

					@Override
					protected void onPostExecute(Object[] pageData) 
					{
						final String result = (String)pageData[0];
//System.out.println("Result in initialize = " + result);						
						
						try
						{
							if (whatToDoWithUrl.equals("display"))
								contentView.loadData(result, "text/html", null);
							else if (whatToDoWithUrl.equals(MAP_STORE)) //expect a hashmap as a storage object
							{
								//	StringBufferInputStream buffer = new StringBufferInputStream(result);
								BufferedReader buffer = new BufferedReader(new StringReader(result));
											
							   String fileLine = "";
							   while ((fileLine = buffer.readLine()) != null) {
								   //System.out.println("Line = " + fileLine);
				             	   if (fileLine.lastIndexOf('=') != -1)
				             	   {
								      //separate the tokens
					        	      String name = fileLine.split("=")[0];   
					        	      String value = fileLine.split("=")[1];   
				        	          ((Map<String, String>)storageObject).put(name, value);
//System.out.println("Name = " + name + " Value = " + value);
								   }
				         
							   }
							}
							else if (whatToDoWithUrl.equals(VECTOR_STORE)) //expect a vector as a storage object
							{
								Vector<String> vectorContentStorage = (Vector<String>)storageObject;
								
							   //get course Index or course page attributes
//								StringBufferInputStream buffer = new StringBufferInputStream(result);
								BufferedReader buffer = new BufferedReader(new StringReader(result));
								String fileLine = "";
								
								while ((fileLine = buffer.readLine()) != null) {
								  //using a vector to store all index lines
					        	    // ((Vector<String>)courseIndex).add(fileLine);
									vectorContentStorage.add(fileLine);
								 }
							}
							
							//initiate course page loading process here
							commenceCoursePageLoad();
							
							//other value expected for whatToDoWithUrl is DO_NOTHING which means response is discarded
						}
						catch (Exception e)
						{
							e.printStackTrace();
							System.out.println("Unhandled exception at Initilaize async task");
						}
							
			} //end of handler function
		} //InitializeCourseClass
*/
		private class CourseAttributeRetrieveAsyncTask extends AsyncTask<Object, Void, Object[]> 
		{   
				
				@Override
				protected Object[] doInBackground(Object... urls) {
					String response = "";
					
					//get the last page and total page and any other test attributes
					String courseAttributeUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getCourseAttributes.php";
					//set the name value parameters for call
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
					nameValuePairs.add(new BasicNameValuePair("CourseId",courseId));            
					nameValuePairs.add(new BasicNameValuePair("SimId",subscriberId));

					PostRequestPerform testAttributeRequest = new PostRequestPerform(courseAttributeUrl, nameValuePairs);
					InputStream content = testAttributeRequest.getResponse();

					if (content != null)
					{
						//reply expected in form lastPage&totalPage
						BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
						try
						{
							String s = "";
							while ((s = buffer.readLine()) != null)
								response += s;
			System.out.println("book mark response " + response);
						}
						catch (IOException ioe)
						{
							ioe.printStackTrace();
							lastPage = totalPages = -1;
							return null;
						}
				
						String[] courseBookmarks = response.split("&");

						lastPage = Integer.parseInt(courseBookmarks[0]);
						totalPages = Integer.parseInt(courseBookmarks[1]);
					}
							
					return null;	
				} //doInBackground()

				@Override
				protected void onPostExecute(Object[] pageData) 
				{
					System.out.println("last page = " + lastPage + " total page = " + totalPages);
					if (lastPage == -1) //means details could not be retrieved and are not locally available also
					{
						//decide what to do here
					} //if (lastPage == -1)

					loadNumberedPage(lastPage+1); 
					
					//check if all pages exist and if not start download
					checkDownloadStatusAndDownloadAll();

					//show message here if both could not be retrieved
				} //onPostExecute
		} //CourseAttributeRetrieveAsyncTask

		
		private class CoursePageRetrieveAsyncTask extends AsyncTask<Object, Void, Object[]> 
		{   
				String contentFileName;
				String soundFileName;
				int pageType;
				int pageNoToRetrieve;
				
				@Override
				protected Object[] doInBackground(Object... urls) {
					String response = "";
					pageNoToRetrieve = ((Integer)urls[0]).intValue();
					int pageNo = pageNoToRetrieve, hasSound = 0;
							
					String courseContentStatusUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getContentStatus.php?courseId=" + courseId + "&simId=" + subscriberId;
					//add page No if available
					courseContentStatusUrl = courseContentStatusUrl + "&pageNo="+Integer.toString(pageNoToRetrieve);


					System.out.println("Content url in coureviewer" + courseContentStatusUrl);
					
					//get the no. of urls and if there is a next page
					HttpClient client = new DefaultHttpClient();
					HttpGet httpGetText = new HttpGet(courseContentStatusUrl);
					try {
							HttpResponse execute = client.execute(httpGetText);
							InputStream content = execute.getEntity().getContent();

							BufferedReader buffer = new BufferedReader(
									new InputStreamReader(content));
							String s = "";
							while ((s = buffer.readLine()) != null) {
								response += s;
							}	
						} catch (Exception e) {
							e.printStackTrace();
						}
					
					String[] courseParams = new String[]{courseId, subscriberId};
					
					String[] contentUrls = parseResponseAndGetUrls(response, courseParams);
					String[] savedFiles = new String[4]; //2 to store the urls, third element to give page no and fourth gives the page type. 0 indicates normal page, 1 question page and 2 is flash/video page
					pageType = Integer.parseInt(contentUrls[3]); //use object array in parseResponseAndGetUrls to avoid needless conversion from string to int
					
					if (contentUrls[0] != null) //not the last page
					{
						
						//get the text content to a local file of the same name
System.out.println("Course page url = " + contentUrls[0]);						
						httpGetText = new HttpGet(contentUrls[0]);
						//postChange
						//String readUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getPage.php";
						//HttpPost httpGetContentPost = new HttpPost(readUrl);
							
						//get the session cookie here to send to server for genuine call check
						//String sessionKeyValue = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(ProfileSelectionActivity.SESSION_KEY, null);
						//httpGetContentPost.setHeader("Cookie", sessionKeyValue);
							
						//set the course id and page no.
						//List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
						//nameValuePairs.add(new BasicNameValuePair("CourseId",courseParams[0]));            
						//nameValuePairs.add(new BasicNameValuePair("PageNo",contentUrls[0]));            
						//nameValuePairs.add(new BasicNameValuePair("Type","Content"));
						//nameValuePairs.add(new BasicNameValuePair("SimId",courseParams[1]));
							
							
						try {
						//		httpGetContentPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
						//		System.out.println("Calling url = " + readUrl);
								
								HttpResponse execute = client.execute(httpGetText);
							//	HttpResponse execute = client.execute(httpGetContentPost); //postChange
								InputStream content = execute.getEntity().getContent();

								BufferedInputStream buffer = new BufferedInputStream(content);

								//open the local text file for storage
								contentFileName = courseId+"_"+contentUrls[2];

								System.out.println("main file name in course viewer = " + contentFileName);
								//					FileOutputStream fText = openFileOutput(textFileName, MODE_WORLD_READABLE); 
								
								//open world readable for video files as video player needs so. Others open as private
							//	FileOutputStream fText;
								OutputStream fText;
								if ((pageType == 3) || (pageType == 4) || (pageType == 5))
								{
								//	fText = openFileOutput(mainContentFileName, MODE_WORLD_READABLE);
								//	File textFile = new File(getExternalFilesDir(null), contentFileName);
									File textFile = new File(storageDirectory, contentFileName);
									
									SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");

									// Create the cipher
						            Cipher cEncrypt = Cipher.getInstance("AES");
						            cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
						            fText = new CipherOutputStream(new FileOutputStream(textFile), cEncrypt); 						
									// fText = new FileOutputStream(textFile);
								}
								else
								{
									//create encrypted content for text content
									
								    //fText = openFileOutput(mainContentFileName, MODE_PRIVATE);
									//File textFile = new File(getExternalFilesDir(null), contentFileName);
									File textFile = new File(storageDirectory, contentFileName);
									//fText = new FileOutputStream(textFile);
									/*
									//create the key for encryption
									SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
						            sr.setSeed(simId.getBytes());
						            KeyGenerator kg = KeyGenerator.getInstance("AES");
						            kg.init(128, sr);
									
									SecretKeySpec encryptKey = new SecretKeySpec(kg.generateKey().getEncoded(), "AES");
									*/
									SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");

									// Create the cipher
						            Cipher cEncrypt = Cipher.getInstance("AES");
						            cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
						            fText = new CipherOutputStream(new FileOutputStream(textFile), cEncrypt); 
									
								}
								byte buf[]=new byte[10240];
								int len;
								while((len=buffer.read(buf)) >0)
									fText.write(buf,0,len);
							
								fText.close();
							} 
							catch (Exception e) {
								e.printStackTrace();
								contentFileName = null;
							}
					} //downloading content
					else
					{
						contentFileName = null;
					}
					
					//get the sound content to a local file of the same name
					if (contentUrls[1] != null)
					{
						hasSound = 1;
						//postChange
						//combine code to create post request to a single location
						httpGetText = new HttpGet(contentUrls[1]);
						//String readUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getPage.php";
						//HttpPost httpGetSoundPost = new HttpPost(readUrl);
							
						//get the session cookie here to send to server for genuine call check
						//String sessionKeyValue = getSharedPreferences(RegistrationActivity.PREFS_NAME, 0).getString(ProfileSelectionActivity.SESSION_KEY, null);
						//httpGetSoundPost.setHeader("Cookie", sessionKeyValue);
							
						//set the course id and page no.
						//List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
						//nameValuePairs.add(new BasicNameValuePair("CourseId",courseParams[0]));            
						//nameValuePairs.add(new BasicNameValuePair("PageNo",contentUrls[1]));            
						//nameValuePairs.add(new BasicNameValuePair("Type","Sound"));
						//nameValuePairs.add(new BasicNameValuePair("SimId",courseParams[1]));
						
						try {
								//postChange
								//httpGetSoundPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
								//System.out.println("Calling url for sound = " + readUrl);
								//HttpResponse execute = client.execute(httpGetSoundPost); //postChange
								HttpResponse execute = client.execute(httpGetText);
								InputStream content = execute.getEntity().getContent();

								BufferedInputStream buffer = new BufferedInputStream(content);

								//open the local sound file for storage
								/*
								//getting date to randomize the file name
								DateFormat dateFormat = new SimpleDateFormat("dd_MM_HH_mm_ss");
								//get current date time with Date()
								Date date = new Date();

								String soundFileName = courseId+"_"+contentUrls[2]+"_"+dateFormat.format(date)+"_s.mp3"; //format is courseid_pageNo_s.mp3
								*/
								//below line commented to avoid putting an extention to the file
								//String soundFileName = courseId+"_"+contentUrls[2]+"_s.mp3"; //format is courseid_pageNo_s.mp3
								soundFileName = courseId+"_"+contentUrls[2]+"_s"; //format is courseid_pageNo_s
								//FileOutputStream fSound = openFileOutput(soundFileName, MODE_WORLD_READABLE);   
								//File soundFile = new File(getExternalFilesDir(null), soundFileName);
								File soundFile = new File(storageDirectory, soundFileName);
								
								//create the encrypted sound file
								/*
								//create the key for encryption
								SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
					            sr.setSeed(simId.getBytes());
					            KeyGenerator kg = KeyGenerator.getInstance("AES");
					            kg.init(128, sr);
								
								SecretKeySpec encryptKey = new SecretKeySpec(kg.generateKey().getEncoded(), "AES");
								*/
								SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");


								// Create the cipher
					            Cipher cEncrypt = Cipher.getInstance("AES");
					            cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
					            OutputStream fSound = new CipherOutputStream(new FileOutputStream(soundFile), cEncrypt); 

								//FileOutputStream fSound = new FileOutputStream(soundFile);
								byte buf[]=new byte[1024];
								int len;
								int lengthRead = 0;
								while((len=buffer.read(buf)) >0)
								{
									fSound.write(buf,0,len);
									lengthRead++;
								}
								
								fSound .close();
								
								System.out.println("Name = "+ soundFileName + " size  = " + lengthRead);
								
								} 
							catch (Exception e) 
							{
								e.printStackTrace();
								soundFileName = null;
								contentFileName = null; //making content file name also null in case sound download had an exception
							}
					} //if (contentUrls[1] != null)
					else
						soundFileName = null;
					
					//save the page details locally if required
					if (contentFileName != null)
					{
							AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

							SQLiteDatabase db = dbHelper.getWritableDatabase();
							ContentValues values = new ContentValues();
							values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, courseId);
							values.put(AnytimeLearnDb.PAGE_NO_COL_NAME, pageNoToRetrieve);
							values.put(AnytimeLearnDb.HAS_SOUND_COL_NAME, hasSound);
							values.put(AnytimeLearnDb.HAS_NEXT_COL_NAME, 0);
							values.put(AnytimeLearnDb.HAS_PREV_COL_NAME, 0);
							values.put(AnytimeLearnDb.PAGE_TYPE_COL_NAME, pageType);
							
							long insertValue = db.insert(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, null, values);
							System.out.println("Insert value = " + insertValue);
							db.close(); // Closing database connection			
					}
					
					savedFiles[2] = contentUrls[2]; //this is the page no.
					savedFiles[3] = contentUrls[3]; //this is the page type
					
					return null;
						
			} //doInBackground()

			@Override
			protected void onPostExecute(Object[] pageData) 
			{
				loadProgress.dismiss();
				if (contentFileName!= null)
				{
					currentTextUrl = contentFileName;
					if (soundFileName != null)
						currentSoundUrl = soundFileName;
					currentPageType = Integer.toString(pageType);
					currentPageNo = Integer.toString(pageNoToRetrieve);
					displayCurrentUrls();
				}
			} //onPostExecute
			
			private String[] parseResponseAndGetUrls(String response, String[] courseParams) {
			    String[] contentValues = response.split("&");
			    
			    //format is contentValue[0] the number value indicates the current page no. of text
			    //contentValue[1] the number 1 indicates sound page corresponding to the text page no
			    //contentValue[2] number 0 indicates no previous, 1 indicates previous
			    //contentValue[3] number 0 indicates no next, 1 indicates next
			    //contentValue[4] number 0 indicates no next, 1 indicates next
			    int pageNo = Integer.valueOf(contentValues[0]);
			    int soundExists = Integer.valueOf(contentValues[1]);
			    int pageType = Integer.valueOf(contentValues[4]);
		System.out.println("Page no = " + pageNo + " soundExists = "+soundExists+ " pageType = "+ pageType);	
			    
			    String[] returnValues = new String[4];
			    
			    returnValues[0] = null;
			    if (pageNo != 0) //not the last page
			    {
			    	returnValues[0] = getString(R.string.baseURL) + courseParams[0] + "/" + pageNo;
			    	//returnValues[0] = ""+ pageNo; //postChange 
					
				    switch (pageType)
				    {
				    	case 2 : returnValues[0] = returnValues[0] + ".swf"; break;
				    	case 3 :  returnValues[0] = returnValues[0] + ".mp4"; break;
				    	case 4 :  returnValues[0] = returnValues[0] + ".ogv"; break;
				    	case 5 :  returnValues[0] = returnValues[0] + ".avi"; break;
				    	default : returnValues[0] = returnValues[0] + ".html"; break;
				    }

		/*
				    if (pageType == 2) //swf page
			    		returnValues[0] = returnValues[0] + ".swf";
			    		//returnValues[0] = returnValues[0] + ".mp4";  //changing to mp4 temporarily
			    	else //html page (content or question)
			    		returnValues[0] = returnValues[0] + ".html"; */
			    }
			    	
			    returnValues[1] = null;
			    if (soundExists > 0) //>0 is the page no. of the sound page
		    	    returnValues[1] = getString(R.string.baseURL) + courseParams[0] + "/" + pageNo + "s.mp3"; //postChange
			    	//returnValues[1] = pageNo + "s.mp3"; //postChange
			    
			    returnValues[2] = "" + pageNo;
			    returnValues[3] = "" + pageType;
			    
			    return returnValues;
			}

		} //CoursePageRetrieveAsyncTask	
		
} //end of class
