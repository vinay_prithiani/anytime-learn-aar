package in.AnytimeLearn;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

public class PostRequestPerform {
	
	String urlToConnect;
	List<NameValuePair>  postParametersToSend;
	JSONObject objectToSend;
	
	public PostRequestPerform(String url, List<NameValuePair>  postParameters)
	{
		urlToConnect = url;
		postParametersToSend = postParameters;
		objectToSend = null;
	}

	public PostRequestPerform(String url, JSONObject sendObject)
	{
		urlToConnect = url;
		postParametersToSend = null;
		objectToSend = sendObject;
	}

	public InputStream getResponse()
	{
		HttpClient client = new DefaultHttpClient();
		InputStream content;
		try {
			HttpResponse execute;
			HttpPost httpPostRequest = new HttpPost(urlToConnect);
			StringEntity entityStringToSend;
			if (objectToSend != null) //need to send json object
			{
				entityStringToSend = new StringEntity(URLEncoder.encode(objectToSend.toString(), HTTP.DEFAULT_CONTENT_CHARSET));
				entityStringToSend.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			}
			else //we have form parameters to post
				entityStringToSend = new UrlEncodedFormEntity(postParametersToSend);
			
			//httpPostRequest.setEntity(new UrlEncodedFormEntity(postParametersToSend));
			httpPostRequest.setEntity(entityStringToSend);
			execute = client.execute(httpPostRequest);
			content = execute.getEntity().getContent();
		}
		catch (Exception e)
		{
System.out.println("Error in post connection");
			e.printStackTrace();
			return null;
		}
		
		return content;
	} //getResponse()

}
