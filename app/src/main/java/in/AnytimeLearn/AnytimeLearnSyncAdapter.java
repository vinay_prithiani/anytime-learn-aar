package in.AnytimeLearn;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;

/** * Handle the transfer of data between a server and an 
 * * app, using the Android sync adapter framework. */
public class AnytimeLearnSyncAdapter extends AbstractThreadedSyncAdapter 
{    
	// Global variables    
	// Define a variable to contain a content resolver instance    
	ContentResolver mContentResolver;    
	protected JSONArray jsonExamAnswerArray = null;
	
	/**     * Set up the sync adapter     */    
	public AnytimeLearnSyncAdapter(Context context, boolean autoInitialize) 
	{        
		super(context, autoInitialize);        
		/** If your app uses a content resolver, get an instance of it         
		 * * from the incoming Context         */        
		mContentResolver = context.getContentResolver();    
	}    
	
	/*** Set up the sync adapter. This form of the     
	 * * constructor maintains compatibility with Android 3.0     
	 * * and later platform versions     */    
	//public AnytimeLearnSyncAdapter(Context context,boolean autoInitialize,boolean allowParallelSyncs)
	//{
		//super(context, autoInitialize, allowParallelSyncs);
		/** If your app uses a content resolver, get an instance of it         
		 * * from the incoming Context         
		 * */        
		//mContentResolver = context.getContentResolver();
	//}
	
	 /*     * Specify the code you want to run in the sync adapter. The entire     
	  * * sync adapter runs in a background thread, so you don't have to set     
	  * * up your own background processing.     */    
	@Override    
	public void onPerformSync(Account account,Bundle extras, String authority, ContentProviderClient provider,SyncResult syncResult) 
	{    
		/*     * Put the data transfer code here.     */
		System.out.println("In perform sync");
	
		JSONArray jsonCourseUpdateDataArray = new JSONArray();
		Context syncAppContext = getContext();

		//get the last transfer time
		SharedPreferences settings = syncAppContext.getSharedPreferences(syncAppContext.getString(R.string.PREFS_NAME), 0);
		String lastSyncDateTime = settings.getString(syncAppContext.getString(R.string.LAST_SYNC_TIME), null); //create storage location if it does not exist
		String subscriberId = settings.getString(syncAppContext.getString(R.string.DEVICE_ID), null);
//System.out.println("Last sync time = " + lastSyncDateTime + " subscriber id = " + subscriberId);

		if (lastSyncDateTime == null)
			lastSyncDateTime = "0"; //make time 0 to get all rows
		
		try
		{
			//get max page course details
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(syncAppContext);
		
			//	read from the db here
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			Cursor cursor = db.query(AnytimeLearnDb.COURSE_TABLE_NAME, new String[] { AnytimeLearnDb.COURSE_ID_COL_NAME, AnytimeLearnDb.MAX_PAGE_SEEN, AnytimeLearnDb.LAST_ACCESS_DATETIME}, 
				AnytimeLearnDb.LAST_ACCESS_DATETIME + ">= ?", new String[] { lastSyncDateTime }, null, null, null, null);
			if (cursor != null)
			{
				while (cursor.moveToNext()) 			//means course row exists.
				{
					JSONObject singleCourseUpdateDetail = new JSONObject();
					int courseIdToUpdate = cursor.getInt(0);
					int maxPageToUpdate = cursor.getInt(1);	
					String dateTimeToUpdate = cursor.getString(2);
				
					//System.out.println("Course id = " +   courseIdToUpdate + " max page = " +  maxPageToUpdate + " last access datetime = " + dateTimeToUpdate);
			
					singleCourseUpdateDetail.put("CourseId", courseIdToUpdate);
					singleCourseUpdateDetail.put("MaxPage", maxPageToUpdate);
					singleCourseUpdateDetail.put("LastAccess", dateTimeToUpdate);
					jsonCourseUpdateDataArray.put(singleCourseUpdateDetail);
				}
			}	
			cursor.close();
			db.close();

			//	now update the contents on server
			String courseProgressUpdateUrl = syncAppContext.getString(R.string.baseURL) + syncAppContext.getString(R.string.MOBLE_APP_EXTENSION) + "updateCourseProgress.php";

			//	create the json object to send all parameters
			JSONObject masterObjectElement = new JSONObject();
			masterObjectElement.put("SimId",subscriberId);

			if (jsonCourseUpdateDataArray.length() != 0) //add the course data if there are any updates
				masterObjectElement.put("CourseProgress", jsonCourseUpdateDataArray);
			
			PostRequestPerform courseProgressUpdate = new PostRequestPerform(courseProgressUpdateUrl, masterObjectElement);
			InputStream responseFromCourseProgressUpdate = courseProgressUpdate.getResponse();
			
			//discard response from course progress update
			
			//send the details of any completed exams
			String completedExamIds[] = getTestIdsToBeUpdated(syncAppContext);
			if (completedExamIds != null)
				for (int examIdCount = 0; examIdCount < completedExamIds.length; examIdCount++)
					uploadExamResult(syncAppContext, subscriberId, completedExamIds[examIdCount]);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			return;
		}
		
		//put the current time as sync time
		Date currentDate = new Date();
		String formattedCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDate);

		SharedPreferences.Editor editor = settings.edit();      
		editor.putString(syncAppContext.getString(R.string.LAST_SYNC_TIME), formattedCurrentDate);
	   	// Commit the edits!      
		editor.commit();

	}
	
	private String[] getTestIdsToBeUpdated(Context appContext)
	{
		String completeExamIds[] = null;
		
		//get answers to this exam
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(appContext);
		
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		//get answers to each question
		Cursor cursor = db.query(AnytimeLearnDb.EXAM_TABLE_NAME, new String[] { AnytimeLearnDb.COURSE_ID_COL_NAME }, 
				AnytimeLearnDb.EXAM_STATUS + "=?",
				new String[] {Character.toString(AnytimeLearnDb.EXAM_STATUS_COMPLETE)}, null, null, null, null);
		if (cursor != null)
		{
			completeExamIds = new String[cursor.getCount()];
			int currentRowCount = 0;
			//get all the answers
			while (cursor.moveToNext())
				completeExamIds[currentRowCount++] = cursor.getString(0);
				
			cursor.close();
		}

		db.close();
		
		return completeExamIds;
		
	} //getTestIdsToBeUpdated()
	
	private void uploadExamResult(Context appContext, String subscriberId, String examId)
	{
System.out.println("Updating exam id = "  + examId);
		jsonExamAnswerArray = new JSONArray();
		
		//get answers to this exam
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(appContext);
		
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		//get answers to each question
		Cursor cursor = db.query(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, new String[] { AnytimeLearnDb.QUESTION_SEQUENCE_NO, AnytimeLearnDb.QUESTION_PAGE_NO, AnytimeLearnDb.GIVEN_ANSWER_OPTION }, 
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {examId}, null, null, AnytimeLearnDb.QUESTION_SEQUENCE_NO + " ASC", null);
		if (cursor != null)
		{
			//get all the answers
			while (cursor.moveToNext())
			{
				try
				{
					JSONObject singleQuestionDetail = new JSONObject();
				
					int questionSequence = cursor.getInt(0);
					int pageNo = cursor.getInt(1);
					String answerGiven = cursor.getString(2);
				
					//System.out.println("Question no = " + questionSequence + " Page no = " + pageNo + " Answer = " + answerGiven);
					singleQuestionDetail.put("SeqNo", questionSequence);
					singleQuestionDetail.put("PageNo", pageNo);
					singleQuestionDetail.put("Answer", answerGiven);
					jsonExamAnswerArray.put(singleQuestionDetail);
				}
				catch (JSONException jse)
				{
					jse.printStackTrace();
					//continue moving to next element
				}
			}
				
			
			cursor.close();
		}
		db.close();
		
		//upload and delete exam record
		String examAnswerListUrl = appContext.getString(R.string.baseURL) + appContext.getString(R.string.MOBLE_APP_EXTENSION) + "takeExamAnswers.php";

		//create the json object to send all parameters
		JSONObject masterObjectElement = new JSONObject();
		try
		{
			masterObjectElement.put("CourseId",examId);
			masterObjectElement.put("SimId",subscriberId);
		
			if ((jsonExamAnswerArray != null) && (jsonExamAnswerArray.length() != 0))//add the answer data if there are any answers
				masterObjectElement.put("AnswerList", jsonExamAnswerArray);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			//cannot do anything here but return
			return;
		}


		PostRequestPerform examAnswerUpdate = new PostRequestPerform(examAnswerListUrl, masterObjectElement);
		InputStream responseFromExamAnswerUpdate = examAnswerUpdate.getResponse();
/*
InputStreamReader isr = new InputStreamReader(responseFromExamAnswerUpdate);
BufferedReader br = new BufferedReader(isr);
String fileLine = "";
String fileData = "";
try
{
while ((fileLine = br.readLine()) != null)
    {
   	 fileData+=fileLine;
    }
     
    br.close();
    isr.close();
}
catch (Exception e)
{
	e.printStackTrace();
}

System.out.println("Output from server for test data = " + fileData);
*/

		if (responseFromExamAnswerUpdate != null)
		{
			//delete this exam's data from internal tables
			//delete all rows of the page content that exist
			db = dbHelper.getWritableDatabase();
			String deleteWhereClause = AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?";
			db.delete(AnytimeLearnDb.EXAM_TABLE_NAME, deleteWhereClause, new String[]{examId});
			db.delete(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, deleteWhereClause, new String[]{examId});

			db.close(); // Closing database connection
		}
	} //prepareExamAnswerUpload()
}
