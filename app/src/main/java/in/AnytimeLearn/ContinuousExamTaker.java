package in.AnytimeLearn;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;

public class ContinuousExamTaker extends Activity {
	protected TextView testQuestion, answerAndMessages, examTimeClock;
	protected RadioGroup optionsRadioGroup;
	protected Button nextButton, evaluateButton, previousButton, completeExamButton, uploadAnswerButton;
	Intent downloadService;
	protected String currentTextUrl, currentSoundUrl, currentPageNo, currentPageType;
	boolean testLocallyCached = false;
	protected String subscriberId, testName;
	protected int testId, revertBackQuestionNum = -1, lastQuestionNum = -1, totalQuestions = -1, /* remainingTime = 0, */nextPageToLoad, totalExamDuration = 0;
	protected long lastTickTimeInMillis, timeToReduceInNextStart = 0, remainingTime = 0;
	protected int[] rightOption, questionNumberArray;
	protected String[] answeredOptionsArray;
	protected String testType, answerExplanation;
	protected boolean multipleRight = false, questionListNotInitialized = true;
	protected RelativeLayout currentLayout;
	protected LinearLayout multipleRightChoice, examViewerScrollLayout;
	private CountDownTimer examTimeKeeper = null;
	protected byte[] deviceKey;
	//ScrollView examViewerScrollLayout;
//	private ScaleGestureDetector mScaleDetector;
	private View titleView, pageNumberView;
	protected final int DEFAULT_TEXT_SIZE = 20;
	protected ProgressDialog loadProgress, initializeProgress, uploadProgress;
	protected String storageDirectory = null;
	protected JSONArray jsonExamAnswerArray = null;
	// Constants for Account
	// The authority for the sync adapter's content provider
	protected String AUTHORITY = null;
	// An account type, in the form of a domain name
	protected String ACCOUNT_TYPE = null;
	// The account name
	//public static final String ACCOUNT = "dummyaccount";
	// Instance fields
	Account mAccount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

System.out.println("Continuous Exam Taker called");

		LayoutInflater inflater = getLayoutInflater();
		//examViewerScrollLayout = (ScrollView) inflater.inflate(R.layout.activity_exam_taker,null);
		examViewerScrollLayout = (LinearLayout) inflater.inflate(R.layout.activity_exam_taker,null);

	//	setContentView(R.layout.activity_test_viewer);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(examViewerScrollLayout);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);

		testQuestion = (TextView)findViewById(R.id.examQuestionText);
		optionsRadioGroup = (RadioGroup)findViewById(R.id.optionsGroup);
		answerAndMessages = (TextView)findViewById(R.id.answerAndNotificationsExam);
		nextButton = (Button)findViewById(R.id.nextExamQuestion);
		evaluateButton = (Button)findViewById(R.id.Answer);
		previousButton = (Button)findViewById(R.id.previousExamQuestion);
		completeExamButton = (Button)findViewById(R.id.completeExam);
		uploadAnswerButton = (Button)findViewById(R.id.uploadExamResult);
		examTimeClock = (TextView)findViewById(R.id.countDownClock);

		titleView = findViewById(R.id.myTitle);
		pageNumberView = findViewById(R.id.pageNumber);

		currentLayout = (RelativeLayout)findViewById(R.id.examLayout);

		//create a new linear layout to show multiple right choice options
		multipleRightChoice = new LinearLayout(this);
		multipleRightChoice.setOrientation(LinearLayout.VERTICAL);
		//multipleRightChoice.setId(2345); //temporary id as the code below works only when an id is set
		multipleRightChoice.setId(R.id.continuousExamTakerOptions); //temporary id as the code below works only when an id is set

		/*
		testQuestion.setText("Sample question");
		for (int count = 0; count < 4; count ++)
		{
			RadioButton optionButton = new RadioButton(this);
			optionButton.setText("Option " + count);
			options.addView(optionButton);
		} */

		//setting text size
		 testQuestion.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
		 answerAndMessages.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);

		//setup account constants and get the existing account
		AUTHORITY = getString(R.string.SYNC_AUTHORITY);
		ACCOUNT_TYPE = getString(R.string.SYNC_ACCOUNT_TYPE);
		// Get an instance of the Android account manager
		AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
		Account[] allAnytimeLearnAccounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
		if (allAnytimeLearnAccounts != null)
			mAccount = allAnytimeLearnAccounts[0]; //there is only one account that we need
		else
			System.out.println("No account exists");

		/* Below code did not work. Replaced by a larger text size above.
		//create gesture detector
		mScaleDetector = new ScaleGestureDetector(testViewerScrollLayout.getContext(), new OnScaleGestureListener() {
		    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
			public void onScaleEnd(ScaleGestureDetector detector) {
		    	testViewerScrollLayout.setScaleX((float) 2.0);
		    	testViewerScrollLayout.setScaleY((float) 2.0);
		    	System.out.println("On scale end called");
		    }
		    public boolean onScaleBegin(ScaleGestureDetector detector) {
		    	System.out.println("On scale begin called");
		        return true;
		    }
		    public boolean onScale(ScaleGestureDetector detector) {
		    	System.out.println("On scale called");
		        return false;
		    }
		});

		testViewerScrollLayout.setOnTouchListener(new OnTouchListener()
		{
			public boolean onTouchEvent(MotionEvent event) {
			    mScaleDetector.onTouchEvent(event);
			    return true;
			}

			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		*/
		//check if the application's key exists. If not disallow load of information
		if (!LoadEncryptionKeyAndStorageLoc())
			displayMessage(getString(R.string.START_HTML)+ "Device key not found" + getString(R.string.END_HTML));

		//disable buttons till first page download is done
		evaluateButton.setEnabled(false);
		previousButton.setEnabled(false);
		nextButton.setEnabled(false);

		//complete exam button enabled only when last page is reached
		completeExamButton.setVisibility(View.INVISIBLE);

		initializeExam();

	} //onCreate()

    /** Called when the activity is destroyed. */
    @Override
    public void onDestroy() {
        super.onDestroy();

        //store current page no. and total pages in db
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		//Update the row corresponding to this course id only. The row would be created in initializeTest
		//this row is against this test id and needs to be fixed if test/eval modes are used for the same test
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(AnytimeLearnDb.LAST_DISPLAY_PAGE, lastQuestionNum); // put current page
		values.put(AnytimeLearnDb.NUM_PAGES, totalQuestions); // put totalPage
		values.put(AnytimeLearnDb.EVALUATION_TYPE_COL_NAME, testType); // put evaluation Type
		long updateValue = db.update(AnytimeLearnDb.COURSE_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});

		values = new ContentValues();
		values.put(AnytimeLearnDb.LAST_DISPLAY_QUESTION_NO, lastQuestionNum); //no questions displayed so far
		//minutes to be updated here based on usage
		int minutesRemaining = (int) ((remainingTime / 60000) + (((remainingTime % 60000) < 30000) ? 0 : 1)); //add 1 minute only if more than half a min remains
		//int usedTime = totalExamDuration - remainingTime;
		int usedTime = totalExamDuration - minutesRemaining;
		values.put(AnytimeLearnDb.USED_MINUTES, usedTime); // used time is in minutes

		updateValue = db.update(AnytimeLearnDb.EXAM_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});

System.out.println("Last Question num = " + lastQuestionNum);
		db.close();

		//cancel the timer. Need to update remaining time in db
		if (examTimeKeeper != null)
			examTimeKeeper.cancel();

		//keep a call to sync
	     // Pass the settings flags by inserting them in a bundle
		Bundle settingsBundle = new Bundle();
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
		/** Request the sync for the default account, authority, and
		 * * manual sync settings         */
		ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);

		//also update page no. on server
		//updating the last page on the server if course id is not null
		UpdatePageNoAsyncTask pageUpdateTask = new UpdatePageNoAsyncTask();
		pageUpdateTask.execute();
    }

    private void checkDownloadStatusAndDownloadAll()
    {
    	int numRecords = -1;

    	//no need to check cache status
    //	if (testLocallyCached)
    	{
    		//check no. of page records against total pages
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			//read the last page stored
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME },
					AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
					new String[] {Integer.toString(testId)}, null, null, null, null);
			if (cursor != null)
			{
				numRecords = cursor.getCount();

				if (numRecords != totalQuestions) //total pages is already filled up before this
					commenceTestDownload();

				cursor.close();
			}
			db.close();
    	}
    } //checkDownloadStatusAndDownloadAll()

    private void commenceTestDownload()
    {
	//	Handler handler = new TestDisplayHandler();

System.out.println("communicating with service for all downloads");

	//	Messenger messenger = new Messenger(handler);
		downloadService = new Intent(this, CourseRetrieverDownloadOptimized.class);
	//	downloadService.putExtra("MESSENGER", messenger);
		downloadService.putExtra("courseId", Integer.toString(testId));
		downloadService.putExtra("simId", subscriberId);
		downloadService.putExtra("Trigger", "AllTestPages");
		downloadService.putExtra("TotalPages", totalQuestions);
		//downloadService.putExtra("cacheStatus", courseLocallyCached);
		startService(downloadService);
    } //commenceTestDownload()

    //returns false if encryptionKey cannot be loaded. True otherwise
	private boolean LoadEncryptionKeyAndStorageLoc()
	{
		SharedPreferences settings = getSharedPreferences(getString(R.string.PREFS_NAME), 0);
		//	key already created during registration

		String deviceKeyString = settings.getString(getString(R.string.DEVICE_KEY), null);
		if (deviceKeyString == null)
		{
		  System.out.println("Device key not found");
		  return false;
		}

		deviceKey = Base64.decode(deviceKeyString, Base64.DEFAULT);

		storageDirectory = settings.getString(getString(R.string.STORAGE_LOCATION), null);
		if (storageDirectory == null)
		{
			System.out.println("Storage directory not found");
			return false;
		}

		return true;
		//  System.out.println("Key retrieved in activity = " + deviceKeyString.getBytes());

	} //LoadEncryptionKeyAndStorageLoc()

	private void initializeExam()
	{
		//comment below statement if caching is to be tested
		testLocallyCached = getIntent().getBooleanExtra(getString(R.string.CACHE_STATE), false);
		testId = Integer.parseInt(getIntent().getStringExtra("COURSE_ID"));
		testName = getIntent().getStringExtra("COURSE_NAME");

		subscriberId = getIntent().getStringExtra("SIM_ID");

		//check if exam initialized before
		boolean examInitialized = checkIfExamInitialized();

		if (examInitialized)
		{
			boolean proceedWithQuestions = false;
			String examStatus = null;
			String examStartTime;

			//get last displayed page and if time remains to do the exam
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			//read the last page stored
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			Cursor cursor = db.query(AnytimeLearnDb.EXAM_TABLE_NAME, new String[] { AnytimeLearnDb.EXAM_DURATION_MINUTES, AnytimeLearnDb.USED_MINUTES, AnytimeLearnDb.LAST_DISPLAY_QUESTION_NO, AnytimeLearnDb.NUM_QUESTIONS, AnytimeLearnDb.EXAM_STATUS, AnytimeLearnDb.EXAM_START_TIME },
					AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
					new String[] {Integer.toString(testId)}, null, null, null, null);

			if (cursor != null)
			{
				if (cursor.moveToFirst()) 	//read values for time and status
				{
					totalExamDuration =  cursor.getInt(0);
					int elapsedExamDuration =  cursor.getInt(1);
					totalQuestions = cursor.getInt(3);
					int lastAnsweredQuestion = cursor.getInt(2);
					if (lastAnsweredQuestion == totalQuestions)
						revertBackQuestionNum = lastQuestionNum =  lastAnsweredQuestion; //load next question to be called afte this. check if last question already answered.
					else
						revertBackQuestionNum = lastQuestionNum =  lastAnsweredQuestion + 1;
					examStatus = cursor.getString(4);
					examStartTime = cursor.getString(5);

					System.out.println("Exam Start time = " + examStartTime);

					if (examStartTime != null)
					{
						//convert start datetime to time
						SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
						Date startDateTime = null;
						try {
							startDateTime = format.parse(examStartTime);
						}
						catch (Exception e)
						{
							e.printStackTrace();
							startDateTime = null;
						}

						long startTimeInMillis = 0;
						if (startDateTime != null)
							startTimeInMillis = startDateTime.getTime();

						//get current time
						long currentTime = System.currentTimeMillis();

						//System.out.println("Current time = " + currentTime + " start time = " + startTimeInMillis);

						//subtract and see if minutes remain
						long elapsedTimeSinceStart = currentTime - startTimeInMillis;
						long examDurationInMillis = totalExamDuration*60000;

						//System.out.println("Elapsed time since start" + elapsedTimeSinceStart + " Duration in Millis = " + examDurationInMillis);

						if (elapsedTimeSinceStart < examDurationInMillis)
							remainingTime = examDurationInMillis - elapsedTimeSinceStart;
						else
						{
							remainingTime = 0;
							markExamComplete(); //required as in active mode user may time out even when closed
							examStatus = String.valueOf(AnytimeLearnDb.EXAM_STATUS_COMPLETE); //change internal variable
						}
					}

					//remaining time calculated differently in case of continuousExamTaker
					//remainingTime = totalExamDuration - elapsedExamDuration;
					//remainingTime = (totalExamDuration - elapsedExamDuration) * 60000; //making milliseconds out of time remaining

					if (remainingTime > 0) //time remains for exam to complete
					{
						proceedWithQuestions = true;
					}

					if (examStatus.compareTo(Character.toString(AnytimeLearnDb.EXAM_STATUS_ACTIVE)) != 0)
						proceedWithQuestions = false;
				}
				cursor.close();
			}
			db.close();
System.out.println("Last question number in initialize = " + lastQuestionNum);
			if (proceedWithQuestions)
			{
				loadNextQuestion(); //load the next question that is not answered
				//startTimer(1); //display time clock
			}
			else
			{
				displayMessage("Exam not available on your device as you might have marked it complete already. Please get in touch with your training provider");

				//show sync upload button here
				if ((examStatus != null) && (examStatus.compareTo(Character.toString(AnytimeLearnDb.EXAM_STATUS_COMPLETE)) == 0))
				{
					uploadAnswerButton.setVisibility(View.VISIBLE);
					uploadAnswerButton.setEnabled(true);
				}
				return;
			}
		} //if (examInitialized)
		else
		{
			//initialize the exam
			//show progress bar for initializing test
			initializeProgress = new ProgressDialog(this);
			initializeProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			initializeProgress.setCanceledOnTouchOutside(false);
			initializeProgress.setCancelable(false);
			initializeProgress.setMessage("Initializing exam..");
			initializeProgress.show();

			ExamQuestionListRetrieveAsyncTask examQuestionListRetrieve = new ExamQuestionListRetrieveAsyncTask();

			examQuestionListRetrieve.execute();
		}//else initializeExam

		if (!testLocallyCached) //put a row to indicate that course first downloaded on today's date/time
		{
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			//delete all rows of the page content that exist
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			String deleteWhereClause = AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?";
			db.delete(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, deleteWhereClause, new String[]{Integer.toString(testId)});

			//get the current date
			Date currentDate = new Date();
			String formattedCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(currentDate);
			ContentValues values = new ContentValues();
			values.put(AnytimeLearnDb.DOWNLOAD_DATE_COL_NAME, formattedCurrentDate); // put current time
			values.put(AnytimeLearnDb.COURSE_NAME_COL_NAME, testName); // put course name
			values.put(AnytimeLearnDb.COURSE_TYPE_COL_NAME, "E"); // put type as exam
			// Update Row
			long updateValue = db.update(AnytimeLearnDb.COURSE_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});
			System.out.println("Update value = " + updateValue);

			//if update did not work, insert a new row
			if (updateValue != 1) //only 1 row should have been updated
			{
				values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, testId);
				long insertValue = db.insert(AnytimeLearnDb.COURSE_TABLE_NAME, null, values);
				System.out.println("Insert value = " + insertValue);
			}
			db.close(); // Closing database connection

		} //end of row insertion in case course is first time downloaded


		//disable the messages
		//answerAndMessages.setVisibility(View.INVISIBLE);
		//nextButton.setVisibility(View.INVISIBLE);

    	//set the title to parameter course name
    	if (testName != null)
    		((TextView)titleView).setText("Anytime Learn - " + testName);

	} //initializeTest()

	private void markExamComplete()
	{
		//mark exam as complete. Sync will be scheduled when the user exits the app
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(AnytimeLearnDb.EXAM_STATUS, Character.toString(AnytimeLearnDb.EXAM_STATUS_COMPLETE)); // put status as complete
		long updateValue = db.update(AnytimeLearnDb.EXAM_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});
		//System.out.println("Update of exam table = " + updateValue);
		//not using the update value

		db.close(); // Closing database connection
	} //markExamComplete()

	private boolean checkIfExamInitialized()
	{
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		//read the last page stored
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor = db.query(AnytimeLearnDb.EXAM_TABLE_NAME, new String[] { AnytimeLearnDb.EXAM_DURATION_MINUTES },
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {Integer.toString(testId)}, null, null, null, null);

		boolean returnValue = false;
		if (cursor != null)
		{
			if (cursor.moveToFirst()) 	//means exam row exists
				returnValue = true;
			cursor.close();
		}

		db.close();
		return returnValue;

	} //checkIfExamInitialized()

	/* Not required as exam initializing is different
	private void getTestAttributes()
	{
		//first fill from db if the course is locally cached
		if (testLocallyCached)
		{
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			//read the last page stored
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			Cursor cursor = db.query(AnytimeLearnDb.COURSE_TABLE_NAME, new String[] { AnytimeLearnDb.NUM_PAGES, AnytimeLearnDb.LAST_DISPLAY_PAGE, AnytimeLearnDb.EVALUATION_TYPE_COL_NAME},
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {Integer.toString(testId)}, null, null, null, null);
			if (cursor != null)
			{
				if (cursor.moveToFirst()) 	//means page row exists.
				{
					totalQuestions =  cursor.getInt(0);
					lastQuestionNum = cursor.getInt(1);
					testType = cursor.getString(2);
				}
				cursor.close();
			}

			db.close();
		}

		//show progress bar for initializing test
		initializeProgress = new ProgressDialog(this);
		initializeProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		initializeProgress.setMessage("Initializing test..");
		initializeProgress.show();

		TestAttributeRetrieveAsyncTask testAttributeRetrieve = new TestAttributeRetrieveAsyncTask();

		testAttributeRetrieve.execute();

	} //getTestAttributes()
	*/

    /** Called when save answer button is clicked  **/
	public void saveAnswer(View view)
	{
		saveThisAnswer();
	}

	private void saveThisAnswer()
	{
		String answerResponseText = "";
		String answeredOptions = "";

		//check if there are answers and form a string from the responses.
		boolean answerGiven = false;
		if (multipleRight)
		{
			 for (int count = 0; count < multipleRightChoice.getChildCount(); count++)
				 if (((CheckBox)multipleRightChoice.getChildAt(count)).isChecked())
				 {
					 answerGiven = true;
					//option string created as 1:2: : at end should be removed while parsing. First option is marked as 1 and count is 0 onwards, therefore 1 being added
					 answeredOptions += (count+1) + ":";
				 }
		}
		else if (optionsRadioGroup.getCheckedRadioButtonId() != -1)
		{
			answerGiven = true;
			answeredOptions += optionsRadioGroup.getCheckedRadioButtonId() + ":";
		}

		//save the response to local db if there is an answer
		if (answerGiven)
		{
			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			SQLiteDatabase db = dbHelper.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(AnytimeLearnDb.GIVEN_ANSWER_OPTION, answeredOptions);
			long updateValue = db.update(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, values,
					AnytimeLearnDb.COURSE_ID_COL_NAME + "=? AND " + AnytimeLearnDb.QUESTION_SEQUENCE_NO + "=?",
				new String[]{Integer.toString(testId), Integer.toString(lastQuestionNum)});

			System.out.println("Update value for option save= " + updateValue);
			db.close(); // Closing database connection

			answeredOptionsArray[lastQuestionNum] = answeredOptions;
		}

		evaluateButton.setEnabled(false);
	} //saveAnswer()

	private boolean rightAnswerEvaluation()
	{
		if (multipleRight)
		{
			int rightOptionCount = 0;
			int numCorrectAnswers = 0, numWrongAnswers = 0;
			for (int count = 0; count < multipleRightChoice.getChildCount(); count++)
			{
				CheckBox optionCheckBox = (CheckBox)multipleRightChoice.getChildAt(count);
				int boxCount = count+1; //child at 0 is option #1
				if (boxCount > rightOption[rightOptionCount]) //see if we have reached the next right option
					if ((rightOptionCount+1) < rightOption.length)
						rightOptionCount++;
				if (optionCheckBox.isChecked())
					if (rightOption[rightOptionCount] == boxCount)
						numCorrectAnswers++;
					else
						numWrongAnswers++;
			}
			if ((numCorrectAnswers == rightOption.length) && (numWrongAnswers == 0))
				return true; //indicates only correct answers and all correct given
			else
				return false;
		} //if multiple Right
		else //only 1 correct answer
		{
			int answeredResponse = optionsRadioGroup.getCheckedRadioButtonId();

			if (answeredResponse == rightOption[0])
				return true;
		}

		return false; //any other condition return false
	} //rightAnswerEvaluation

	public void loadNextExamQuestion(View view)
	{
		//nextButton.setVisibility(View.INVISIBLE);

		if (remainingTime == 0) //time over
		{
			loadForcedCompletionScreen();
			return;
		}

//System.out.println("Last question number = " + lastQuestionNum);
		if (lastQuestionNum >= 0)
			saveThisAnswer(); //save current answer anyway before going to next

		revertBackQuestionNum = lastQuestionNum++; //increase the question no. to the current one to be shown
		loadNextQuestion();
	}

	public void loadPreviousExamQuestion(View view)
	{

		if (remainingTime == 0) //time over
		{
			loadForcedCompletionScreen();
			return;
		}

		//disable the complete exam button
		completeExamButton.setVisibility(View.INVISIBLE);

		//System.out.println("Last question num in previous = " + lastQuestionNum);
		if ((lastQuestionNum >= 0) && (lastQuestionNum < totalQuestions))
			saveThisAnswer(); //save current answer anyway

		if (lastQuestionNum >= 1)
		{
			revertBackQuestionNum = lastQuestionNum--; //reduce page no. and call the loadNextQuestion
			loadNextQuestion();
		}
		else
		{
			displayMessage("No previous question. Press next to continue");
			//lastQuestionNum = 0;
			revertBackQuestionNum = lastQuestionNum = -1; //making -1 since that will be incremented in next button.
			//nextButton.setVisibility(View.VISIBLE);
		}
	}

	private void loadForcedCompletionScreen()
	{
		//simulate click of completion button
		completeExamButton.performClick();

		//display closure message
		displayMessage("Alloted time over for this exam. Please close the app and connect to Internet to send the results");

		//disable back and forward buttons
		previousButton.setEnabled(false);
		nextButton.setEnabled(false);
	} //loadForcedCompletionScreen()

	//called when upload result button is clicked
	public void uploadExamAnswers(View view)
	{
		//create progress bar to upload exam result
		uploadProgress = new ProgressDialog(this);
		uploadProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		uploadProgress.setMessage("Uploading Exam Answers..");
		uploadProgress.show();

		//upload and delete exam record
		UploadExamResultsAsyncTask uploadExamResult = new UploadExamResultsAsyncTask();
		uploadExamResult.execute();

	}

		//called when complete button is clicked
	public void sendAnswersAndCloseExam(View view)
	{
//System.out.println("Exam completed called");
/*
		//commment from here for production load as sync cannot be tested otherwise
		jsonExamAnswerArray = new JSONArray();

		//get answers to this exam
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		SQLiteDatabase db = dbHelper.getReadableDatabase();

		//get answers to each question
		Cursor cursor = db.query(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, new String[] { AnytimeLearnDb.QUESTION_SEQUENCE_NO, AnytimeLearnDb.QUESTION_PAGE_NO, AnytimeLearnDb.GIVEN_ANSWER_OPTION },
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {Integer.toString(testId)}, null, null, AnytimeLearnDb.QUESTION_SEQUENCE_NO + " ASC", null);
		if (cursor != null)
		{
			//get all the answers
			while (cursor.moveToNext())
			{
				try
				{
					JSONObject singleQuestionDetail = new JSONObject();

					int questionSequence = cursor.getInt(0);
					int pageNo = cursor.getInt(1);
					String answerGiven = cursor.getString(2);

					System.out.println("Question no = " + questionSequence + " Page no = " + pageNo + " Answer = " + answerGiven);
					singleQuestionDetail.put("SeqNo", questionSequence);
					singleQuestionDetail.put("PageNo", pageNo);
					singleQuestionDetail.put("Answer", answerGiven);
					jsonExamAnswerArray.put(singleQuestionDetail);
				}
				catch (JSONException jse)
				{
					jse.printStackTrace();
					//continue moving to next element
				}
			}


			cursor.close();
		}
		db.close();

		//upload and delete exam record
		UploadExamResultsAsyncTask uploadExamResult = new UploadExamResultsAsyncTask();
		uploadExamResult.execute();
		//comment till here for production version where sync will do this task
*/
		//mark exam as complete. Sync will be scheduled when the user exits the app
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(AnytimeLearnDb.EXAM_STATUS, Character.toString(AnytimeLearnDb.EXAM_STATUS_COMPLETE)); // put status as complete
		long updateValue = db.update(AnytimeLearnDb.EXAM_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});
		//System.out.println("Update of exam table = " + updateValue);
		//not using the update value

		db.close(); // Closing database connection

		previousButton.setEnabled(false);

		//displayMessage("Exam closed. Please exit the app and ensure Internet connectivity to upload the answers");
		displayColoredMessage("Exam closed. Please exit the app and ensure Internet connectivity to upload the answers", Color.BLUE);
	} //sendAnswersAndCloseExam()

	protected void startTimer(int callerId)
	{
		//put the initial time
		int displayTimeInMinutes = (int) ((remainingTime / 60000) + (((remainingTime % 60000) < 30000) ? 0 : 1)); //add 1 minute only if more than half a min remains
//		examTimeClock.setText("Minutes remaining: " + remainingTime);
		examTimeClock.setText("Minutes remaining: " + displayTimeInMinutes);
		System.out.println("Starting with time remaining in millis = " + remainingTime + " called from " + callerId + " time to reduce = " + timeToReduceInNextStart);

		//initialization of lastTickTime
		Calendar rightNow = Calendar.getInstance();
		lastTickTimeInMillis = rightNow.getTimeInMillis();

		if (examTimeKeeper == null) //initialize only when not running already
		{
//			examTimeKeeper = new CountDownTimer(((remainingTime*60*1000)-timeToReduceInNextStart) , 60000) {
			examTimeKeeper = new CountDownTimer((remainingTime -timeToReduceInNextStart) , 60000) {
				public void onTick(long millisUntilFinished) {
		    		//remainingTime = (int) ((millisUntilFinished / 60000) + (((millisUntilFinished % 60000) < 30000) ? 0 : 1)); //add 1 minute only if more than half a min remains
					remainingTime = millisUntilFinished;
					int displayTimeInMinutes = (int) ((remainingTime / 60000) + (((remainingTime % 60000) < 30000) ? 0 : 1)); //add 1 minute only if more than half a min remains

					Calendar rightNow = Calendar.getInstance();
					lastTickTimeInMillis = rightNow.getTimeInMillis();

System.out.println("On tick called with time remaining time in ms = " + millisUntilFinished);
		    		//examTimeClock.setText("Minutes remaining: " + remainingTime);
					examTimeClock.setText("Minutes remaining: " + displayTimeInMinutes);
				}

				public void onFinish() {
					System.out.println("Exam over");
					examTimeClock.setText("Exam Over");
					remainingTime = 0;
				}

			};
			examTimeKeeper.start();
		} //if time keeper not null
	} //startTimer()

	protected void stopTimer()
	{
		//subtract time already elapsed
		Calendar rightNow = Calendar.getInstance();
		long stopTimeInMillis = rightNow.getTimeInMillis();
		timeToReduceInNextStart = stopTimeInMillis - lastTickTimeInMillis;


		System.out.println("Stopping with time remaining = " + remainingTime);
		//stopping timer with remaining time used as a benchmark. This will have an error in terms of seconds unused but
		//the benefit can be given to exam taker to account for time taken in loading
		if (examTimeKeeper != null)
		{
			examTimeKeeper.cancel();
			examTimeKeeper = null;
		}
	} //stopTimer()

	protected void loadNextQuestion()
	{
		/*
		//call course retriever with a specific page no. to retrieve
		downloadService.putExtra(getString(R.string.CURRENT_PAGE_NO), currentPageNo);
		downloadService.putExtra(getString(R.string.TOTAL_PAGES), totalQuestions);
		startService(downloadService);
		*/
//System.out.println("Question list not initialized = " + questionListNotInitialized);
//System.out.println("Last question num = " + lastQuestionNum);
		if (questionListNotInitialized)
		{
			questionListNotInitialized = false;

			AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

			//read the last page stored
			SQLiteDatabase db = dbHelper.getReadableDatabase();

			//get question numbers for test in ascending order
		/*	Cursor cursor = db.query(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, new String[] { AnytimeLearnDb.QUESTION_SEQUENCE_NO, AnytimeLearnDb.QUESTION_PAGE_NO },
					AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
					new String[] {Integer.toString(testId)}, null, null, AnytimeLearnDb.QUESTION_SEQUENCE_NO + " ASC", null); */
			Cursor cursor = db.query(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, new String[] { AnytimeLearnDb.QUESTION_SEQUENCE_NO, AnytimeLearnDb.QUESTION_PAGE_NO, AnytimeLearnDb.GIVEN_ANSWER_OPTION },
			AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
			new String[] {Integer.toString(testId)}, null, null, AnytimeLearnDb.QUESTION_SEQUENCE_NO + " ASC", null);
			if (cursor != null)
			{
				int numQuestions = cursor.getCount();
				questionNumberArray = new int[numQuestions];
				answeredOptionsArray = new String[numQuestions]; //stores the saved options

				int rowCounter = 0;
				while (cursor.moveToNext())
				{
					answeredOptionsArray[rowCounter] = cursor.getString(2); //store the answers already given
					questionNumberArray[rowCounter++] = cursor.getInt(1); //store the page no. of the question
				}
				cursor.close();
			}
			db.close();

			startTimer(1); //display time clock. Since question list initialized here, this is always called once per session

			checkDownloadStatusAndDownloadAll();

		/*
for (int count = 0; count < questionNumberArray.length; count++)
		System.out.println("Question no. at " + count + " = " + questionNumberArray[count]);

for (int count = 0; count < answeredOptionsArray.length; count++)
	System.out.println("Answered at " + count + " = " + answeredOptionsArray[count]);
*/
		} //questionListNotInitialized

//System.out.println("load Next question called");
		if (lastQuestionNum < totalQuestions)
		{
			boolean localCacheFound = false;

			//get the page no. to load
			nextPageToLoad = questionNumberArray[lastQuestionNum]; //array starts from 0, so next page is lastQuestionNum-1+1

			//read the cache from db
			//check if page exists locally irrespective of cache status because it might be that pages are downloaded in background after first page

			//if (testLocallyCached)
			{
				//find the next page to be shown
				AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

				//read the last page stored
				SQLiteDatabase db = dbHelper.getReadableDatabase();

				Cursor cursor = db.query(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, new String[] { AnytimeLearnDb.PAGE_NO_COL_NAME, AnytimeLearnDb.HAS_SOUND_COL_NAME, AnytimeLearnDb.HAS_NEXT_COL_NAME, AnytimeLearnDb.HAS_PREV_COL_NAME, AnytimeLearnDb.PAGE_TYPE_COL_NAME},
						AnytimeLearnDb.COURSE_ID_COL_NAME + "=? AND " + AnytimeLearnDb.PAGE_NO_COL_NAME + "=?",
						new String[] {Integer.toString(testId), Integer.toString(nextPageToLoad)}, null, null, null, null);

				if (cursor != null)
				{
					if (cursor.moveToFirst()) 	//means page row exists.
					{
						localCacheFound = true;
						currentTextUrl = testId + "_" + cursor.getInt(0);
					}
				}
				cursor.close();
				db.close();
			}

		//	localCacheFound = false;
			System.out.println("Local cached found status = " + localCacheFound);

			if (!localCacheFound)
			{
		    	//stopping the timer to remove the network time from exam time
				stopTimer();

				//retrieve specifc page
				TestPageRetrieveAsyncTask pageRetrieveTask = new TestPageRetrieveAsyncTask();
				pageRetrieveTask.execute();

				//block buttons for any unintentional clicks
				previousButton.setEnabled(false);
				nextButton.setEnabled(false);
				evaluateButton.setEnabled(false);
				//show progress bar here
		    	loadProgress = new ProgressDialog(this);
		    	loadProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				loadProgress.setCanceledOnTouchOutside(false);
				loadProgress.setCancelable(false);
		    	loadProgress.setMessage("Loading exam question..");
		    	loadProgress.show();
			}
			else
				displayCurrentUrls();
		} //if (lastQuestionNum < totalPage)
		else
		{
			//get if there unanswered questions
			boolean unAnsweredFlag = checkIfUnansweredQuestions();
			String lastPageMessage = "Last question reached.";
			if (unAnsweredFlag)
				lastPageMessage += " Some questions are unanswered.";
			lastPageMessage += " Use BACK to see previous questions or COMPLETE EXAM to end the exam";

			//displayMessage("Last question reached. Use back to see previous question or complete exam to end the exam");
			displayMessage(lastPageMessage);
			//enable previous button for situations where last page is the page loaded
			previousButton.setEnabled(true);
			nextButton.setEnabled(false);
			completeExamButton.setVisibility(View.VISIBLE);
		}
	} //loadNextQuestion()

	private boolean checkIfUnansweredQuestions()
	{
		boolean returnValue = false;

		for (int questionCounter = 0; questionCounter < totalQuestions; questionCounter++)
			if ((answeredOptionsArray[questionCounter].compareTo("-1")) == 0) //means unanswered
			{
				returnValue = true;
				break;
			}
		return returnValue;
	} //checkIfUnansweredQuestions()

	//this method is called to display a message and hide evaluate and other buttons
	private void displayMessage(String messageToDisplay)
	{
		//System.out.println("To display message = " + messageToDisplay);
		//	display the data
		/*
		testQuestion.setText("".toCharArray(), 0, 0);
		answerAndMessages.setVisibility(View.VISIBLE);
		answerAndMessages.setText(messageToDisplay.toCharArray(), 0, messageToDisplay.length());
		//delete existing options
		optionsRadioGroup.removeAllViews();
		multipleRightChoice.removeAllViews();
		evaluateButton.setEnabled(false);*/
		displayMessageCommonWork();
		answerAndMessages.setText(messageToDisplay.toCharArray(), 0, messageToDisplay.length());

		//nextButton.setVisibility(View.INVISIBLE);
	}

	private void displayColoredMessage(String messageToDisplay, int colorCode)
	{
		SpannableString messageInColor = null;
		messageInColor = new SpannableString(messageToDisplay);
		messageInColor.setSpan(new ForegroundColorSpan(colorCode), 0, messageToDisplay.length(), 0);

		displayMessageCommonWork();
		answerAndMessages.setText(messageInColor);
	}

	private void displayMessageCommonWork()
	{
		testQuestion.setText("".toCharArray(), 0, 0);
		answerAndMessages.setVisibility(View.VISIBLE);
		//delete existing options
		optionsRadioGroup.removeAllViews();
		multipleRightChoice.removeAllViews();
		evaluateButton.setEnabled(false);
	}

	private void displayCurrentUrls()
	{
		//disable the answer
		answerAndMessages.setVisibility(View.INVISIBLE);

		//enable evaluate button for next question
		evaluateButton.setEnabled(true);

		//enable previous and next button
		previousButton.setEnabled(true);
		nextButton.setEnabled(true);

		//uncheck all options
		optionsRadioGroup.clearCheck();

		if (multipleRight) //remove the checkbox group
		{
			if (multipleRightChoice != null)
			{
				multipleRightChoice.removeAllViews();
				currentLayout.removeView(multipleRightChoice);
			}
		}
		else //remove all radio boxes
			optionsRadioGroup.removeAllViews();

		//make all option views invisible
		multipleRightChoice.setVisibility(View.INVISIBLE);
		optionsRadioGroup.setVisibility(View.INVISIBLE);

		multipleRight = false;

		try
		{
			//read the first two lines of the file
			//File questionFile = new File(getExternalFilesDir(null), currentTextUrl);
			File questionFile = new File(storageDirectory, currentTextUrl);
			//FileReader questionFileReader = new FileReader(questionFile);

			//setup decrytion for data
			SecretKeySpec decryptKey = new SecretKeySpec(deviceKey, "AES");
			// 	Create the cipher
			Cipher cDecrypt = Cipher.getInstance("AES");
			cDecrypt.init(Cipher.DECRYPT_MODE, decryptKey);
			InputStream fIn = new CipherInputStream(new FileInputStream(questionFile), cDecrypt);

//			BufferedReader bufferedQuestionReader = new BufferedReader(questionFileReader);
			BufferedReader bufferedQuestionReader = new BufferedReader(new InputStreamReader(fIn));

			String questionLengthStr = bufferedQuestionReader.readLine(); //first line is length of question
			String numberOfOptionsStr = bufferedQuestionReader.readLine(); //next line is no. of options

			//	convert the two to ints
			int questionLength = Integer.parseInt(questionLengthStr);
			int numberOfOptions = Integer.parseInt(numberOfOptionsStr);

			//	read the question as no. of characters
			char[] questionBuf = new char[questionLength];
			//bufferedQuestionReader.read(questionBuf, 0, questionLength);
			// for some reason lolipop is not reading a buffer cleanly with length specified. Therefore using single character read
			for (int questionBufLength = 0; questionBufLength < questionLength; questionBufLength++)
				questionBuf[questionBufLength] = (char)bufferedQuestionReader.read();


			//read the no. of new lines as options
			String[] options = new String[numberOfOptions];
			for (int count = 0; count < numberOfOptions; count++)
			{
				//read the length of option given till end of line
				String optionLengthStr = bufferedQuestionReader.readLine();
				int optionLength = Integer.parseInt(optionLengthStr);

				//read characters as per length and assign to option string array
				char[] thisOption = new char[optionLength];
				//bufferedQuestionReader.read(thisOption, 0, optionLength);
				// for some reason lolipop is not reading a buffer cleanly with length specified. Therefore using single character read
				for (int optionLengthCount = 0; optionLengthCount < optionLength; optionLengthCount++)
					thisOption[optionLengthCount] = (char)bufferedQuestionReader.read();

				options[count] = String.valueOf(thisOption);
			}

			String rightOptionStr = bufferedQuestionReader.readLine();
			//System.out.println("right option = " + rightOptionStr);

			String[] rightOptionBreakup = rightOptionStr.split(",");
			rightOption = new int[rightOptionBreakup.length];

			for (int count = 0; count < rightOptionBreakup.length; count++)
				rightOption[count] = Integer.parseInt(rightOptionBreakup[count]);

			if (rightOption.length > 1)
				multipleRight = true;

			String explanationLengthStr = bufferedQuestionReader.readLine();
			int explanationLength = Integer.parseInt(explanationLengthStr);
			if (explanationLength != 0)
			{
				//	read the question as no. of characters
				char[] explanationBuf = new char[explanationLength];
				//bufferedQuestionReader.read(explanationBuf, 0, explanationLength);
				// for some reason lolipop is not reading a buffer cleanly with length specified. Therefore using single character read
				for (int explanationBufCount = 0; explanationBufCount < explanationLength; explanationBufCount++)
					explanationBuf[explanationBufCount] = (char)bufferedQuestionReader.read();

				answerExplanation = String.valueOf(explanationBuf);
			}
			else
				answerExplanation = "";

			//read the option and make it a hashmap
			String[] answeredOptions = null;
			HashMap<Integer, Boolean> optionHash = new HashMap<Integer, Boolean>();
			//get the answered option string broken up
			if ((answeredOptionsArray[lastQuestionNum].compareTo("-1")) != 0)
				answeredOptions = answeredOptionsArray[lastQuestionNum].split(":");
			if (answeredOptions != null)
			{
				for (int count = 0; count < answeredOptions.length; count++)
					optionHash.put(Integer.parseInt(answeredOptions[count]), Boolean.valueOf(true)); //fill up hashmap for answered options
			}

			//	display the data
			testQuestion.setText(questionBuf, 0, questionLength);

			//delete existing options
			//optionsRadioGroup.removeAllViews();

			LayoutParams optionLayoutParam = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);

			//display the options either as a radio group or check box
			if (multipleRight)
			{
				//optionsRadioGroup.setVisibility(View.INVISIBLE); //disable radio group

				multipleRightChoice.setVisibility(View.VISIBLE);

				//add checkboxes
				for (int count = 0; count < numberOfOptions; count++)
				{
					CheckBox optionCheckBox = new CheckBox(currentLayout.getContext());
					optionCheckBox.setText(options[count]);
					optionCheckBox.setTextColor(getResources().getColor(R.color.blackText));
					optionCheckBox.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
					if ((count % 2) == 0) //odd row as count starts from 0 for row #1
						optionCheckBox.setBackgroundColor(getResources().getColor(R.color.lightGrey));
					optionCheckBox.setLayoutParams(optionLayoutParam);
					optionCheckBox.setId(count+1);
					multipleRightChoice.addView(optionCheckBox);
				}

				LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				params.addRule(RelativeLayout.BELOW, R.id.questionText);
				currentLayout.addView(multipleRightChoice, params);

				//show buttons relative to last option
				params = (LayoutParams)previousButton.getLayoutParams();
				params.addRule(RelativeLayout.BELOW, 1234);
			}
			else
			{
				optionsRadioGroup.setVisibility(View.VISIBLE);

				for (int count = 0; count < numberOfOptions; count++)
				{
					RadioButton optionButton = new RadioButton(this);
					optionButton.setText(options[count]);
					optionButton.setId(count+1);
					//check if this option should be checked
					if ((optionHash != null) && (optionHash.get(Integer.valueOf(count+1)) != null))
						optionButton.setChecked(true); //setSelected did not work

					optionButton.setTextColor(getResources().getColor(R.color.blackText));
					optionButton.setTextSize(TypedValue.COMPLEX_UNIT_DIP, DEFAULT_TEXT_SIZE);
					if ((count % 2) == 0) //odd row as count starts from 0 for row #1
						optionButton.setBackgroundColor(getResources().getColor(R.color.lightGrey));
					optionButton.setLayoutParams(optionLayoutParam);
					optionsRadioGroup.addView(optionButton);
				}

				LayoutParams params = (LayoutParams)previousButton.getLayoutParams();
				params.addRule(RelativeLayout.BELOW, R.id.optionsGroup);
			}
			
			bufferedQuestionReader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		//set the page number values
    	((TextView)pageNumberView).setText((lastQuestionNum+1) + "/" + totalQuestions);

    	examViewerScrollLayout.scrollTo(0, 0); //show the top of the screen always
		
	} //displayCurrentUrls()

	protected void resetStateBeforeDownloadAttempt()
	{
		//called when current page is to be shown without any change
		lastQuestionNum = revertBackQuestionNum;
		evaluateButton.setEnabled(true);
		nextButton.setEnabled(true);
		previousButton.setEnabled(true);
		if (lastQuestionNum == totalQuestions)
			nextButton.setEnabled(false);
		if (lastQuestionNum <= 1)
			previousButton.setEnabled(false);

	} //resetStateBeforeDownloadAttempt()


	private class TestPageRetrieveAsyncTask extends AsyncTask<Object, Void, Object[]> 
	{   
			String testPageName = null;
			
			@Override
			protected Object[] doInBackground(Object... urls) {
		
				//get the required page
				String testQuestionPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestQuestion.php";
				//temporary comment to try out on a different page.
				//String testQuestionPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestQuestion_Trial.php";
				//set the name value parameters for call
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
				nameValuePairs.add(new BasicNameValuePair("CourseId",Integer.toString(testId)));            
				nameValuePairs.add(new BasicNameValuePair("PageNo", Integer.toString(nextPageToLoad)));            

				PostRequestPerform testQuestionPageRequest = new PostRequestPerform(testQuestionPageUrl, nameValuePairs);
				InputStream content = testQuestionPageRequest.getResponse();

				if (content != null)
				{
					//create the file to store
					testPageName = testId+"_"+ (nextPageToLoad); 
					//System.out.println("Test file name = " + testPageName);
				
					try
					{
						//File questionTextFile = new File(getExternalFilesDir(null), testPageName);
						File questionTextFile = new File(storageDirectory, testPageName);
						FileOutputStream questionFileOutputStream= new FileOutputStream(questionTextFile);

						SecretKeySpec encryptKey = new SecretKeySpec(deviceKey, "AES");
						// Create the cipher
						Cipher cEncrypt = Cipher.getInstance("AES");
						cEncrypt.init(Cipher.ENCRYPT_MODE, encryptKey);
						CipherOutputStream encrypytedQuestionFile = new CipherOutputStream(questionFileOutputStream, cEncrypt); 


						BufferedInputStream buffer = new BufferedInputStream(content);
						byte buf[]=new byte[10240];
						int len;
						while((len=buffer.read(buf)) >0)
							encrypytedQuestionFile.write(buf,0,len);
				
						encrypytedQuestionFile.close();
						questionFileOutputStream.close();
					}
					catch (Exception e)
					{
						testPageName = null;
						e.printStackTrace();
					}
				
					//	make the entry for the page in the local db
					//save the page details locally if required
					AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
						
					SQLiteDatabase db = dbHelper.getWritableDatabase();
					ContentValues values = new ContentValues();
					values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, testId);
					values.put(AnytimeLearnDb.PAGE_NO_COL_NAME, nextPageToLoad);
					values.put(AnytimeLearnDb.HAS_SOUND_COL_NAME, 0);
					values.put(AnytimeLearnDb.HAS_NEXT_COL_NAME, 0);
					values.put(AnytimeLearnDb.HAS_PREV_COL_NAME, 0);
					values.put(AnytimeLearnDb.PAGE_TYPE_COL_NAME, 0);
						
					long insertValue = db.insert(AnytimeLearnDb.PAGE_INFO_TABLE_NAME, null, values);
					//System.out.println("Insert value for test page= " + insertValue);
					db.close(); // Closing database connection
				}
				else
				{
					//content null means page could not be retrieved
					testPageName = null;
				}

				return null;
					
		} //doInBackground()

		@Override
		protected void onPostExecute(Object[] pageData) 
		{
			//show progress bar here
	    	loadProgress.dismiss();

			if (testPageName != null)
			{
		    	//start the timer
				startTimer(2);
				
				currentTextUrl = testPageName;  
				displayCurrentUrls();
			}
			else
			{
				//null indicates problems in decryption or no internet. Ignoring decryption for now
				Toast.makeText(getApplicationContext(), "Please check your Internet connectivity and setup", Toast.LENGTH_LONG).show();
				//start the timer as user can still try to use downloaded questions
				startTimer(4);
				//no change to testPageName
				resetStateBeforeDownloadAttempt();
			}
		} //onPostExecute
	} //TestPageRetrieveAsyncTask	
	
	private class UpdatePageNoAsyncTask extends AsyncTask<Object, Void, Object[]> 
	{   
			
			@Override
			protected Object[] doInBackground(Object... urls) {
				String response = "";

				//update the last page no. on server
				String pageResetUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "updatePageNoPost.php";
				//set the name value parameters for call
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);            
				nameValuePairs.add(new BasicNameValuePair("CourseId",Integer.toString(testId)));            
				nameValuePairs.add(new BasicNameValuePair("SimId",subscriberId));
				nameValuePairs.add(new BasicNameValuePair("PageNo", Integer.toString(lastQuestionNum)));

				PostRequestPerform pageNoUpdateRequest = new PostRequestPerform(pageResetUrl, nameValuePairs);
				InputStream responseFromPageUpdate = pageNoUpdateRequest.getResponse();
				
				//discard response
					
				return null;	
			} //doInBackground()

			@Override
			protected void onPostExecute(Object[] pageData) 
			{
		//System.out.println("Page no. updated");
				//do nothing here
			} //onPostExecute
	} //UpdatePageNoAsyncTask


	private class ExamQuestionListRetrieveAsyncTask extends AsyncTask<Object, Void, Object[]> 
	{   
			String testPageName = null;
			
			@Override
			protected Object[] doInBackground(Object... urls) {
				String response = "";
		
				//get the required page
				String testQuestionPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getExamQuestionList.php";
				//temporary comment to try out on a different page.
				//String testQuestionPageUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "getTestQuestion_Trial.php";
				//set the name value parameters for call
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
				nameValuePairs.add(new BasicNameValuePair("CourseId",Integer.toString(testId)));            
				nameValuePairs.add(new BasicNameValuePair("SimId",subscriberId));            

				PostRequestPerform testQuestionPageRequest = new PostRequestPerform(testQuestionPageUrl, nameValuePairs);
				InputStream content = testQuestionPageRequest.getResponse();

				if (content != null)
				{
					//reply expected in form lastPage&totalPage
					BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
					try
					{
						String s = "";
						while ((s = buffer.readLine()) != null)
							response += s;
						//	System.out.println("book mark response " + response);
					}
					catch (IOException ioe)
					{
						ioe.printStackTrace();
						//lastPage = totalPages = -1; //commenting to avoid overwriting of value from db. We ignore exception and let value remain -1
						return null;
					}
//System.out.println("Response to exam initialization = " + response);
					String[] testQuestionList = response.split("&"); //example format for 3 questions is 3&10&2&4&6
					int numQuestions = Integer.parseInt(testQuestionList[0]); //first location is the number of questions
					int totalTime = Integer.parseInt(testQuestionList[1]); //next location is the duration of the test in minutes
					
					boolean completeListReceived = true;
					questionNumberArray = new int[numQuestions]; //internal array to hold questions
					answeredOptionsArray = new String[numQuestions]; //internal array to hold options
					
					for (int count = 0; count < numQuestions; count++)
					{
						if ((count + 2) == testQuestionList.length) //stop when we go beyond the received list
						{
							completeListReceived = false;	
							break;
						}
						questionNumberArray[count] = Integer.parseInt(testQuestionList[count+2]); //page number of question at this index
						answeredOptionsArray[count] = "-1";
					}
					
					if (!completeListReceived) //if there was a problem in the list received
					{
						//do something here to reread the list
					}
					
					//store list in db here and also in local variable
					//first create entry in db for this exam
					AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());
					
					SQLiteDatabase db = dbHelper.getWritableDatabase();

					ContentValues values = new ContentValues();
					values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, testId); // put test id
					values.put(AnytimeLearnDb.COURSE_NAME_COL_NAME, testName); // put test name 
					values.put(AnytimeLearnDb.EXAM_DURATION_MINUTES, totalTime); // put total test duration
					values.put(AnytimeLearnDb.LAST_DISPLAY_QUESTION_NO, 0); //no questions displayed so far
					values.put(AnytimeLearnDb.NUM_QUESTIONS, numQuestions); // put total no. of questions
					values.put(AnytimeLearnDb.USED_MINUTES, 0); // put 0 duration as test duration used
					values.put(AnytimeLearnDb.EXAM_STATUS, Character.toString(AnytimeLearnDb.EXAM_STATUS_ACTIVE)); // put status as active for this exam
					//put start time as now
					long currentTime = System.currentTimeMillis();
					String formattedStartTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(currentTime);

					System.out.println("start time at initialize = " + formattedStartTime);
					values.put(AnytimeLearnDb.EXAM_START_TIME, formattedStartTime);

					long insertValue = db.insert(AnytimeLearnDb.EXAM_TABLE_NAME, null, values);
					//System.out.println("Insert value for exam table row = " + insertValue);
					
					//insert list of questions
					values = new ContentValues(); //remove the old one
					values.put(AnytimeLearnDb.COURSE_ID_COL_NAME, testId); // put test id
					//put each row of question
					for (int count = 0; count < numQuestions; count++)
					{
						values.put(AnytimeLearnDb.QUESTION_SEQUENCE_NO, count); // sequence no. of question
						values.put(AnytimeLearnDb.QUESTION_PAGE_NO, questionNumberArray[count]); //page no. of question
						values.put(AnytimeLearnDb.GIVEN_ANSWER_OPTION, -1); // put default option no. till answered

						insertValue = db.insert(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, null, values);
						//System.out.println("Insert value for exam question table row = " + insertValue);
						
						//remove the values for next insert
						values.remove(AnytimeLearnDb.QUESTION_SEQUENCE_NO); //remove the old sequence no.
						values.remove(AnytimeLearnDb.QUESTION_PAGE_NO); //remove the old page no.
					}

					db.close();
					
					//initialize internal lists
					revertBackQuestionNum = lastQuestionNum = 0;
					totalQuestions = numQuestions;
					//totalExamDuration = remainingTime = totalTime; //remaining time also initialized to same value
					totalExamDuration = totalTime;
					remainingTime = totalExamDuration * 60000; //remaining time also initialized to same value in millis
					questionListNotInitialized = false;
			}
					
			return null;	
		} //doInBackground()

		@Override
		protected void onPostExecute(Object[] pageData) 
		{
			//show progress bar here
	    	initializeProgress.dismiss();

			startTimer(3); //starting timer
	    	loadNextQuestion();

			checkDownloadStatusAndDownloadAll();  //for first time load this is called

		} //onPostExecute
	} //ExamQuestionListRetrieveAsyncTask	

	protected void createExamAnswerArrayForSend()
	{
		jsonExamAnswerArray = new JSONArray();

		//get answers to this exam
		AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

		SQLiteDatabase db = dbHelper.getReadableDatabase();

		//get answers to each question
		Cursor cursor = db.query(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, new String[] { AnytimeLearnDb.QUESTION_SEQUENCE_NO, AnytimeLearnDb.QUESTION_PAGE_NO, AnytimeLearnDb.GIVEN_ANSWER_OPTION },
				AnytimeLearnDb.COURSE_ID_COL_NAME + "=?",
				new String[] {Integer.toString(testId)}, null, null, AnytimeLearnDb.QUESTION_SEQUENCE_NO + " ASC", null);
		if (cursor != null)
		{
			//get all the answers
			while (cursor.moveToNext())
			{
				try
				{
					JSONObject singleQuestionDetail = new JSONObject();

					int questionSequence = cursor.getInt(0);
					int pageNo = cursor.getInt(1);
					String answerGiven = cursor.getString(2);

					//System.out.println("Question no = " + questionSequence + " Page no = " + pageNo + " Answer = " + answerGiven);
					singleQuestionDetail.put("SeqNo", questionSequence);
					singleQuestionDetail.put("PageNo", pageNo);
					singleQuestionDetail.put("Answer", answerGiven);
					jsonExamAnswerArray.put(singleQuestionDetail);
				}
				catch (JSONException jse)
				{
					jse.printStackTrace();
					//continue moving to next element
				}
			}


			cursor.close();
		}
		db.close();

	} //createExamAnswerArrayForSend()

	private class UploadExamResultsAsyncTask extends AsyncTask<Object, Void, Object[]> 
	{   
			boolean deleteResults = true;
			
			@Override
			protected Object[] doInBackground(Object... urls) {
				String response = "";

				createExamAnswerArrayForSend();

				//page to send exam answers
				String examAnswerListUrl = getString(R.string.baseURL) + getString(R.string.MOBLE_APP_EXTENSION) + "takeExamAnswers.php";

				//create the json object to send all parameters
				JSONObject masterObjectElement = new JSONObject();
				try
				{
					masterObjectElement.put("CourseId",testId);
					masterObjectElement.put("SimId",subscriberId);
				
				/*
				//set the name value parameters for call
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);            
				nameValuePairs.add(new BasicNameValuePair("CourseId",Integer.toString(testId)));            
				nameValuePairs.add(new BasicNameValuePair("SimId",subscriberId)); */

					if (jsonExamAnswerArray != null) //add the answer data if there are any answers
						masterObjectElement.put("AnswerList", jsonExamAnswerArray);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					displayColoredMessage("Internal Error. Please contact Anytime Learn", Color.RED);
					return null;
				}


				PostRequestPerform examAnswerUpdate = new PostRequestPerform(examAnswerListUrl, masterObjectElement);
				InputStream responseFromExamAnswerUpdate = examAnswerUpdate.getResponse();
				if (responseFromExamAnswerUpdate == null) {
					deleteResults = false;
					//message for internet connectivity here
					//displayColoredMessage("Problem with internet connectivity. Please re-try upload after connecting to Internet", Color.RED);
				}
					//discard response
//System.out.println("Response = " + responseFromExamAnswerUpdate);
				return null;	
			} //doInBackground()

			@Override
			protected void onPostExecute(Object[] pageData) 
			{
				uploadProgress.dismiss();

				if (deleteResults)
				{
					//System.out.println("Answers updated");

					//delete the records for this test from the table
					AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

					//delete all rows of the page content that exist
					SQLiteDatabase db = dbHelper.getWritableDatabase();
					String deleteWhereClause = AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?";
					db.delete(AnytimeLearnDb.EXAM_TABLE_NAME, deleteWhereClause, new String[]{Integer.toString(testId)});
					db.delete(AnytimeLearnDb.EXAM_QUESTION_TABLE_NAME, deleteWhereClause, new String[]{Integer.toString(testId)});

					db.close(); // Closing database connection

					Toast.makeText(getApplicationContext(), "Data successfully saved. You may now close the app", Toast.LENGTH_LONG).show();

				}
				else
				{
					//System.out.println("Cannot delete local db");
					//update the status of the exam
					AnytimeLearnDb dbHelper = new AnytimeLearnDb(getApplicationContext());

					//delete all rows of the page content that exist
					SQLiteDatabase db = dbHelper.getWritableDatabase();
					ContentValues values = new ContentValues();
					values.put(AnytimeLearnDb.EXAM_STATUS, Character.toString(AnytimeLearnDb.EXAM_STATUS_COMPLETE)); // put status as complete
					long updateValue = db.update(AnytimeLearnDb.EXAM_TABLE_NAME, values, AnytimeLearnDb.COURSE_ID_COL_NAME + "= ?", new String[]{Integer.toString(testId)});
					//System.out.println("Update of exam table = " + updateValue);
					//not using the update value
					db.close(); // Closing database connection

					Toast.makeText(getApplicationContext(), "Error in saving data. Please verify Internet Connectivity", Toast.LENGTH_LONG).show();
					
				}

			} //onPostExecute
	} //UploadExamResultAsyncTask

} //ExamViewer
